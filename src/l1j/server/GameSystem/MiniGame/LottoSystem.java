package l1j.server.GameSystem.MiniGame;

import java.util.Calendar;
import java.util.Locale;
import java.util.Random;
import java.text.SimpleDateFormat;

import javolution.util.FastTable;

import l1j.server.Config;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_EffectLocation;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_SystemMessage;

public class LottoSystem extends Thread {

	private static LottoSystem _instance;

	private boolean _LottoStart;

	public boolean getLottoStart() {
		return _LottoStart;
	}

	public void setLottoStart(boolean Lotto) {
		_LottoStart = Lotto;
	}

	private static long sTime = 0;

	public boolean isGmOpen = false;

	private int lottoAden = 0;

	public void addLottoAden(int i) {
		lottoAden += i;
	}

	public int getLottoAden() {
		return lottoAden;
	}

	public void setLottoAden(int i) {
		lottoAden = i;
	}

	private static final FastTable<L1PcInstance> sList = new FastTable<L1PcInstance>();

	private static final SimpleDateFormat ss = new SimpleDateFormat("MM-dd HH:mm", Locale.KOREA);

	public static LottoSystem getInstance() {
		if (_instance == null) {
			_instance = new LottoSystem();
		}
		return _instance;
	}

	@Override
	public void run() {
		try {
			while (true) {
				Thread.sleep(1000);
				/** 오픈 **/
				if (!isOpen() && !isGmOpen)
					continue;
				if (L1World.getInstance().getAllPlayers().size() <= 0)
					continue;

				isGmOpen = false;

				/** 시작 메세지 **/
				L1World.getInstance().broadcastServerMessage("실시간 로또게임이 시작됩니다.");
				L1World.getInstance()
						.broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "실시간 로또게임이 시작됩니다."));

				/** 로또게임 시작 **/
				setLottoStart(true);
				Thread.sleep(5000);

				for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
					pc.setLotto(0);
					if (pc.getLevel() >= Config.LOTTO_LEVEL) {
						Random lotto = new Random();
						int lottonumber = 0;
						lottonumber = lotto.nextInt(L1World.getInstance().getAllPlayersSize()) + 1;
						pc.sendPackets(new S_SystemMessage(
								"\\aG" + pc.getName() + "님의 로또 응모번호는 \\aD" + lottonumber + "번 \\aG입니다."));
						pc.sendPackets(new S_SystemMessage("\\aG30초 후 당첨자를 발표 합니다."));
						LottoSystem.getInstance().addLottoAden(Config.LOTTO_BATTING);
						pc.setLotto(lottonumber);
					}
				}

				Thread.sleep(25000L);

				L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
						"\\f=          5초뒤 \\f7실시간 로또게임 \\f=당첨번호 발표\\f7가 있겠습니다."));

				Thread.sleep(5000);

				/** 당첨번호 발표 **/
				getNumber();

				Thread.sleep(5000);

				/** 당첨자 발표 및 금액 배분 **/
				giveAden();

				/** 로또 게임 종료 **/

				setLottoStart(false);
				End();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 오픈 시각을 가져온다
	 *
	 * @return (String) 오픈 시각(MM-dd HH:mm)
	 */
	public String OpenTime() {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(sTime);
		return ss.format(c.getTime());
	}

	private void giveAden() {
		int count = 0;
		int lcount = 0;
		if (sList.size() > 0) {
			lcount = getLottoAden() + Config.LOTTO_BONUS;
			count = lcount / sList.size();
			for (L1PcInstance c : sList) {
				if (getLottoAden() > 150000000) {
					setLottoAden(150000000);
				}
				S_EffectLocation L = new S_EffectLocation(c.getX() - 2, c.getY() - 2, 6422);
				c.sendPackets(L, false);
				c.broadcastPacket(L);
				S_EffectLocation O = new S_EffectLocation(c.getX() - 1, c.getY() - 1, 6425);
				c.sendPackets(O, false);
				c.broadcastPacket(O);
				S_EffectLocation T = new S_EffectLocation(c.getX(), c.getY(), 6430);
				c.sendPackets(T, false);
				c.broadcastPacket(T);
				S_EffectLocation T2 = new S_EffectLocation(c.getX() + 1, c.getY() + 1, 6430);
				c.sendPackets(T2, false);
				c.broadcastPacket(T2);
				S_EffectLocation O2 = new S_EffectLocation(c.getX() + 2, c.getY() + 2, 6425);
				c.sendPackets(O2, false);
				c.broadcastPacket(O2);
				c.sendPackets(new S_SystemMessage("\\aE축하드립니다. 로또 게임에 당첨 되었습니다."));
				c.sendPackets(new S_SystemMessage("\\aE총 금액에서 당첨자 수로 나눈 금액이 지급됩니다."));
				c.sendPackets(new S_SystemMessage("\\aE당첨금 \\aD" + count + "아데나 \\aE가 지급 되었습니다."));
				L1World.getInstance().broadcastServerMessage("\\aD" + c.getName() + "님이 로또에 당첨 되었습니다. 축하드립니다.");
				c.getInventory().storeItem(40308, count);
			}
			L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
					"\\f7이번 실시간 로또의 총상금은 \\f=" + lcount + "아데나 \\f7이며, 총 \\f=" + sList.size() + "명\\f7의 당첨자가 나왔습니다"));
			L1World.getInstance().broadcastPacketToAll(new S_SystemMessage("\\aC이번 로또게임의 총 상금은 \\aD" + lcount + "아데나 \\aC입니다."));
			L1World.getInstance()
					.broadcastPacketToAll(new S_SystemMessage("\\aC총 당첨자는 \\aD" + sList.size() + "명 \\aC입니다."));
			setLottoAden(0);
		} else {
			lcount = getLottoAden() + Config.LOTTO_BONUS;
			L1World.getInstance()
					.broadcastPacketToAll(new S_SystemMessage("\\aC이번 로또게임의 총상금은 \\aD" + lcount + "아데나 \\aC입니다."));
			L1World.getInstance().broadcastPacketToAll(new S_SystemMessage("\\aC당첨자가 없으므로 \\aD당첨금은 이월처리 \\aC되었습니다."));
			setLottoAden(lcount);
		}
	}

	private void getNumber() {
		Random lotto = new Random();
		int number = 0;
		number = lotto.nextInt(L1World.getInstance().getAllPlayersSize()) + 1;
		for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
			if (pc.getLotto() == number) {
				if (!sList.contains(pc)) {
					sList.add(pc); // 당첨자수 추가
				}
			}
			pc.sendPackets(new S_SystemMessage("\\aC실시간 로또게임 당첨번호는 \\aD" + number + "번 \\aC입니다."));
		}
	}

	private boolean isOpen() {
		Calendar oCalendar = Calendar.getInstance();
		int hour = oCalendar.get(Calendar.HOUR_OF_DAY);
		int minute = oCalendar.get(Calendar.MINUTE);
		int second = oCalendar.get(Calendar.SECOND);

		String[] open_time = Config.LOTTO_TIME.split(",");

		for (int i = 0; i < open_time.length; i++) {
			String[] time = open_time[i].split(":");
			if (hour == Integer.valueOf(time[0]) && minute == Integer.valueOf(time[1]) && second == 0) {
				return true;
			}
		}
		return false;
	}

	/** 종료 **/
	private void End() {
		sList.clear(); // 당첨자수 초기화
		setLottoStart(false);
	}
}