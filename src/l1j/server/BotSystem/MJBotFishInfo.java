package l1j.server.BotSystem;

/**********************************
 * 
 * MJ Bot Fish position information.
 * made by mjsoft, 2016.
 *  
 **********************************/
public class MJBotFishInfo {
	public int x;
	public int y;
	public int h;
	public int mid;
	public int fx;
	public int fy;
}
