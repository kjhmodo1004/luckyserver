package l1j.server.swing;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.lang.management.ManagementFactory;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import com.sun.management.OperatingSystemMXBean;

import l1j.server.Config;
import l1j.server.GrangKinConfig;
import l1j.server.Server;
import l1j.server.server.GameServer;
import l1j.server.server.GameServerSetting;
import l1j.server.server.datatables.IpTable;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.skill.L1SkillUse;
import l1j.server.server.monitor.LoggerInstance;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.utils.SystemUtil;

/** 구성원 로드 */

public class chocco extends JFrame {
	/** 생성자 */
	public static boolean inf;
	public static String[] save_text = new String[7]; // 각 필드들의 내용을 저장하기 위한 임시용
														// 공간
	public static boolean serverstart; // 서버가 실행중인지 판단
	public static int check = 0; // 인터페이스로 크기 조절(이미지 변경을 위한)
	public static int x_size = 0; // 최초 매니져 사이즈를 담기 위해
	private ImageIcon logo = new ImageIcon("data/img/logo.jpg"); // 매니져 로도이미지
	private ImageIcon start = new ImageIcon("data/img/start.jpg"); // 시작 버튼 이미지
	private ImageIcon exit = new ImageIcon("data/img/exit.jpg"); // 종료 버튼 이미지
	private ImageIcon up = new ImageIcon("data/img/up.jpg"); // 창 업 이미지
	private ImageIcon down = new ImageIcon("data/img/down.jpg"); // 창 다운 이미지
	private ImageIcon so = new ImageIcon("data/img/so.jpg"); // 위 화살표 이미지
	private ImageIcon chating = new ImageIcon("data/img/chat.jpg"); // 채팅 이미지
	private ImageIcon in = new ImageIcon("data/img/in.jpg"); // 오른쪽 화살표 이미지

	private javax.swing.JButton buf; // 올버프 버튼
	private javax.swing.JTextField chat; // 채팅 필드
	private javax.swing.JButton ex; // 종료버튼
	public static javax.swing.JLabel first; // 매니져 첫 글귀용
	private javax.swing.JInternalFrame iframe; // 각종 로그를 보기 위한 프레임
	private javax.swing.JInternalFrame iframe2; // 유저리스트를 보기 위한 프레임
	private javax.swing.JInternalFrame iframe3;
	private javax.swing.JButton info; // 서버 제작정보 버튼
	private javax.swing.JButton info1; // 서버 정보 버튼
	private javax.swing.JLabel label1; // 현재 유저를 표기하기 위한 라벨
	public static javax.swing.JLabel label2; // --
	// private javax.swing.JLabel label3; // --
	private javax.swing.JLabel label4; // 현재 사용중인 메모리량 표기
	private javax.swing.JLabel lblThread;
	private javax.swing.JLabel lblCPU;
	private javax.swing.JLabel lblCPUProgressBack;
	private javax.swing.JLabel lblCPUProgress;
	public static javax.swing.JLayeredPane layout1; // 메뉴와 비슷한 인터페이스를 위한 레이아웃
	private javax.swing.JLayeredPane layout2; // --
	private javax.swing.JLayeredPane layout3; // --
	private javax.swing.JLayeredPane layout4; // --
	private javax.swing.JLayeredPane layout5;
	private javax.swing.JButton logcl; // 로그창(각종 텍스트창) 초기화 버튼
	private javax.swing.JButton logsv; // 로그창 저장 버튼
	private javax.swing.JTextField name; // 귓속말을 위한 필드

	private javax.swing.JButton st; // 시작 버튼
	public static javax.swing.JLabel stime; // 서버가동시간을 표기하기 위한 라벨
	private javax.swing.JButton sv; // 서버 저장 버튼
	private javax.swing.JTabbedPane tabframe; // 각 로그창을 별로도 보기위한 탭패널

	private javax.swing.JScrollPane spGlobalChat; // 각 로그창 스크롤 패널
	private javax.swing.JScrollPane spClanChat; // --
	private javax.swing.JScrollPane spPartyChat; // --
	private javax.swing.JScrollPane spWhisper; // --
	private javax.swing.JScrollPane spShop; // --
	private javax.swing.JScrollPane spTrade; // --
	private javax.swing.JScrollPane spWarehouse; // --
	private javax.swing.JScrollPane spEnchant;
	private javax.swing.JScrollPane spPickup;
	private javax.swing.JScrollPane spUserList; // --
	public static javax.swing.JTextArea txtGlobalChat; //
	public static javax.swing.JTextArea txtClanChat;
	public static javax.swing.JTextArea txtPartyChat; //
	public static javax.swing.JTextArea txtWhisper; //
	public static javax.swing.JTextArea txtShop; //
	public static javax.swing.JTextArea txtTrade; //
	public static javax.swing.JTextArea txtWarehouse; //
	public static javax.swing.JTextArea txtEnchant; //
	public static javax.swing.JTextArea txtPickup; //

	private javax.swing.JButton updown; // 위아래 버튼
	public static java.awt.List userlist; // 리스트
	public static java.awt.List iplist; // 차단아이피
	private javax.swing.JPopupMenu popmenu1; // 리스트창용 팝업메뉴
	private javax.swing.JPopupMenu popmenu2; // 리스트창용 팝업메뉴2
	private javax.swing.JMenuItem menu1; // 메뉴1(추방)
	private javax.swing.JMenuItem menu2; // 메뉴2 -> ??
	private javax.swing.JMenuItem menu3; // 메뉴3 //
	private javax.swing.JMenuItem menu4; // 메뉴3 //
	private javax.swing.JMenuItem menu5; // 메뉴3 //
	private javax.swing.JMenuItem menu6; // 선물주기
	private javax.swing.JMenuItem menu7; // 귓속말
	private javax.swing.JTabbedPane chocco; // 추가용
	public static int count = 0;

	private javax.swing.JPopupMenu pmReload;

	public static JFrame setting;
	private javax.swing.JTextField exp;
	private javax.swing.JTextField lawful;
	private javax.swing.JTextField karma;
	private javax.swing.JTextField adena;
	private javax.swing.JTextField item;
	private javax.swing.JTextField enweapon;
	private javax.swing.JTextField enarmor;
	private javax.swing.JTextField weightlimit;
	private javax.swing.JTextField chatlvl;
	private javax.swing.JTextField maxuser;
	private javax.swing.JButton set;

	private javax.swing.JLabel exp1;
	private javax.swing.JLabel lawful1;
	private javax.swing.JLabel karma1;
	private javax.swing.JLabel adena1;
	private javax.swing.JLabel item1;
	private javax.swing.JLabel enweapon1;
	private javax.swing.JLabel enarmor1;
	private javax.swing.JLabel weightlimit1;
	private javax.swing.JLabel chatlvl1;
	private javax.swing.JLabel maxuser1;

	/** 기본 생성자 */
	public chocco() {
		/** 각 메뉴 / 컴포넌트 셋팅 */
		initComponents();
		/** 최초 매니저 사이즈 저장 */
		x_size = this.getHeight();
		/** 최초 매니저 사이즈 셋팅 */
		this.setSize(this.getWidth() + 10, 60);
		/** 매니저 실행 위치 셋팅 */
		this.setLocation(300, 200);
	}

	/** 각 메뉴 / 컴포넌트 셋팅... 하나라도 빠지면 안된다.. 컴파일뿐 아니라. 실행 동작에서 오류가 발생 가능성이 있다. */
	private void initComponents() {
		layout1 = new javax.swing.JLayeredPane();
		first = new javax.swing.JLabel();
		st = new javax.swing.JButton(start);
		ex = new javax.swing.JButton(exit);
		sv = new javax.swing.JButton();
		buf = new javax.swing.JButton();
		info = new javax.swing.JButton();
		updown = new javax.swing.JButton(down);
		info1 = new javax.swing.JButton();
		logsv = new javax.swing.JButton();
		logcl = new javax.swing.JButton();
		label1 = new javax.swing.JLabel();
		label2 = new javax.swing.JLabel();
		label4 = new javax.swing.JLabel();
		lblThread = new javax.swing.JLabel();
		lblCPU = new javax.swing.JLabel();
		layout2 = new javax.swing.JLayeredPane();
		iframe = new javax.swing.JInternalFrame();
		tabframe = new javax.swing.JTabbedPane();
		spGlobalChat = new javax.swing.JScrollPane();
		txtGlobalChat = new javax.swing.JTextArea();
		spShop = new javax.swing.JScrollPane();
		txtShop = new javax.swing.JTextArea();
		spTrade = new javax.swing.JScrollPane();
		txtTrade = new javax.swing.JTextArea();
		spClanChat = new javax.swing.JScrollPane();
		txtClanChat = new javax.swing.JTextArea();
		spPartyChat = new javax.swing.JScrollPane();
		txtPartyChat = new javax.swing.JTextArea();
		spWhisper = new javax.swing.JScrollPane();
		txtWhisper = new javax.swing.JTextArea();
		spWarehouse = new javax.swing.JScrollPane();
		txtWarehouse = new javax.swing.JTextArea();
		spEnchant = new javax.swing.JScrollPane();
		txtEnchant = new javax.swing.JTextArea();

		spPickup = new javax.swing.JScrollPane();
		txtPickup = new javax.swing.JTextArea();

		layout3 = new javax.swing.JLayeredPane();
		layout5 = new javax.swing.JLayeredPane();
		iframe2 = new javax.swing.JInternalFrame();
		iframe3 = new javax.swing.JInternalFrame();
		spUserList = new javax.swing.JScrollPane();
		userlist = new java.awt.List();
		iplist = new java.awt.List();
		layout4 = new javax.swing.JLayeredPane();
		name = new javax.swing.JTextField();
		chat = new javax.swing.JTextField();
		stime = new javax.swing.JLabel();
		popmenu1 = new javax.swing.JPopupMenu();
		popmenu2 = new javax.swing.JPopupMenu();
		pmReload = new javax.swing.JPopupMenu();
		menu1 = new javax.swing.JMenuItem();
		menu2 = new javax.swing.JMenuItem();
		menu3 = new javax.swing.JMenuItem();
		menu4 = new javax.swing.JMenuItem();
		menu5 = new javax.swing.JMenuItem();
		menu6 = new javax.swing.JMenuItem();
		menu7 = new javax.swing.JMenuItem();
		chocco = new javax.swing.JTabbedPane();

		setting = new javax.swing.JFrame("Server Setting");
		chatlvl = new javax.swing.JTextField("", 5);
		maxuser = new javax.swing.JTextField("", 5);
		exp = new javax.swing.JTextField("", 5);
		adena = new javax.swing.JTextField("", 5);
		item = new javax.swing.JTextField("", 5);
		weightlimit = new javax.swing.JTextField("", 5);
		lawful = new javax.swing.JTextField("", 5);
		karma = new javax.swing.JTextField("", 5);
		enweapon = new javax.swing.JTextField("", 5);
		enarmor = new javax.swing.JTextField("", 5);
		set = new javax.swing.JButton("Setting Save");

		chatlvl1 = new javax.swing.JLabel("　채팅 레벨");
		maxuser1 = new javax.swing.JLabel("　최대 인원");
		exp1 = new javax.swing.JLabel("경험치 배율");
		adena1 = new javax.swing.JLabel("아데나 배율");
		item1 = new javax.swing.JLabel("아이템 배율");
		weightlimit1 = new javax.swing.JLabel("무게 최대치");
		lawful1 = new javax.swing.JLabel("라우풀 배율");
		karma1 = new javax.swing.JLabel("카르마 배율");
		enweapon1 = new javax.swing.JLabel("무기 인챈율");
		enarmor1 = new javax.swing.JLabel("방어 인챈율");

		setting.add(chatlvl1);
		setting.add(chatlvl);
		setting.add(maxuser1);
		setting.add(maxuser);
		setting.add(exp1);
		setting.add(exp);
		setting.add(adena1);
		setting.add(adena);
		setting.add(item1);
		setting.add(item);
		setting.add(weightlimit1);
		setting.add(weightlimit);
		setting.add(lawful1);
		setting.add(lawful);
		setting.add(karma1);
		setting.add(karma);
		setting.add(enweapon1);
		setting.add(enweapon);
		setting.add(enarmor1);
		setting.add(enarmor);
		set.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				setMouseClicked(evt);
			}
		});
		setting.add(set);

		menu1.setIcon(in);
		menu1.setText("강제종료");
		menu1.setToolTipText("캐릭명으로 강제종료");
		menu1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				menu1ActionPerformed(evt);
			}
		});
		popmenu1.add(menu1);

		menu2.setIcon(in);
		menu2.setText("아이피차단");
		menu2.setToolTipText("캐릭명으로 아이피차단");
		menu2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				menu2ActionPerformed(evt);
			}
		});
		popmenu1.add(menu2);

		menu7.setIcon(in);
		menu7.setText("귓속말");
		menu7.setToolTipText("해당 캐릭 귓속말");
		menu7.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				menu7ActionPerformed(evt);
			}
		});
		popmenu1.add(menu7);

		menu3.setIcon(in);
		menu3.setText("캐릭터정보");
		menu3.setToolTipText("캐릭터 정보");
		menu3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				menu3ActionPerformed(evt);
			}
		});
		popmenu1.add(menu3);

		menu4.setIcon(in);
		menu4.setText("아이피 삭제(List)");
		menu4.setToolTipText("리스트에 차단된 아이피 삭제");
		menu4.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				menu4ActionPerformed(evt);
			}
		});
		popmenu2.add(menu4);

		menu5.setIcon(in);
		menu5.setText("아이피 삭제(dB, List)");
		menu5.setToolTipText("디비, 리스트에 차단된 아이피 삭제");
		menu5.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				menu5ActionPerformed(evt);
			}
		});
		popmenu2.add(menu5);

		menu6.setIcon(in);
		menu6.setText("선물 주기");
		menu6.setToolTipText("해당 캐릭 선물주기");
		menu6.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				menu6ActionPerformed(evt);
			}
		});
		popmenu1.add(menu6);

		setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
		setTitle("Lucky Server monitoring Service");
		setName("frame");
		setResizable(false);

		// 여기

		first.setFont(new java.awt.Font("돋음", 1, 13));
		first.setText("실행 상태 : off");
		first.setForeground(Color.black);
		first.setBounds(10, 10, 100, 16);
		layout1.add(first, javax.swing.JLayeredPane.DEFAULT_LAYER);

		st.setToolTipText("Server Start");
		st.setContentAreaFilled(false);
		st.setMaximumSize(new java.awt.Dimension(21, 18));
		st.setMinimumSize(new java.awt.Dimension(21, 18));
		st.setPreferredSize(new java.awt.Dimension(21, 18));
		st.setRequestFocusEnabled(false);
		st.setSelected(true);
		st.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				stMouseClicked(evt);
			}
		});
		st.setBounds(110, 10, 21, 18);
		layout1.add(st, javax.swing.JLayeredPane.DEFAULT_LAYER);

		ex.setToolTipText("Server Exit");
		ex.setContentAreaFilled(false);
		ex.setMaximumSize(new java.awt.Dimension(21, 18));
		ex.setMinimumSize(new java.awt.Dimension(21, 18));
		ex.setPreferredSize(new java.awt.Dimension(21, 18));
		ex.setRequestFocusEnabled(false);
		ex.setSelected(true);
		ex.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				exMouseClicked(evt);
			}
		});
		ex.setBounds(140, 10, 21, 18);
		layout1.add(ex, javax.swing.JLayeredPane.DEFAULT_LAYER);

		sv.setText("R");
		sv.setToolTipText("리로드");
		sv.setContentAreaFilled(false);
		sv.setMargin(new java.awt.Insets(0, 0, 0, 0));
		sv.setMaximumSize(new java.awt.Dimension(21, 18));
		sv.setMinimumSize(new java.awt.Dimension(21, 18));
		sv.setPreferredSize(new java.awt.Dimension(21, 18));
		sv.setRequestFocusEnabled(false);
		sv.setSelected(true);
		sv.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				reloadMouseClicked(evt);
			}
		});
		sv.setBounds(170, 10, 21, 18);
		layout1.add(sv, javax.swing.JLayeredPane.DEFAULT_LAYER);

		buf.setText("E");
		buf.setToolTipText("Event");
		buf.setContentAreaFilled(false);
		buf.setMargin(new java.awt.Insets(0, 0, 0, 0));
		buf.setMaximumSize(new java.awt.Dimension(21, 18));
		buf.setMinimumSize(new java.awt.Dimension(21, 18));
		buf.setPreferredSize(new java.awt.Dimension(21, 18));
		buf.setRequestFocusEnabled(false);
		buf.setSelected(true);
		buf.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				bufMouseClicked(evt);
			}
		});
		buf.setBounds(200, 10, 21, 18);
		layout1.add(buf, javax.swing.JLayeredPane.DEFAULT_LAYER);

		info.setText("C");
		info.setToolTipText("Creat Info");
		info.setContentAreaFilled(false);
		info.setMargin(new java.awt.Insets(0, 0, 0, 0));
		info.setMaximumSize(new java.awt.Dimension(21, 18));
		info.setMinimumSize(new java.awt.Dimension(21, 18));
		info.setPreferredSize(new java.awt.Dimension(21, 18));
		info.setRequestFocusEnabled(false);
		info.setSelected(true);
		info.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				infoMouseClicked(evt);
			}
		});
		info.setBounds(260, 10, 21, 18);
		layout1.add(info, javax.swing.JLayeredPane.DEFAULT_LAYER);

		updown.setToolTipText("Page Up / Down");
		updown.setContentAreaFilled(false);
		updown.setMargin(new java.awt.Insets(0, 0, 0, 0));
		updown.setMaximumSize(new java.awt.Dimension(25, 18));
		updown.setMinimumSize(new java.awt.Dimension(25, 18));
		updown.setPreferredSize(new java.awt.Dimension(21, 18));
		updown.setRequestFocusEnabled(false);
		updown.setSelected(true);
		updown.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				updownMouseClicked(evt);
			}
		});
		updown.setBounds(350, 10, 21, 18);
		layout1.add(updown, javax.swing.JLayeredPane.DEFAULT_LAYER);

		info1.setText("!");
		info1.setToolTipText("Server Info");
		info1.setContentAreaFilled(false);
		info1.setMargin(new java.awt.Insets(0, 0, 0, 0));
		info1.setMaximumSize(new java.awt.Dimension(21, 18));
		info1.setMinimumSize(new java.awt.Dimension(21, 18));
		info1.setPreferredSize(new java.awt.Dimension(21, 18));
		info1.setRequestFocusEnabled(false);
		info1.setSelected(true);
		info1.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				info1MouseClicked(evt);
			}
		});
		info1.setBounds(230, 10, 21, 18);
		layout1.add(info1, javax.swing.JLayeredPane.DEFAULT_LAYER);

		logsv.setText("S");
		logsv.setToolTipText("Server Setting");
		logsv.setContentAreaFilled(false);
		logsv.setMargin(new java.awt.Insets(0, 0, 0, 0));
		logsv.setMaximumSize(new java.awt.Dimension(25, 18));
		logsv.setMinimumSize(new java.awt.Dimension(25, 18));
		logsv.setPreferredSize(new java.awt.Dimension(21, 18));
		logsv.setRequestFocusEnabled(false);
		logsv.setSelected(true);
		logsv.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				logsvMouseClicked(evt);
			}
		});
		logsv.setBounds(290, 10, 21, 18);
		layout1.add(logsv, javax.swing.JLayeredPane.DEFAULT_LAYER);

		logcl.setText("L");
		logcl.setToolTipText("Log Clear");
		logcl.setContentAreaFilled(false);
		logcl.setMargin(new java.awt.Insets(0, 0, 0, 0));
		logcl.setMaximumSize(new java.awt.Dimension(25, 18));
		logcl.setMinimumSize(new java.awt.Dimension(25, 18));
		logcl.setPreferredSize(new java.awt.Dimension(21, 18));
		logcl.setRequestFocusEnabled(false);
		logcl.setSelected(true);
		logcl.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				logclMouseClicked(evt);
			}
		});
		logcl.setBounds(320, 10, 21, 18);
		layout1.add(logcl, javax.swing.JLayeredPane.DEFAULT_LAYER);

		label1.setFont(new java.awt.Font("굴림", 1, 11));
		label1.setText("Users:");
		label1.setForeground(Color.red);
		label1.setToolTipText("All Players");
		label1.setBounds(400, 10, 50, 15);
		layout1.add(label1, javax.swing.JLayeredPane.DEFAULT_LAYER);

		label2.setFont(new java.awt.Font("굴림", 1, 11));
		label2.setText(" " + count);
		label2.setForeground(Color.red);
		label2.setBounds(450, 10, 70, 16);
		layout1.add(label2, javax.swing.JLayeredPane.DEFAULT_LAYER);

		label4.setFont(new java.awt.Font("굴림", 1, 11));
		label4.setText("Memory : 1024MB");
		label4.setForeground(Color.red);
		label4.setToolTipText("Use Memory");
		label4.setBounds(520, 10, 130, 15);
		layout1.add(label4, javax.swing.JLayeredPane.DEFAULT_LAYER);

		lblThread.setFont(new java.awt.Font("굴림", 1, 11));
		lblThread.setText("Thread : 1000");
		lblThread.setToolTipText("Use Thread Count.");
		lblThread.setForeground(Color.red);
		lblThread.setBounds(660, 10, 100, 16);
		layout1.add(lblThread, javax.swing.JLayeredPane.DEFAULT_LAYER);

		lblCPU.setFont(new java.awt.Font("굴림", 1, 11));
		lblCPU.setText("CPU : 100%");
		lblCPU.setToolTipText("Use CPU");
		lblCPU.setForeground(Color.red);
		lblCPU.setBounds(770, 10, 100, 16);
		layout1.add(lblCPU, javax.swing.JLayeredPane.DEFAULT_LAYER);

		iframe.setTitle("Main Game Server");
		iframe.setFrameIcon(chating);
		iframe.setVisible(true);

		txtGlobalChat.setColumns(20);
		txtGlobalChat.setEditable(false);
		txtGlobalChat.setRows(5);
		spGlobalChat.setViewportView(txtGlobalChat);
		tabframe.addTab("전체채팅", spGlobalChat);

		txtClanChat.setColumns(20);
		txtClanChat.setEditable(false);
		txtClanChat.setRows(5);
		spClanChat.setViewportView(txtClanChat);
		tabframe.addTab("혈맹채팅", spClanChat);

		txtPartyChat.setColumns(20);
		txtPartyChat.setEditable(false);
		txtPartyChat.setRows(5);
		spPartyChat.setViewportView(txtPartyChat);
		tabframe.addTab("파티채팅", spPartyChat);

		txtWhisper.setColumns(20);
		txtWhisper.setEditable(false);
		txtWhisper.setRows(5);
		spWhisper.setViewportView(txtWhisper);
		tabframe.addTab("귓속말", spWhisper);

		txtShop.setColumns(20);
		txtShop.setEditable(false);
		txtShop.setRows(5);
		spShop.setViewportView(txtShop);

		tabframe.addTab("NPC 상점", spShop);

		txtTrade.setColumns(20);
		txtTrade.setEditable(false);
		txtTrade.setRows(5);
		spTrade.setViewportView(txtTrade);
		tabframe.addTab("유저간 거래", spTrade);

		txtWarehouse.setColumns(20);
		txtWarehouse.setEditable(false);
		txtWarehouse.setRows(5);
		spWarehouse.setViewportView(txtWarehouse);
		tabframe.addTab("창고(통합)", spWarehouse);

		txtEnchant.setColumns(20);
		txtEnchant.setEditable(false);
		txtEnchant.setRows(5);
		spEnchant.setViewportView(txtEnchant);
		tabframe.addTab("인챈트", spEnchant);

		txtPickup.setColumns(20);
		txtPickup.setEditable(false);
		txtPickup.setRows(5);
		spPickup.setViewportView(txtPickup);
		tabframe.addTab("아이템 로그", spPickup);

		javax.swing.GroupLayout iframeLayout = new javax.swing.GroupLayout(iframe.getContentPane());
		iframe.getContentPane().setLayout(iframeLayout);
		iframeLayout.setHorizontalGroup(iframeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(iframeLayout.createSequentialGroup()
						.addComponent(tabframe, javax.swing.GroupLayout.PREFERRED_SIZE, 700,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		iframeLayout.setVerticalGroup(iframeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(tabframe, javax.swing.GroupLayout.DEFAULT_SIZE, 339, Short.MAX_VALUE));

		iframe.setBounds(0, 0, 700, 350);
		layout2.add(iframe, javax.swing.JLayeredPane.DEFAULT_LAYER);
		try {
			iframe.setMaximum(true);
		} catch (java.beans.PropertyVetoException e1) {
			e1.printStackTrace();
		}

		iframe2.setTitle("Server List");
		iframe2.setFrameIcon(logo);
		iframe2.setVisible(true);

		userlist.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				userlistMouseClicked(evt);
			}
		});

		iplist.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				iplistMouseClicked(evt);
			}
		});
		iplist.setFont(new Font("굴림", 0, 11));

		spUserList.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		spUserList.setViewportView(userlist);
		userlist.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				userlistActionPerformed(evt);
			}
		});
		chocco.addTab("Pc", so, userlist);
		chocco.addTab("Ip", so, iplist);

		javax.swing.GroupLayout iframe2Layout = new javax.swing.GroupLayout(iframe2.getContentPane());
		iframe2.getContentPane().setLayout(iframe2Layout);
		iframe2Layout.setHorizontalGroup(iframe2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(iframe2Layout.createSequentialGroup()
						.addComponent(chocco, javax.swing.GroupLayout.PREFERRED_SIZE, 115,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		iframe2Layout.setVerticalGroup(iframe2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(chocco, javax.swing.GroupLayout.DEFAULT_SIZE, 234, Short.MAX_VALUE));

		iframe2.setBounds(0, 0, 115, 340);
		layout3.add(iframe2, javax.swing.JLayeredPane.DEFAULT_LAYER);
		try {
			iframe2.setMaximum(true);
		} catch (java.beans.PropertyVetoException e1) {
			e1.printStackTrace();
		}
		iframe3.setVisible(true);
		javax.swing.GroupLayout iframe3Layout = new javax.swing.GroupLayout(iframe3.getContentPane());
		iframe3.getContentPane().setLayout(iframe3Layout);
		iframe3.setBounds(0, 0, 50, 340);
		lblCPUProgressBack = new JLabel();
		lblCPUProgressBack.setOpaque(true);
		lblCPUProgressBack.setBackground(Color.black);
		lblCPUProgressBack.setBounds(0, 0, 50, 330);
		lblCPUProgressBack.setText("");
		iframe3.add(lblCPUProgressBack);

		lblCPUProgress = new JLabel();
		lblCPUProgress.setOpaque(true);
		lblCPUProgress.setBackground(Color.green);
		lblCPUProgress.setBounds(0, 0, 50, 330);
		lblCPUProgress.setText("");
		iframe3.add(lblCPUProgress);
		layout5.add(iframe3, javax.swing.JLayeredPane.DEFAULT_LAYER);
		try {
			iframe3.setMaximum(true);
		} catch (PropertyVetoException e) {
			e.printStackTrace();
		}

		name.setBounds(0, 0, 80, 21);
		layout4.add(name, javax.swing.JLayeredPane.DEFAULT_LAYER);

		chat.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyPressed(java.awt.event.KeyEvent evt) {
				chatKeyPressed(evt);
			}
		});
		chat.setBounds(80, 0, 390, 21);
		layout4.add(chat, javax.swing.JLayeredPane.DEFAULT_LAYER);

		stime.setFont(new java.awt.Font("굴림", 1, 12));
		stime.setText("");
		stime.setBounds(480, 0, 180, 20);
		layout4.add(stime, javax.swing.JLayeredPane.DEFAULT_LAYER);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addContainerGap()
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(layout4, javax.swing.GroupLayout.DEFAULT_SIZE, 654, Short.MAX_VALUE)
								.addGroup(layout.createSequentialGroup()
										.addComponent(layout2, javax.swing.GroupLayout.PREFERRED_SIZE, 680,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(layout3, javax.swing.GroupLayout.PREFERRED_SIZE, 123,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(layout5, javax.swing.GroupLayout.PREFERRED_SIZE, 50,
												javax.swing.GroupLayout.PREFERRED_SIZE))
						.addComponent(layout1, javax.swing.GroupLayout.DEFAULT_SIZE, 850, Short.MAX_VALUE))
				.addContainerGap()));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addComponent(layout1, javax.swing.GroupLayout.PREFERRED_SIZE, 32,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(layout5, javax.swing.GroupLayout.DEFAULT_SIZE, 365, Short.MAX_VALUE)
								.addComponent(layout3, javax.swing.GroupLayout.DEFAULT_SIZE, 365, Short.MAX_VALUE)
								.addComponent(layout2, javax.swing.GroupLayout.DEFAULT_SIZE, 365, Short.MAX_VALUE))
				.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(layout4,
						javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
				.addContainerGap()));

		pack();
		new Thread(new PerformReader()).start();
	}
	/**
	 * 각 이벤트를 관리하는 곳
	 */

	/** 서버 시작 이벤트 */
	private void stMouseClicked(java.awt.event.MouseEvent evt) {
		/** 서버가 동작중이지 않다면... */
		if (!serverstart) {
			first.setText("실행 상태 : on");
			first.setForeground(Color.red);
			try {
				new Thread(new Runnable() {
					@Override
					public void run() {
						new Server();
						new ServerStart(evt.getXOnScreen(), evt.getYOnScreen());
					}
				}).start();
			} catch (Exception e) {
			}
			serverstart = true;
			/** 이미 서버가 동작중이면 */
		} else {
			javax.swing.JOptionPane.showMessageDialog(this, "이미 서버가 실행중입니다.", "Server Message",
					javax.swing.JOptionPane.ERROR_MESSAGE);
		}
	}

	/** 서버 종료 이벤트 */
	private void exMouseClicked(java.awt.event.MouseEvent evt) {
		/** 서버가 실행중이면 */
		if (serverstart) {
			/**
			 * 각 캐릭, 서버 데이터를 저장후.. 안전하게 종료 되도록 코딩
			 */
			int a = JOptionPane.showConfirmDialog(this, "정말 종료하시겠습니까 ?", "Server Message", 2,
					JOptionPane.INFORMATION_MESSAGE);
			if (a == JOptionPane.YES_OPTION) {
				GameServer.getInstance().shutdownWithCountdown(0);
			}

		} else {
			System.exit(0);
		}
	}

	public static boolean _isManagerCommands = false;

	private void reloadMouseClicked(java.awt.event.MouseEvent evt) {
		if (serverstart) {
			if (!_isManagerCommands) {
				_isManagerCommands = true;
				new ManagerCommands(evt.getXOnScreen(), evt.getYOnScreen());
			} else
				MJMessageBox.show(this, "이미 창이 활성화 되어 있습니다.", false);
		} else
			MJMessageBox.show(this, "서버가 실행중이지 않습니다.", false);
	}

	/** 이벤트 */
	private void bufMouseClicked(java.awt.event.MouseEvent evt) {
		/** 서버가 실행중이면 */
		if (serverstart) {
			/** 각종 이벤트 처리 */
			Object smallList[] = { "올버프", "버그경주", "무한대전", "전체채금", "채금해제" };
			String value = (String) javax.swing.JOptionPane.showInputDialog(this, "이벤트를 시작하시겠습니까?", " Server Message",
					JOptionPane.QUESTION_MESSAGE, null, smallList, smallList[0]);
			if (value != null) {
				// javax.swing.JOptionPane.showMessageDialog(this, "준비중인 서비스
				// 입니다..", " Server Message",
				// javax.swing.JOptionPane.INFORMATION_MESSAGE);
				if (value == "버그경주") {
					GameServerSetting _GameServerSetting = GameServerSetting.getInstance();
					// _GameServerSetting.BugRaceRestart = true;
				} else if (value == "올버프") {
					int[] allBuffSkill = { 26, 42, 48, 57, 68, 79, 158, 163, 168 };
					for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
						L1SkillUse l1skilluse = new L1SkillUse();
						for (int i = 0; i < allBuffSkill.length; i++) {
							l1skilluse.handleCommands(pc, allBuffSkill[i], pc.getId(), pc.getX(), pc.getY(), null, 0,
									L1SkillUse.TYPE_GMBUFF);
						}
					}
				} else if (value == "전체채금") {
					L1World.getInstance().set_worldChatElabled(false);
					txtGlobalChat.append("전체채금 실행");
				} else if (value == "채금해제") {
					L1World.getInstance().set_worldChatElabled(true);
					txtGlobalChat.append("채금해제 실행");
				} else {
					javax.swing.JOptionPane.showMessageDialog(this, "준비중인 서비스 입니다..", " Server Message",
							javax.swing.JOptionPane.INFORMATION_MESSAGE);
				}
			}
		} else {
			javax.swing.JOptionPane.showMessageDialog(this, "서버가 실행되지 않았습니다.", " Server Message",
					javax.swing.JOptionPane.ERROR_MESSAGE);
		}
	}

	/** 서버 제작정보 이벤트 */
	private void infoMouseClicked(java.awt.event.MouseEvent evt) {
		javax.swing.JOptionPane.showMessageDialog(this, "제작 : MJCodes", "Server Message",
				javax.swing.JOptionPane.INFORMATION_MESSAGE);
	}

	/** 메니저 크기 조절 이벤트 */
	private void updownMouseClicked(java.awt.event.MouseEvent evt) {
		/** 작은 상태라면 */
		if (check == 0) {
			/** 이미지 재셋팅 */
			updown.setIcon(up);
			/** 크기조절 */
			this.setSize(this.getWidth(), x_size);
			check = 1;
			/** 커진 상태라면 */
		} else {
			updown.setIcon(down);
			this.setSize(this.getWidth(), 60);
			check = 0;
		}
	}

	/** 서버 정보 이벤트 */
	private void info1MouseClicked(java.awt.event.MouseEvent evt) {
		/** 쓰레드 갯수, 데이터베이스 풀 사이즈 및 서버관련해서 표기 되지 않는 부분 별도로 표기 */
		try {
			if (serverstart)
				javax.swing.JOptionPane.showMessageDialog(this, "Pool : 사용 안함 - Max : 사용 안함", "Server Message",
						javax.swing.JOptionPane.INFORMATION_MESSAGE);
			else
				javax.swing.JOptionPane.showMessageDialog(this, "서버가 실행되지 않았습니다.", "Server Message",
						javax.swing.JOptionPane.ERROR_MESSAGE);
		} catch (Exception e) {
		}
	}

	/** 서버 설정 이벤트 */
	private void logsvMouseClicked(java.awt.event.MouseEvent evt) {
		/** 서버가 실행중이면 */
		if (serverstart) {
			chatlvl.setText("" + Config.GLOBAL_CHAT_LEVEL);
			maxuser.setText("" + Config.MAX_ONLINE_USERS);
			exp.setText("" + Config.RATE_XP);
			adena.setText("" + Config.RATE_DROP_ADENA);
			item.setText("" + Config.RATE_DROP_ITEMS);
			weightlimit.setText("" + Config.RATE_WEIGHT_LIMIT);
			lawful.setText("" + Config.RATE_LAWFUL);
			karma.setText("" + Config.RATE_KARMA);
			enweapon.setText("" + Config.ENCHANT_CHANCE_WEAPON);
			enarmor.setText("" + Config.ENCHANT_CHANCE_ARMOR);

			setting.setLayout(new FlowLayout());
			setting.setSize(340, 200);// 수정한부분
			setting.setLocation(250, 250);
			setting.setResizable(false);
			setting.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			setting.setVisible(true);
		} else {
			javax.swing.JOptionPane.showMessageDialog(this, "서버가 실행되지 않았습니다.", "Server Message",
					javax.swing.JOptionPane.ERROR_MESSAGE);
		}
	}

	/** 서버 로그 초기화 이벤트 */
	private void logclMouseClicked(java.awt.event.MouseEvent evt) {
		if (serverstart) {
			try {
				LoggerInstance.getInstance().flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			javax.swing.JOptionPane.showMessageDialog(this, "서버가 실행되지 않았습니다.", "Server Message",
					javax.swing.JOptionPane.ERROR_MESSAGE);
		}
	}

	/** 서버 설정이벤트 */
	private void setMouseClicked(java.awt.event.MouseEvent evt) {
		int chatlevel = Integer.parseInt(chatlvl.getText());
		short chatlevel2 = (short) chatlevel;
		Config.GLOBAL_CHAT_LEVEL = chatlevel2;
		int Max = Integer.parseInt(maxuser.getText());
		short Max2 = (short) Max;
		Config.MAX_ONLINE_USERS = Max2;
		Float Exprate = Float.parseFloat(exp.getText());
		double Exprate2 = (double) Exprate;
		Config.RATE_XP = Exprate2;
		Float Aden = Float.parseFloat(adena.getText());
		double Aden2 = (double) Aden;
		Config.RATE_DROP_ADENA = Aden2;
		Float Droprate = Float.parseFloat(item.getText());
		double Droprate2 = (double) Droprate;
		Config.RATE_DROP_ITEMS = Droprate2;
		Float weight = Float.parseFloat(weightlimit.getText());
		double weight2 = (double) weight;
		Config.RATE_WEIGHT_LIMIT = weight2;
		Float lawfulrate = Float.parseFloat(lawful.getText());
		double lawful2 = (double) lawfulrate;
		Config.RATE_LAWFUL = lawful2;
		Float karmarate = Float.parseFloat(karma.getText());
		double karma2 = (double) karmarate;
		Config.RATE_KARMA = karma2;
		int armor = Integer.parseInt(enarmor.getText());
		Config.ENCHANT_CHANCE_ARMOR = armor;
		int enchant = Integer.parseInt(enweapon.getText());
		Config.ENCHANT_CHANCE_WEAPON = enchant;
		txtGlobalChat.append("\n          [  Server Setting  ]");
		txtGlobalChat.append("\n    [SET] Chating Level : " + Config.GLOBAL_CHAT_LEVEL);
		txtGlobalChat.append("\n    [SET] Max Online User : " + Config.MAX_ONLINE_USERS);
		txtGlobalChat.append("\n    [SET] ExpRate : " + Exprate2 + " - Adena : " + Aden2 + " - Item : " + Droprate2);
		txtGlobalChat.append(
				"\n    [SET] WeightLimit : " + weight2 + " - LawfulRate : " + lawful2 + " - KarmaRate : " + karma2);
		txtGlobalChat.append("\n    [SET] [Enchant] Weapon : " + enchant + " - Armor : " + armor);
		txtGlobalChat.append("\n       [  Server Setting Compleate ]");
		setting.setVisible(false);
	}

	/** 서버 채팅 이벤트 */
	private void chatKeyPressed(java.awt.event.KeyEvent evt) {

		/** 눌린 키가 Enter 키라면 */
		if (evt.getKeyCode() == evt.VK_ENTER) {
			if (serverstart) {
				/** 채팅내용이 있다면... */
				if (!chat.getText().equalsIgnoreCase("")) {
					/** 특정 캐릭명이 지정되지 않았다면.. 즉 일반채팅이라면 */
					if (name.getText().equalsIgnoreCase("")) {
						/** 전체채팅 블록으로 쏴준다. */
						txtTrade.append("\\aD[******]  :  " + chat.getText());
						/** 월드맵상 존재하는 모든 유저한테 채팅 패킷 전송 */
						L1World world = L1World.getInstance();
						world.broadcastServerMessage("\\aD[******] " + chat.getText());
						/** 특정 캐릭명이 지정이 되었다면.. */
					} else {
						/** 월드맵에서 해당 캐릭명으로 객체를 찾은후 해당 객체에게 채팅 패킷 전송 */
						L1PcInstance pc = L1World.getInstance().getPlayer(name.getText());
						if (pc != null) {
							/** 귓속채팅 블록으로 쏴준다. */
							txtWhisper.append("\n[******] -> [" + name.getText() + "]  :  " + chat.getText());
							pc.sendPackets(new S_SystemMessage("[******] -> " + chat.getText()));
						} else {
							javax.swing.JOptionPane.showMessageDialog(this,
									"월드내에 " + name.getText() + " (은)는 존재하지않습니다.", "Server Message",
									javax.swing.JOptionPane.ERROR_MESSAGE);
						}
					}
					chat.setText("");
				}
			} else {
				javax.swing.JOptionPane.showMessageDialog(this, "서버가 실행되지 않았습니다.", "Server Message",
						javax.swing.JOptionPane.ERROR_MESSAGE);
				chat.setText("");
			}
		}

	}

	/** 특정 캐릭명을 지정할수 있게 -> 일단 캐릭리스트 창에서 선택할수 있도록 코딩 -> chat TextField와 연계 */
	private void userlistActionPerformed(java.awt.event.ActionEvent evt) {
		/** 마우스가 가르킨 이벤트 값.. 즉 리턴받는 값이 있다면.. 만약을 위해 */
		if (!evt.getActionCommand().equalsIgnoreCase("")) {
			/** name 필드 셋팅 */
			name.setText(evt.getActionCommand());
		}
	}

	/** 캐릭 리스트 팝업메뉴 */
	private void userlistMouseClicked(java.awt.event.MouseEvent evt) {
		if (evt.getButton() == evt.BUTTON3) {
			popmenu1.show(userlist, evt.getX(), evt.getY());
		}
	}

	/** 아이피 리스트 팝업메뉴 */
	private void iplistMouseClicked(java.awt.event.MouseEvent evt) {
		if (evt.getButton() == evt.BUTTON3) {
			popmenu2.show(iplist, evt.getX(), evt.getY());
		}
	}

	/** 팝업메뉴 - 개인 강제추방 */
	private void menu1ActionPerformed(java.awt.event.ActionEvent evt) {
		/** 서버가 실행중이면 */
		if (serverstart) {
			if ((!userlist.getSelectedItem().equalsIgnoreCase("")) && (userlist.getSelectedItem() != null)) {
				try {
					L1PcInstance players = L1World.getInstance().getPlayer(userlist.getSelectedItem());
					if (players != null) {
						GameServer.disconnectChar(userlist.getSelectedItem());
						javax.swing.JOptionPane.showMessageDialog(this, players.getName() + "를 강제추방 하였습니다.",
								"Server Message", javax.swing.JOptionPane.INFORMATION_MESSAGE);
						userlist.remove(userlist.getSelectedItem());
					} else {
						javax.swing.JOptionPane.showMessageDialog(this,
								"월드내에 " + userlist.getSelectedItem() + " (은)는 존재하지않습니다.", "Server Message",
								javax.swing.JOptionPane.ERROR_MESSAGE);
					}
				} catch (Exception e) {
					javax.swing.JOptionPane.showMessageDialog(this, userlist.getSelectedItem() + "의 강제추방이 실패하였습니다.",
							"Server Message", javax.swing.JOptionPane.INFORMATION_MESSAGE);
				} finally {
				}
			} else {
				javax.swing.JOptionPane.showMessageDialog(this, "캐릭명이 지정되지 않았습니다.", "Server Message",
						javax.swing.JOptionPane.ERROR_MESSAGE);
			}
			/** 서버가 실행중이지 않다면 */
		} else {
			javax.swing.JOptionPane.showMessageDialog(this, "서버가 실행되지 않았습니다.", "Server Message",
					javax.swing.JOptionPane.ERROR_MESSAGE);
		}
	}

	/** 팝업메뉴 - 개인 아이피 차단 */
	private void menu2ActionPerformed(java.awt.event.ActionEvent evt) {
		/** 서버가 실행중이면 */
		if (serverstart) {
			if ((!userlist.getSelectedItem().equalsIgnoreCase("")) && (userlist.getSelectedItem() != null)) {
				L1PcInstance pc = L1World.getInstance().getPlayer(userlist.getSelectedItem());
				if (pc != null) {
					if (pc.getNetConnection() != null) {
						IpTable.getInstance().banIp(pc.getNetConnection().getIp());
						iplist.add(pc.getNetConnection().getIp());
					}
					GameServer.disconnectChar(pc);
					userlist.remove(userlist.getSelectedItem());
					javax.swing.JOptionPane.showMessageDialog(this, pc.getName() + "를 강제추방, 밴  하였습니다.",
							"Server Message", javax.swing.JOptionPane.INFORMATION_MESSAGE);
				} else {
					javax.swing.JOptionPane.showMessageDialog(this,
							"월드내에 " + userlist.getSelectedItem() + " (은)는 존재하지않습니다.", " Server Message",
							javax.swing.JOptionPane.ERROR_MESSAGE);
				}
			}
			/** 서버가 실행중이지 않다면 */
		} else {
			javax.swing.JOptionPane.showMessageDialog(this, "서버가 실행되지 않았습니다.", "Server Message",
					javax.swing.JOptionPane.ERROR_MESSAGE);
		}
	}

	/** 팝업메뉴 - 개인 캐릭터 정보 */
	private void menu3ActionPerformed(java.awt.event.ActionEvent evt) {
		/** 서버가 실행중이면 */
		if (serverstart) {
			if (!inf) {
				L1PcInstance pc = L1World.getInstance().getPlayer(userlist.getSelectedItem());
				if (pc != null) {
					new infomation(pc);
				}
			} else {
				javax.swing.JOptionPane.showMessageDialog(this, "이미 실행중입니다.", "Server Message",
						javax.swing.JOptionPane.ERROR_MESSAGE);
			}
			/** 서버가 실행중이지 않다면 */
		} else {
			javax.swing.JOptionPane.showMessageDialog(this, "서버가 실행되지 않았습니다.", "Server Message",
					javax.swing.JOptionPane.ERROR_MESSAGE);
		}
	}

	/** 팝업메뉴 - 차단 아이피 삭제 리스트 */
	private void menu4ActionPerformed(java.awt.event.ActionEvent evt) {
		/** 서버가 실행중이면 */
		if (serverstart) {
			iplist.remove(iplist.getSelectedItem());
			javax.swing.JOptionPane.showMessageDialog(this, "차단리스트(List) 삭제:" + iplist.getSelectedItem(),
					"Server Message", javax.swing.JOptionPane.INFORMATION_MESSAGE);
			/** 서버가 실행중이지 않다면 */
		} else {
			javax.swing.JOptionPane.showMessageDialog(this, "서버가 실행되지 않았습니다.", "Server Message",
					javax.swing.JOptionPane.ERROR_MESSAGE);
		}
	}

	/** 팝업메뉴 - 차단 아이피 삭제 디비 */
	private void menu5ActionPerformed(java.awt.event.ActionEvent evt) {
		/** 서버가 실행중이면 */
		if (serverstart) {
			IpTable.getInstance().liftBanIp(iplist.getSelectedItem());
			iplist.remove(iplist.getSelectedItem());
			javax.swing.JOptionPane.showMessageDialog(this, "차단리스트(dB, List) 삭제:" + iplist.getSelectedItem(),
					"Server Message", javax.swing.JOptionPane.INFORMATION_MESSAGE);
			/** 서버가 실행중이지 않다면 */
		} else {
			javax.swing.JOptionPane.showMessageDialog(this, "서버가 실행되지 않았습니다.", "Server Message",
					javax.swing.JOptionPane.ERROR_MESSAGE);
		}
	}

	/** 팝업메뉴 - 귓말하기 */
	private void menu7ActionPerformed(java.awt.event.ActionEvent evt) {
		/** 서버가 실행중이면 */
		if (serverstart) {
			if (!inf) {
				L1PcInstance pc = L1World.getInstance().getPlayer(userlist.getSelectedItem());
				if (pc != null) {
					name.setText(pc.getName());
				}
			} else {
				javax.swing.JOptionPane.showMessageDialog(this, "이미 실행중입니다.", "Server Message",
						javax.swing.JOptionPane.ERROR_MESSAGE);
			}
			/** 서버가 실행중이지 않다면 */
		} else {
			javax.swing.JOptionPane.showMessageDialog(this, "서버가 실행되지 않았습니다.", "Server Message",
					javax.swing.JOptionPane.ERROR_MESSAGE);
		}
	}

	/** 팝업메뉴 - 선물 주기 */
	private void menu6ActionPerformed(java.awt.event.ActionEvent evt) {
		/** 서버가 실행중이면 */
		if (serverstart) {
			if (!inf) {
				L1PcInstance pc = L1World.getInstance().getPlayer(userlist.getSelectedItem());
				if (pc != null) {
					new Give(pc);
				}
			} else {
				javax.swing.JOptionPane.showMessageDialog(this, "이미 실행중입니다.", "Server Message",
						javax.swing.JOptionPane.ERROR_MESSAGE);
			}
			/** 서버가 실행중이지 않다면 */
		} else {
			javax.swing.JOptionPane.showMessageDialog(this, "서버가 실행되지 않았습니다.", "Server Message",
					javax.swing.JOptionPane.ERROR_MESSAGE);
		}
	}

	public static void main(String args[]) {
		/** 쓰레드로 이벤트 쿼리로 담당한다. */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				Config.load();
				GrangKinConfig.load();
				new chocco().setVisible(true);
			}
		});
	}

	private static final double perToheight = 3.3D;
	public static long cpu;

	class PerformReader implements Runnable {
		@Override
		public void run() {
			try {
				while (true) {
					// TODO 매니져 동기화시간
					Thread.sleep(500L);
					long mem = SystemUtil.getUsedMemoryMB();
					int thread = Thread.activeCount();
					cpu = (int) (getUseCpu() * 100D);
					label4.setText(String.format("Memory : %d", mem));
					lblThread.setText(String.format("Thread : %d", thread));
					lblCPU.setText(String.format("CPU : %d%%", cpu));
					lblCPUProgressBack.setBounds(0, 0, 50, 330 - (int) (cpu * perToheight));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		private double getUseCpu() {
			return ((OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean()).getSystemCpuLoad();
		}
	}
}
/**
 * 차후 이벤트, 유저 채금, 차단, 추방, 변신, 아이템 등 이벤트관련은 팝업메뉴 혹은 작은 단추형식의 별도의 컴포넌트 제공으로 기존
 * 인터페이스에 크게 벗어나지 않는 범위로 제작한다
 */