package l1j.server;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.sql.SQLException;
import java.util.logging.LogManager;

import l1j.server.MJNetServer.MJNetServerLoadManager;
import l1j.server.server.GameServer;
import l1j.server.server.Controller.LoginController;
import l1j.server.server.monitor.LoggerInstance;
import l1j.server.server.utils.PerformanceTimer;

/**
 * l1j-jp의 서버를 기동한다.
 */
public class Server {
	/** 로그 설정 파일의 폴더.  */
	private static final String LOG_PROP = "./config/log.properties";

	private static volatile Server uniqueInstance;

	public static Server createServer()
	{
		if (uniqueInstance == null) {
			synchronized (Server.class) {
				if (uniqueInstance == null) {
					uniqueInstance = new Server();
				}
			}
		}

		return uniqueInstance;
	}

	public static void startLoginServer() {
		LoginController.getInstance().setMaxAllowedOnlinePlayers(Config.MAX_ONLINE_USERS);
		MJNetServerLoadManager.getInstance().run();
	}

	public void changePort(int port){
		MJNetServerLoadManager.reload();
	}
	
	public void shutdown() {
		GameServer.getInstance().shutdown();		
	}
	
	public static PrintStream _dout;
	
	public class ConsoleScanner implements Runnable {
		@Override
		public void run() {
			try {
				// test
				ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);
				_dout = System.out;
				PrintStream ps = new PrintStream(baos);
	            System.setOut(ps);
	            System.setErr(ps);
				while (true) {
					Thread.sleep(1000L);
					String s = null;
					synchronized(baos){
						s = baos.toString();
						baos.reset();
					}

					if(s == null)
						continue;
					s = s.trim();
					if(s.length() > 0){
						String msg = String.format("%s\r\n", s);
						LoggerInstance.getInstance().addCmd(msg);
						_dout.print(msg);
					}
				}
			} catch (InterruptedException e) {
			} finally  {
			}
		}
	}

	/**
	 * 서버 메인.
	 *
	 * @param args
	 *            커멘드 라인 인수
	 * @throws SQLException 
	 * @throws Exception
	 */
	public Server()  {
		new Thread(new ConsoleScanner()).start();
		initLogManager();
		initDBFactory();
		try {
			PerformanceTimer timer = new PerformanceTimer();
			timer.reset();
			timer = null;
			/*startGameServer();
			startLoginServer();*/
		} catch (Exception e) {}
	}

	public static void startGameServer() {
		OpeningMent();
		try {
			GameServer.getInstance().initialize();
		} catch(Exception e) {
			e.printStackTrace();
		};
		ClosingMent();
	}

	private void initLogManager() {
		File logFolder = new File("log");
		logFolder.mkdir();

		try {
			InputStream is = new BufferedInputStream(new FileInputStream(LOG_PROP));
			LogManager.getLogManager().readConfiguration(is);
			is.close();
		} catch (IOException e) {
			System.exit(0);
		}
		try {
			//Config.load();
		} catch (Exception e) {
			System.exit(0);
		}
	}
	
	private static void OpeningMent() {
		System.out.println("-------------------------------------------------------------------------------");
		System.out.println("[럭키서버] version 1.0");
		System.out.println("[럭키서버] 김행운");
		System.out.println("[럭키서버] 푸들");
		System.out.println("-------------------------------------------------------------------------------");
	}

	private static void ClosingMent() {
		try {
			System.out.println("-------------------------------------------------------------------------------");
			System.out.println(" [SERVER IP] => " + InetAddress.getLocalHost().getHostAddress() + "");
			System.out.println(" [SERVER PORT] => " + Config.GAME_SERVER_PORT + "");
			System.out.println("-------------------------------------------------------------------------------");
			System.out.println(" [배	율] 경험치: x" + Config.RATE_XP + " / 아데나: x" + Config.RATE_DROP_ADENA + " / 아이템: x"
					+ Config.RATE_DROP_ITEMS);
			System.out.println("-------------------------------------------------------------------------------");
			System.out.println(" [인챈트] 무기: +" + Config.ENCHANT_CHANCE_WEAPON + "% / 방어구: +"
					+ Config.ENCHANT_CHANCE_ARMOR + "% / 장신구: +" + Config.ENCHANT_CHANCE_ACCESSORY + "%");
			System.out.println("-------------------------------------------------------------------------------");
		} catch (Exception localException) {
		}
	}

	private void initDBFactory() {// L1DatabaseFactory 초기설정
		L1DatabaseFactory.setDatabaseSettings(Config.DB_DRIVER, Config.DB_URL,
				Config.DB_LOGIN, Config.DB_PASSWORD);
		try {
			L1DatabaseFactory.getInstance();
		} catch(Exception e) {};
	}

}
