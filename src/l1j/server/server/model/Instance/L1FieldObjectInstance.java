package l1j.server.server.model.Instance;

import l1j.server.server.Controller.CrockController;
import l1j.server.server.model.L1World;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.serverpackets.S_ChatPacket;
import l1j.server.server.serverpackets.S_Message_YN;
import l1j.server.server.serverpackets.S_RemoveObject;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.templates.L1Npc;

public class L1FieldObjectInstance extends L1NpcInstance {

	private static final long serialVersionUID = 1L;
	private int moveMapId;

	public L1FieldObjectInstance(L1Npc template) {
		super(template);
	}

	@Override
	public void onAction(L1PcInstance pc) {  }

	@Override
	public void onTalkAction(L1PcInstance pc) {
		int npcid = getNpcTemplate().get_npcId();

		switch(npcid){
		/** �ð��� �տ� **/
		case 200:
			if (CrockController.getInstance().isTimeCrock()) {
				if (CrockController.getInstance().crocktype() == 0) {
//					L1Teleport.teleport(pc, 32639, 32876, (short) 780, 4, false);// �׺�
					pc.start_teleport(32639, 32876, 780, 5, 169, false, false);
				} else {
//					L1Teleport.teleport(pc, 32794, 32751, (short) 783, 4, false);// ƼĮ
					pc.start_teleport(32794, 32751, 783, 5, 169, false, false);
				}
			} else {
				pc.sendPackets(new S_ChatPacket(pc, "�ð��� �տ��� ���� �����ֽ��ϴ�."));
				pc.sendPackets(new S_ChatPacket(pc, "���½ð��� �������� 7�� �Դϴ�."));
			}
			break;
		case 7210011: //�߶�ī���� �Ƚ�ó
			telValakasRoom(pc); 
			break;
		default: 
			break; 
		}
	}

	/** ������ ���� 32���� �Ѵ��� üũ�ؼ� �ڽ�Ų�� @param pc @param mapid 
	 * 	1626: �巡���� ������ �� ������ ǳ�����ϴ�. ������ ������ ����� ������ �巡�� ��Ż�� ���� �� �� �����ϴ�.*/
	private void DragonRaidMap(L1PcInstance pc, int mapid){
		int count = 0;
		for(L1PcInstance player : L1World.getInstance().getAllPlayers()){
			if(player == null)
				continue;
			if(player.getMapId() == mapid){
				count += 1;
				if(count > 31)
					return;
			}
		}

		switch(getNpcTemplate().get_npcId()) {
		case 900007:
			if (pc.hasSkillEffect(L1SkillId.ANTA_BUFF) || pc.hasSkillEffect(L1SkillId.FAFU_BUFF) || pc.hasSkillEffect(L1SkillId.RIND_BUFF)){
				pc.sendPackets(new S_ServerMessage(1626)); 
				return; 
			} 
//			if(AntarasRaidSystem.getInstance().getAR(mapid).isAntaras()){
//				pc.sendPackets(new S_ServerMessage(1537));// �巡���� ���� ���� ���Ѵ�
//				return;
//			} else {
				pc.sendPackets(new S_Message_YN(2923, ""));
				pc.DragonPortalLoc[0] = 32600;
				pc.DragonPortalLoc[1] = 32741;
				pc.DragonPortalLoc[2] = mapid;
//			}
			break;
		case 900036:
			if (pc.hasSkillEffect(L1SkillId.ANTA_BUFF) || pc.hasSkillEffect(L1SkillId.FAFU_BUFF) || pc.hasSkillEffect(L1SkillId.RIND_BUFF)){
				pc.sendPackets(new S_ServerMessage(1626)); 
				return; 
			} 
//			if(FafurionRaidSystem.getInstance().getAR(mapid).isFafurion()){
//				pc.sendPackets(new S_ServerMessage(1537));// �巡���� ���� ���� ���Ѵ�
//				return;
//			} else {
				pc.sendPackets(new S_Message_YN(2923, ""));
				pc.DragonPortalLoc[0] = 32976;
				pc.DragonPortalLoc[1] = 32743;
				pc.DragonPortalLoc[2] = mapid;
//			}
			break;
		}
	}
	
	private void telValakasRoom(L1PcInstance pc) {
		pc.start_teleport(32833, 32757, getMapId(), 5, 169, false, false);
		pc.isInValakasBoss = true;
	}
	/** �̵��� ���� �����Ѵ�. @param id */
	public void setMoveMapId(int id){ moveMapId = id; }

	@Override
	public void deleteMe() {
		_destroyed = true;
		if (getInventory() != null) {
			getInventory().clearItems();
		}
		L1World.getInstance().removeVisibleObject(this);
		L1World.getInstance().removeObject(this);
		for (L1PcInstance pc : L1World.getInstance().getRecognizePlayer(this)) {
			pc.removeKnownObject(this);
			pc.sendPackets(new S_RemoveObject(this));
		}
		removeAllKnownObjects();
	}
}
