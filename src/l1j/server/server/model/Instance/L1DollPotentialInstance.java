package l1j.server.server.model.Instance;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import l1j.server.L1DatabaseFactory;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.utils.SQLUtil;

public class L1DollPotentialInstance {
	String strRank = null;
	String strPotential = null;
	public L1DollPotentialInstance() {	}
	
	public void getPotential(int i) {  
		
		}
	
	
	public String find_potentialRank(int pi) {
		//System.out.println(pi);
		
		int rank = 0;
		
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			boolean error = false;

			//pc.sendPackets(new S_SystemMessage(" "));

			
			statement = con.prepareStatement("SELECT Rank FROM dollpotentiallist WHERE PotentialIdx =" + pi );
			
			
			if (error == false) {
				rs = statement.executeQuery();
				
				while (rs.next()) {
					rank = rs.getInt(1);
					
										
					if (rank == 1) {
						strRank = "\\fk[잠재력 강화:일반]";
					} else if (rank == 2) {
						strRank = "\\fv[잠재력 강화:고급]";
					}else if (rank == 3) {
						strRank = "\\fa[잠재력 강화:희귀]";
					}else if (rank == 4) {
						strRank = "\\fq[잠재력 강화:영웅]";
					} else if (rank == 5) {
						strRank = "\\fp[잠재력 강화:전설]";
					}					
				}
				
			}
			
			
		} catch (Exception e) {
		} finally {
			SQLUtil.close(rs, statement, con);
		}
		return strRank;
	}
	
public String find_potential(int pi) {
		
		
		int rank = 0;
		
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			boolean error = false;

			//pc.sendPackets(new S_SystemMessage(" "));

			
			statement = con.prepareStatement("SELECT Potential FROM dollpotentiallist WHERE PotentialIdx =" + pi );
			
			
			if (error == false) {
				rs = statement.executeQuery();
				
				while (rs.next()) {
					strPotential = rs.getString(1);										
									
				}
				
			}
			
			
		} catch (Exception e) {
		} finally {
			SQLUtil.close(rs, statement, con);
		}
		return strPotential;
	}

}
