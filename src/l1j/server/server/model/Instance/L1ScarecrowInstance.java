package l1j.server.server.model.Instance;

import java.util.ArrayList;

import l1j.server.Config;
import l1j.server.server.model.L1Attack;
import l1j.server.server.serverpackets.S_ChangeHeading;
import l1j.server.server.serverpackets.S_NewCreateItem;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.templates.L1Npc;
import l1j.server.server.utils.CalcExp;

public class L1ScarecrowInstance extends L1NpcInstance {

	private static final long serialVersionUID = 1L;

	public L1ScarecrowInstance(L1Npc template) {
		super(template);
	}

	@Override
	public void onAction(L1PcInstance player) {
		L1Attack attack = new L1Attack(player, this);
		if (player.isInParty() || player.isDead()) { 
			player.sendPackets("파티중에는 허수아비를 공격할 수 없습니다.");
			return;
		}
		if (attack.calcHit() && player.getAI() == null) {
			if (player.getLevel() < Config.SCARELEVEL && !player.noPlayerCK) {
				ArrayList<L1PcInstance> targetList = new ArrayList<L1PcInstance>();
				targetList.add(player);
				ArrayList<Integer> hateList = new ArrayList<Integer>();
				hateList.add(1);
				CalcExp.calcExp(player, getId(), targetList, hateList, getExp());
			}
			if (player.getLevel() >= 1 && !player.noPlayerCK) {// 탐지급 레벨
				player.getInventory().storeItem(41302, Config.tamsc1);
				player.getInventory().storeItem(40308, Config.tamsc2);
				player.sendPackets(new S_NewCreateItem(S_NewCreateItem.TAM_POINT, player.getNetConnection()), true);// 탐지급
				player.getNetConnection().getAccount().tam_point += Config.tamsc;// 탐지급갯수
				player.getNetConnection().getAccount().updateTam();// 탐업뎃
			}
			int dmg = attack.calcDamage();
			if (this.getNpcId() == 7320088) {
				player.sendPackets(new S_SystemMessage("물리대미지: [" + dmg + "]"));
			}
			if (getHeading() < 7) {
				setHeading(getHeading() + 1);
			} else {
				setHeading(0);
			}
			broadcastPacket(new S_ChangeHeading(this));
		}
		attack.action();
	}

	@Override
	public void onTalkAction(L1PcInstance l1pcinstance) {}
	public void onFinalAction() {}
	public void doFinalAction() {}
}
