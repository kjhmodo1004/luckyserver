package l1j.server.server.model.item.function.Change;


import l1j.server.Config;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_PremiumShopSellList1;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.utils.TYUtil;

public class ChangeItem1_1
{
  public static void ClickToItem(L1PcInstance pc, L1ItemInstance item)
  {
	  
    int itemid = item.getItemId();
    int type = -1;
    if (pc.getInventory().getSize() >= 170)
    {
      pc.sendPackets(new S_SystemMessage("아이템 사용 실패 : 인벤토리 갯수 줄인 후 사용가능"));
      return;
    }
    if (item.getEndTime() != null)
    {
      pc.sendPackets(new S_SystemMessage("\\fW시간제 아이템은 불가능합니다."));
      return;
    }
    int[] bossIds = TYUtil.GetIntTokenizeIntArray(Config.ChangeItemList1);
    for (int i = 0; i < bossIds.length; i++) {
      if (itemid == bossIds[i]) {
        type = 0;
      }
    }
    
    if (type == -1)
    {
      pc.sendPackets(new S_SystemMessage("교환할 수 없는 아이템입니다"));
    }
    else
    {
    	pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\f3변경창을 취소 할 경우, 원활한 상점이용을 위해 리스한번 해주시길 바랍니다."));
    	pc.sendPackets(
				new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\f3리스를 안 할 경우, 발생한 불이익은 책임지지 않습니다."));
      pc.sendPackets(new S_PremiumShopSellList1(true, type));
      pc.setIsChangeItem1(true);
      pc.setDefaultItem1(item);
    }
  }
}
