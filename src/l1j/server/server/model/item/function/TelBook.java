package l1j.server.server.model.item.function;

import static l1j.server.server.model.skill.L1SkillId.BONE_BREAK;
import static l1j.server.server.model.skill.L1SkillId.EARTH_BIND;
import static l1j.server.server.model.skill.L1SkillId.ICE_LANCE;
import static l1j.server.server.model.skill.L1SkillId.SHOCK_STUN;
import static l1j.server.server.model.skill.L1SkillId.THUNDER_GRAB;

import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.serverpackets.S_SystemMessage;


public class TelBook {
	
	public static void clickItem(L1PcInstance pc, int itemId, int BookTel, L1ItemInstance l1iteminstance) {
		if (pc.isParalyzed() || pc.isSleeped() || pc.isDead()) {
			return;
		}
		if (!pc.getMap().isEscapable()) {
			pc.sendPackets(new S_SystemMessage("������ �˼����� ���¿����� �ڷ���Ʈ�� �� �� �����ϴ�."));
			return;
		}
		
		
		if ((pc.hasSkillEffect(SHOCK_STUN)) 
				|| pc.hasSkillEffect(L1SkillId.EMPIRE)
				|| (pc.hasSkillEffect(ICE_LANCE)) || (pc.hasSkillEffect(BONE_BREAK))
				|| (pc.hasSkillEffect(THUNDER_GRAB)) || (pc.hasSkillEffect(EARTH_BIND))) {
			return;
		}
		if (itemId == 560025) {
			try {
				final int[][] �������å = { 
						{ 34060, 32281, 4 }, // ����
						{ 33079, 33390, 4 }, // �����
						{ 32750, 32439, 4 }, // ��ũ��
						{ 32612, 33188, 4 }, // ���ٿ��
						{ 33720, 32492, 4 }, // ����
						{ 32872, 32912, 304 }, // ħ���� ����
						{ 32612, 32781, 4 }, // �۷���
						{ 33067, 32803, 4 }, // ��Ʈ
						{ 33933, 33358, 4 }, // �Ƶ�
						{ 33601, 33232, 4 }, // ���̳�
						{ 32574, 32942, 0 }, // ���ϴ� ��
						{ 33430, 32815, 4 }, }; // ���
				int[] a = �������å[BookTel];
				if (a != null) {
					pc.start_teleport(a[0], a[1], a[2], pc.getHeading(), 169, true, true);
					pc.getInventory().removeItem(l1iteminstance, 1);
				}
			} catch (Exception e) {}
		} else if (itemId == 560027) {
			try {
				final int[][] ������� = { 
						/** �� ���� ��õ ����� **/
						{ 34266, 32190, 4 }, // �׽��Ա�
						{ 32507, 32924, 9 }, // ���� ���� ��
						{ 32491, 32854, 9 }, // ���� ���� �Ա�
						{ 32409, 32938, 9 }, // ���� ��ũ ���� ����
						{ 32883, 32647, 4 }, // ���� ������ ����
						{ 32875, 32927, 4 }, // ���� ������ ����
						{ 32726, 32928, 4 }, // �۴�1���Ա�
						{ 32764, 32840, 77 }, // ���ž4��
						{ 32708, 33150, 9 }, // ���� ���� ���� ����
						{ 32599, 32289, 4 }, // ���� ��ũ �ζ�
						{ 32908, 33222, 4 }, // ���� �縷(�����ں�)
						{ 32761, 33167, 4 }, // ���� �縷(�����)
						{ 32806, 32726, 19 }, // ���� �� ���� 1��
						{ 32796, 32753, 809 }, // �۴�3���Ա�
						{ 33429, 32826, 4 }, // ��� ���� �Ա�
						{ 32809, 32729, 25 }, // ���� ���� 1��
						/** �� ���� ��õ ����� **/
						{ 32745, 32427, 4 }, // ����
						{ 33764, 33314, 4 }, // ���� �ſ��� ��
						{ 33804, 32966, 4 }, // ���� �и� ����
						{ 32710, 32790, 59 }, // ���ٿձ� 1��
						{ 34251, 33453, 4 }, // ������ ž �Ա�
						{ 32811, 32909, 4 }, // ���� ���� ��� ����
						{ 32766, 32798, 20 }, // ���� �� ���� 2��
						{ 32726, 32808, 61 }, // ���� �ձ� 3��
						{ 32809, 32808, 30 }, // ���� ���� 1��
						{ 32809, 32767, 27 }, // ���ô��� 3��
						{ 32801, 32928, 800 }, //
						/** �� ���� ��õ ����� **/
						{ 32705, 32822, 32 }, // ���� ��� ���� 3��
						{ 33436, 33475, 4 }, // ���̳� ������ �� ��ǥ��
						{ 33182, 33006, 4 }, // ���� ������� ����
						{ 34126, 32799, 4 }, // ���� ǳ���� ����
						{ 34126, 32192, 4 }, // ���� ���� ����
						{ 33331, 32459, 4 }, // ���� ���� ��� �Ա�
						{ 34051, 32561, 4 }, // ���� ����� ������
						{ 33643, 32419, 4 }, // ���� ȭ���� ���� �Ա�
				};
				int[] b = �������[BookTel];
				if (b != null) {
					pc.start_teleport(b[0], b[1], b[2], pc.getHeading(), 169, true, true);
					pc.getInventory().removeItem(l1iteminstance, 1);
				}
			} catch (Exception e) {
				
			}

		} else if (itemId == 560028) {
			try {
				final int[][] ������� = { 
						{ 32735, 32798, 101 }, // ����1
						{ 32727, 32803, 102 }, // ����2
						{ 32726, 32803, 103 }, // ����3
						{ 32620, 32859, 104 }, // ����4
						{ 32601, 32866, 105 }, // ����5
						{ 32611, 32863, 106 }, // ����6
						{ 32618, 32866, 107 }, // ����7
						{ 32602, 32867, 108 }, // ����8
						{ 32613, 32866, 109 }, // ����9
						{ 32730, 32803, 110 }, // ����10
						{ 32646, 32808, 111 }, // �������� ��������
						{ 32801, 32963, 111 },};// �������� �߰�����
				int[] c = �������[BookTel];
				if (c != null) {
					pc.start_teleport(c[0], c[1], c[2], pc.getHeading(), 169, true, true);

				}
			} catch (Exception e) {}
			
		} else if (itemId == 560029) {
			try {
				final int[][] ������ = {
						{ 0, 0, 0 }, /** �� ���� ��õ ����� **/
						{ 32643, 32841, 9 }, // ���� ���� ��
						{ 32477, 32855, 9 }, // ���� ���� �Ա�
						{ 32437, 32911, 9 }, // ���� ��ũ ���� ����
						{ 32874, 32653, 4 }, // ������ ����
						{ 32879, 32895, 4 }, // ������ ����
						{ 32724, 32925, 4 }, // �۷������� �Ա�1��
						{ 32764, 32841, 77 }, // ���ž 4���Ա�
						{ 32706, 33153, 9 }, // ���� ���� ���� ����
						{ 32723, 32398, 4 }, // ���� ��ũ �ζ�
						{ 32895, 33242, 4 }, // �����ں�
						{ 32767, 33164, 4 }, // �����
						{ 32937, 32277, 4 }, // ���� �� ���� 1�� �Ա�
						{ 32798, 32754, 809 }, // �۷������� �Ա�3��
						{ 33430, 32825, 4 }, // ������� 1�� �Ա�
						{ 32808, 32730, 25 }, // ���ô��� 1�� �Ա�
						
						{ 0, 0, 0 }, /** �� ���� ��õ ����� **/
						{ 33768, 33312, 4 }, // ���� �ſ｣ ����
						{ 33800, 32844, 4 }, // ���� �и�����
						{ 33628, 33505, 4 }, // ���� �ձ� 1�� �Ա�
						{ 34247, 33453, 4 }, // ������ ž �Ա� 1��
						{ 32900, 32909, 4 }, // ���� ���� ��� ����
						{ 32766, 32796, 20 }, // ���� �� ���� 3�� �Ա�
						{ 32726, 32807, 61 }, // ���ٿձ� 3��
						{ 32809, 32808, 30 }, // ���� ���� 1��
						{ 32809, 32767, 27 }, // ���ô��� 3��
						
						{ 0, 0, 0 }, /** �� ���� ��õ ����� **/
						{ 32706, 32821, 32 }, // ���� ���� 3��
						{ 33432, 33497, 4 }, // ���̳� �������� ��ǥ
						{ 33159, 32969, 4 }, // ������� ����
						{ 34125, 32799, 4 }, // ���� ǳ���� ���� �Ա�
						{ 34127, 32192, 4 }, // ���� ���� ���� �Ա�
						{ 33330, 32458, 4 }, // ���� ���� ��� �Ա�
						{ 34056, 32547, 4 }, // ���� ����� ������
						{ 33645, 32418, 4 },  }; // ���� ȭ���� ���� �Ա�
				int[] c = ������[BookTel];
				if (c != null) {
					pc.start_teleport(c[0], c[1], c[2], pc.getHeading(), 169, true, true);
					pc.getInventory().removeItem(l1iteminstance, 1);
				}
			} catch (Exception e) {}

		} else if (itemId == 4100135) {
			try {
				final int[][] ruletalisman = { 
						{ 32728, 32800, 12852 }, // ������ ž 1��
						{ 32721, 32803, 12853 }, // ������ ž 2��
						{ 32724, 32802, 12854 }, // ������ ž 3��
						{ 32597, 32863, 12855 }, // ������ ž 4��
						{ 32592, 32866, 12856 }, // ������ ž 5��
						{ 32602, 32865, 12857 }, // ������ ž 6��
						{ 32604, 32866, 12858 }, // ������ ž 7��
						{ 32593, 32866, 12859 }, // ������ ž 8��
						{ 32602, 32866, 12860 }, // ������ ž 9��
						{ 32732, 32802, 12861 }, // ������ ž 10��
						};
				int[] c = ruletalisman[BookTel];
				if (c != null) {
					pc.start_teleport(c[0], c[1], c[2], pc.getHeading(), 169, true, true);
				}
			} catch (Exception e) {}
		}
	}
	
}
