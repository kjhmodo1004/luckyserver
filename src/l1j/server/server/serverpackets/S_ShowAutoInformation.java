package l1j.server.server.serverpackets;

import l1j.server.server.Opcodes;
import l1j.server.server.model.Instance.L1PcInstance;

public class S_ShowAutoInformation extends ServerBasePacket {
	private static final String S_ShowAutoInformation = "[S] S_SHOW_AUTOINFORMATION";
	public S_ShowAutoInformation(L1PcInstance pc) {
		buildPacket(pc);
	}

	private void buildPacket(L1PcInstance pc) {
		writeC(Opcodes.S_BOARD_READ);
		writeD(0);
		writeS("�ڵ����");
		writeS("��� ������");
		writeS("");

		StringBuffer type1 = new StringBuffer();		
		String type1Potion = "   ���� : ü�� ȸ����(500��)\r\n";
		String type2Potion = "   ���� : ���� ü�� ȸ����(500��)\r\n";		
		String etcItem = "  �ֹ��� : �����̵� (300��)\r\n" +
				"  �ֹ��� : ������� ��ȯ(5��)\r\n" +
				"  �ֹ��� : ���� �ֹ���(20��)";

		if(pc.isCrown()) {
			type1.append("   �ͱ� : ����(5��)\r\n");
			type1.append("   ��� : �Ǹ��� ��(30��)\r\n");
		}else if(pc.isKnight()) {
			type1.append("   �ͱ� : ����(5��)\r\n");
			type1.append("   ��� : ���� ���(5��)\r\n");
		}else if(pc.is����()) {
			type1.append("   �ͱ� : ����(5��)\r\n");
			type1.append("   ��� : ���� ���(5��)\r\n");		
			type1.append("   ��� : ����ü(2000)��\r\n");	
		}else if(pc.isDragonknight()) {
			type1.append("   �ͱ� : ����(5��)\r\n");
			type1.append("   ��� : ������ ������(100��)\r\n");
		}else if(pc.isElf()) {
			if(pc.getWeapon() == null || pc.getWeapon().getItem().getType1() != 20) {
				type1.append("   �ͱ� : ����(5��)\r\n");
				type1.append("   ��� : ���ɿ�(100��)\r\n");
				type1.append("   ��� : �������(30��)\r\n");
			}else {
				type1.append("   �ͱ� : ����(5��)\r\n");
				type1.append("   ��� : ���� ����(5��)\r\n");
				type1.append("   ȭ�� : �� ȭ��(3000��)\r\n");
			} 
		}else if(pc.isDarkelf()) {
			type1.append("   �ͱ� : ����(5��)\r\n");
			type1.append("   ��� : ��伮(100��)\r\n");
		}else if(pc.isWizard()) {
			type1.append("   �ͱ� : ����(5��)\r\n");
			type1.append("   ���� : ���� ����(3��)\r\n");
			type1.append("   ���� : ���� ����(3��)\r\n");
			type1.append("   ��� : ������ ��(100��)\r\n");
		}else if(pc.isBlackwizard()) {
			type1.append("   �ͱ� : ����(5��)\r\n");
			type1.append("   ���� : ���� ����(3��)\r\n");
			type1.append("   ��� : ���׵��(300��)\r\n");			
		}

		writeS("Ÿ�� 1.\r\n" +
				type1Potion +
				type1.toString()  +
				etcItem + "\r\n" +
				"Ÿ�� 2.\r\n" +
				type2Potion +
				type1.toString()+
				etcItem);//����
	}

	@Override
	public byte[] getContent() {
		return getBytes();
	}

	public String getType() {
		return S_ShowAutoInformation;
	}
}