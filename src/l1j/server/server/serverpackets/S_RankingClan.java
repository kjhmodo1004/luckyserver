package l1j.server.server.serverpackets;

import l1j.server.server.Opcodes;
import l1j.server.server.model.L1ClanRanking.RankData;

public class S_RankingClan extends ServerBasePacket {

	private static final String S_RankingClan = "[S] S_RankingClan";
	
	/*
	 * @param Serverbasepacket 
	 * S_RankingClan.java make by �°�	
	 * �������� : �������� ����� �ػ� ������ �߰����� ����. 
	 */
	public S_RankingClan(RankData[] datas) {
		writeC(Opcodes.S_EXTENDED_PROTOBUF);
		writeH(147);
		for (int i = 0; i < datas.length; i++) {
			writeC(0x0a);// �ѱ���
			int currentRank = datas[i].getCurrentR();
			int oldRank = datas[i].getPastR();
			int clanId = datas[i].getClanid();
			String name = datas[i].getClanName();
			int time = datas[i].getComTime();
			int data = (int) (datas[i].getDate().getTime() / 1000L);			
			int size = 9 + bitlengh(currentRank) + bitlengh(oldRank)+ bitlengh(clanId) + name.getBytes().length + bitlengh(time) + bitlengh(data) ;			
			writeC(size);
			writeC(0x08);// ���緩ŷ
			writeBit(currentRank);
			writeC(0x10);// ������ŷ
			writeBit(oldRank);
			writeC(0x18);// ���;��̵�
			writeBit(clanId);
			writeC(0x22);// �����̸�
			writeS2(name);
			writeC(0x28);// 0:Ȱ�� 1:�ػ�
			writeC(0);
			writeC(0x30);// Ŭ���� �ð�(����:��)
			writeBit(time);
			writeC(0x38);// Ŭ���� ��¥
			writeBit(data);		
		}
		writeH(0);
	}

	@Override
	public byte[] getContent() {
		return getBytes();
	}

	@Override
	public String getType() {
		return S_RankingClan;
	}
}