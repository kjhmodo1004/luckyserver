
package l1j.server.server.serverpackets;
import l1j.server.server.Opcodes;

public class S_UserCommands2 extends ServerBasePacket {

	private static final String S_UserCommands2 = "[C] S_UserCommands2";

	public S_UserCommands2(int number) {
		buildPacket(number);
	}

	private void buildPacket(int number) {
		writeC(Opcodes.S_BOARD_READ);
		writeD(number);//�ѹ�
		writeS(" ��Ƽ�� ");//�۾���?
		writeS(" ���� ����� ");//��¥?
		writeS("");//����?
        writeS("\n === �������� Ȱ ===\n" +
		 "\n" +
		 " ���(1��)\n" +
		 " ǳ���� ���(15��)\n" +
		 " �ٶ��� ����(50��)\n" +
		 " �׸����� ����(30��)\n" +
		 "\n" +
		 " ==========================");

	}

	@Override
	public byte[] getContent() {
		return getBytes();
	}

	public String getType() {
		return S_UserCommands2;
	}
}

