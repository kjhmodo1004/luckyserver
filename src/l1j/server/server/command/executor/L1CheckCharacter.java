package l1j.server.server.command.executor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.StringTokenizer;

import l1j.server.L1DatabaseFactory;
import l1j.server.server.datatables.ItemTable;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1DollPotentialInstance;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.utils.SQLUtil;

public class L1CheckCharacter
  implements L1CommandExecutor
{
  public static L1CommandExecutor getInstance()
  {
    return new L1CheckCharacter();
  }

  public void execute(L1PcInstance pc, String cmdName, String arg) {
    Connection c = null;
    PreparedStatement p = null;
    PreparedStatement p1 = null;
    ResultSet r = null;
    ResultSet r1 = null;
    try {
      StringTokenizer st = new StringTokenizer(arg);
      String charname = st.nextToken();
      String type = st.nextToken();

      c = L1DatabaseFactory.getInstance().getConnection();

      int searchCount = 0;

      p = c.prepareStatement(new StringBuilder().append("SELECT objid, char_name FROM characters WHERE char_name = '").append(charname).append("'").toString());
      r = p.executeQuery();

      if (type.equalsIgnoreCase("인벤"))
        try {
          while (r.next()) {
            pc.sendPackets(new S_SystemMessage(new StringBuilder().append("\\fW** 검사: ").append(type).append(" 캐릭: ").append(charname).append(" **").toString()));
            L1PcInstance target = L1World.getInstance().getPlayer(charname);
            if (target != null) target.saveInventory();

            p1 = c.prepareStatement(new StringBuilder().append("SELECT item_id,enchantlvl,is_equipped,count,item_name,bless,attr_enchantlvl,bless_level,item_level FROM character_items WHERE char_id = '")
              .append(r
              .getInt(1))
              .append("' ORDER BY 3 DESC,2 DESC, 1 ASC").toString());
            r1 = p1.executeQuery();

            while (r1.next()) {
              String itemname = getInvenItem(r1.getInt(1), r1.getInt(2), r1.getInt(3), r1.getInt(4), r1.getString(5), r1.getInt(6), r1.getInt(7), r1.getInt(8), r1.getInt(9));
              pc.sendPackets(new S_SystemMessage(new StringBuilder().append("\\fU").append(++searchCount).append(". ").append(itemname).toString()));
              itemname = "";
            }
            pc.sendPackets(new S_SystemMessage(new StringBuilder().append("\\fW** 총 ").append(searchCount).append("건의 아이템이 검색 되었습니다 **").toString()));
          }
        } catch (Exception e) {
          pc.sendPackets(new S_SystemMessage(new StringBuilder().append("\\fW** [").append(charname).append("] 캐릭 검색 오류 **").toString()));
        }
      if (type.equalsIgnoreCase("창고"))
        try {
          while (r.next()) {
            pc.sendPackets(new S_SystemMessage(new StringBuilder().append("\\fW** 검사: ").append(type).append(" 캐릭: ").append(charname).append("(").append(r.getString(1)).append(") **").toString()));

            p1 = c.prepareStatement(new StringBuilder().append("SELECT item_id,enchantlvl,count,item_name,bless,attr_enchantlvl FROM character_warehouse WHERE account_name = '")
              .append(r
              .getString(1))
              .append("' ORDER BY 2 DESC, 1 ASC").toString());
            r1 = p1.executeQuery();
            while (r1.next()) {
              String itemname = getInvenItem(r1.getInt(1), r1.getInt(2), r1.getInt(3), r1.getInt(4), r1.getString(5), r1.getInt(6), r1.getInt(7), r1.getInt(8), 0);
              pc.sendPackets(new S_SystemMessage(new StringBuilder().append("\\fU").append(++searchCount).append(". ").append(itemname).toString()));
              itemname = "";
            }
            pc.sendPackets(new S_SystemMessage(new StringBuilder().append("\\fW** 총 ").append(searchCount).append("건의 아이템이 검색 되었습니다 **").toString()));
          }
        } catch (Exception e) {
          pc.sendPackets(new S_SystemMessage(new StringBuilder().append("\\fW** [").append(charname).append("] 캐릭 검색 오류 **").toString()));
        }
      if (type.equalsIgnoreCase("장비"))
        try
        {
          while (r.next()) {
            pc.sendPackets(new S_SystemMessage(new StringBuilder().append("\\fW** 검사: ").append(type).append(" 캐릭: ").append(charname).append(" **").toString()));
            L1PcInstance target = L1World.getInstance().getPlayer(charname);
            if (target != null) target.saveInventory();

            p1 = c.prepareStatement(new StringBuilder().append("SELECT item_id,enchantlvl,is_equipped,count,item_name,bless,attr_enchantlvl FROM character_items WHERE char_id = '")
              .append(r
              .getInt(1))
              .append("' ORDER BY 3 DESC,2 DESC, 1 ASC").toString());
            r1 = p1.executeQuery();
            while (r1.next()) {
              L1ItemInstance item = ItemTable.getInstance().createItem(r1.getInt(1));
              if ((item.getItem().getType2() == 1) || (item.getItem().getType2() == 2)) {
                String itemname = getInvenItem(r1.getInt(1), r1.getInt(2), r1.getInt(3), r1.getInt(4), r1.getString(5), r1.getInt(6), r1.getInt(7), r1.getInt(8), 0);
                pc.sendPackets(new S_SystemMessage(new StringBuilder().append("\\fU").append(++searchCount).append(". ").append(itemname).toString()));
                itemname = "";
              }
            }
            pc.sendPackets(new S_SystemMessage(new StringBuilder().append("\\fW** 총 ").append(searchCount).append("건의 아이템이 검색 되었습니다 **").toString()));
          }
        } catch (Exception e) {
          pc.sendPackets(new S_SystemMessage(new StringBuilder().append("\\fW** [").append(charname).append("] 캐릭 검색 오류 **").toString()));
        }
      if (type.equalsIgnoreCase("요정창고"))
        try {
          while (r.next()) {
            pc.sendPackets(new S_SystemMessage(new StringBuilder().append("\\fW** 검사: ").append(type).append(" 캐릭: ").append(charname).append("(").append(r.getString(1)).append(") **").toString()));

            p1 = c.prepareStatement(new StringBuilder().append("SELECT item_id,enchantlvl,count,item_name,bless,attr_enchantlvl,bless_level FROM character_elf_warehouse WHERE account_name = '")
              .append(r
              .getString(1))
              .append("' ORDER BY 2 DESC, 1 ASC").toString());
            r1 = p1.executeQuery();
            while (r1.next()) {
              String itemname = getInvenItem(r1.getInt(1), r1.getInt(2), r1.getInt(3), r1.getInt(4), r1.getString(5), r1.getInt(6), r1.getInt(7), r1.getInt(8), 0);
              pc.sendPackets(new S_SystemMessage(new StringBuilder().append("\\fU").append(++searchCount).append(". ").append(itemname).toString()));
              itemname = "";
            }
            pc.sendPackets(new S_SystemMessage(new StringBuilder().append("\\fW** 총 ").append(searchCount).append("건의 아이템이 검색 되었습니다 **").toString()));
          }
        } catch (Exception e) {
          pc.sendPackets(new S_SystemMessage(new StringBuilder().append("\\fW** [").append(charname).append("] 캐릭 검색 오류 **").toString()));
        }
    }
    catch (Exception e) {
      pc.sendPackets(new S_SystemMessage(".검사 [캐릭명] [장비,인벤,창고,요정창고]"));
    } finally {
      SQLUtil.close(r1); SQLUtil.close(p1);
      SQLUtil.close(r); SQLUtil.close(p);
      SQLUtil.close(c);
    }
  }

  private static String getInvenItem(int itemid, int enchant, int equip, int count, String itemname, int bless, int attr, int bless_level, int item_level) { StringBuilder name = new StringBuilder();

    switch (bless_level) { case 1:
      name.append("\\aD[축1단]\\fU"); break;
    case 2:
      name.append("\\aD[축2단]\\fU"); break;
    case 3:
      name.append("\\aD[축3단]\\fU"); break;
    case 4:
      name.append("\\aD[축4단]\\fU"); break;
    case 5:
      name.append("\\aD[축5단]\\fU"); break;
    }

    L1DollPotentialInstance doll = new L1DollPotentialInstance();
    if (item_level > 0) {
      name.append(new StringBuilder().append("\\aG[").append(doll.find_potential(item_level)).append("]\\fU").toString());
    }

    if (enchant > 0)
      name.append(new StringBuilder().append("+").append(enchant).append(" ").toString());
    else if (enchant == 0)
      name.append("");
    else if (enchant < 0) {
      name.append(new StringBuilder().append(String.valueOf(enchant)).append(" ").toString());
    }

    switch (attr) {
    case 1:
      name.append("화령:1단 ");
      break;
    case 2:
      name.append("화령:2단 ");
      break;
    case 3:
      name.append("화령:3단 ");
      break;
    case 4:
      name.append("수령:1단 ");
      break;
    case 5:
      name.append("수령:2단 ");
      break;
    case 6:
      name.append("수령:3단 ");
      break;
    case 7:
      name.append("풍령:1단 ");
      break;
    case 8:
      name.append("풍령:2단 ");
      break;
    case 9:
      name.append("풍령:3단 ");
      break;
    case 10:
      name.append("지령:1단 ");
      break;
    case 11:
      name.append("지령:2단 ");
      break;
    case 12:
      name.append("지령:3단 ");
      break;
    case 33:
      name.append("화령:4단 ");
      break;
    case 34:
      name.append("화령:5단 ");
      break;
    case 35:
      name.append("수령:4단 ");
      break;
    case 36:
      name.append("수령:5단 ");
      break;
    case 37:
      name.append("풍령:4단 ");
      break;
    case 38:
      name.append("풍령:5단 ");
      break;
    case 39:
      name.append("지령:4단 ");
      break;
    case 40:
      name.append("지령:5단 ");
      break;
    case 13:
    case 14:
    case 15:
    case 16:
    case 17:
    case 18:
    case 19:
    case 20:
    case 21:
    case 22:
    case 23:
    case 24:
    case 25:
    case 26:
    case 27:
    case 28:
    case 29:
    case 30:
    case 31:
    case 32: } name.append(new StringBuilder().append(itemname).append(" ").toString());

    if (equip == 1) {
      name.append("(착용)");
    }
    switch (bless) { case 129:
      name.append("[봉인]"); break;
    }

    if (count > 1) {
      name.append(new StringBuilder().append("(").append(count).append(")").toString());
    }
    return name.toString();
  }
}