package l1j.server.server.Controller;

import l1j.server.Config;
import l1j.server.server.model.L1Clan;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_NewCreateItem;
import l1j.server.server.serverpackets.S_SystemMessage;

public class TamController implements Runnable {

	public static final int SLEEP_TIME = Config.Tam_Time * 60000;

	private static TamController _instance;

	public static TamController getInstance() {
		if (_instance == null) {
			_instance = new TamController();
		}
		return _instance;
	}

	@Override
	public void run() {
		try {
			PremiumTime();
		} catch (Exception e1) {
		}
	}

	private void PremiumTime() {
		for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
			int premium1 = Config.Ž���Ͱ���;
			int premium2 = Config.Ž��������;
			/** Ž���޺κ� ����ȭ **/
			if (!pc.isPrivateShop() && !pc.noPlayerCK && !pc.noPlayerck2 && pc != null && !pc.isDead()) {
				int tamcount = pc.tamcount();
				if (tamcount > 0) {
					int addtam = Config.Ž���� * tamcount;
					pc.getNetConnection().getAccount().addTamPoint(addtam);
					try {
						pc.getNetConnection().getAccount().updateTam();
					} catch (Exception e) {
					}
					L1Clan clan = L1World.getInstance().getClan(pc.getClanid());
					pc.sendPackets(new S_SystemMessage("������ ����(" + tamcount + ")�ܰ� Tam����Ʈ(" + addtam + ")�� ȹ��"));
					if (clan != null) {
						if (clan.getClanId() != 0) {
							pc.getAccount().addTamPoint(premium1);
							pc.sendPackets(new S_SystemMessage("���ͼ����� ���� Tam (" + premium1 + ")ȹ��"));
						}else if (clan.getCastleId() != 0) {
							pc.getAccount().addTamPoint(premium2);
							pc.sendPackets(new S_SystemMessage("���������� ���� Tam (" + premium2 + ")ȹ��"));
						}
					}
					try {
						pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.TAM_POINT, pc.getNetConnection()), true);
					} catch (Exception e) {
					}
				}
			}
		}
	}
}