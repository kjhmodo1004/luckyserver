package l1j.server.server.Controller;

import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javolution.util.FastTable;
import l1j.server.Config;
import l1j.server.MJExpRevision.MJEFishingType;
import l1j.server.MJExpRevision.MJFishingExpInfo;
import l1j.server.MJTemplate.MJProto.MainServer_Client_Pledge.SC_REST_EXP_INFO_NOTI;
import l1j.server.server.GameServerSetting;
import l1j.server.server.datatables.ExpTable;
import l1j.server.server.model.Broadcaster;
import l1j.server.server.model.L1PcInventory;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.serverpackets.S_CharVisualUpdate;
import l1j.server.server.serverpackets.S_InventoryIcon;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.serverpackets.S_SkillSound;
import l1j.server.server.serverpackets.S_����;
import l1j.server.server.serverpackets.ServerBasePacket;

public class FishingTimeController implements Runnable {

   public static final int SLEEP_TIME = 1000;

   private static FishingTimeController _instance;

   private final List<L1PcInstance> _fishingList = new FastTable<L1PcInstance>();

   private static Random _random = new Random(System.nanoTime());

   public static FishingTimeController getInstance() {
      if (_instance == null) {
         _instance = new FishingTimeController();
      }
      return _instance;
   }

   public void run() {
      try {
         fishing();
      } catch (Exception e1) {
         e1.printStackTrace();//����ó�� ���
      } finally {
      }
   }

   public void addMember(L1PcInstance pc) {
      if (pc == null || _fishingList.contains(pc)) {
         return;
      }
      _fishingList.add(pc);
   }

   public boolean isMember(L1PcInstance pc) {
      if (_fishingList.contains(pc)) {
         return true;
      }
      return false;
   }

   public void removeMember(L1PcInstance pc) {
      if (pc == null || !_fishingList.contains(pc)) {
         return;
      }
      _fishingList.remove(pc);
   }

   public boolean ���峬�� = false;
   public boolean ���޼��峬�� = false;
   public boolean ������������ = false;
   public boolean ����ݺ����� = false;
   
   
   private void fishing() {
      if (_fishingList.size() > 0) {
         long currentTime = System.currentTimeMillis();
         L1PcInstance pc = null;
         Iterator<L1PcInstance> iter = _fishingList.iterator();
         while (iter.hasNext()) {
            pc = iter.next();
            if (pc == null || pc.getMapId() != 5490 || pc.isDead() || pc.getNetConnection() == null || pc.getCurrentHp() == 0)
               continue;
            if (pc.isFishing()) {
               long time = pc.getFishingTime();
               if (currentTime > (time + 1000)) {
                  //TODO �̳��� �������
                  if (pc._fishingRod.getItemId() == 600229 || pc._fishingRod.getItemId() == 87058 || pc._fishingRod.getItemId() == 87059 
                        || pc._fishingRod.getItemId() == 4100293 || pc.getInventory().consumeItem(41295, 1)) {
                     //TODO �� ���� ��ź�� ���˴�
                     if (pc._fishingRod.getItemId() == 41294) {
                        L1ItemInstance item = pc._fishingRod;
                        if (item != null) {
                           if(Config.INFINIE_FISHING){
                              pc.setFishingTime(System.currentTimeMillis() + 80000);
                              pc.sendPackets(new S_����(80));
                              ��������ź�³��˴�(pc);
                           }else {
                              if (item.getChargeCount() <= 0) {
                                 L1ItemInstance newfishingRod = null;
                                 pc.getInventory().removeItem(item, 1);
                                 newfishingRod = pc.getInventory().storeItem(41293, 1);
                                 pc._fishingRod = newfishingRod;
                                 endFishing(pc);
                              } else {
                                 item.setChargeCount(item.getChargeCount() - 1);
                                 pc.getInventory().updateItem(item, L1PcInventory.COL_CHARGE_COUNT);
                                 pc.setFishingTime(System.currentTimeMillis() + 80000);
                                 pc.sendPackets(new S_����(80));
                                 ��������ź�³��˴�(pc);
                              }
                           }
                        }
                        //TODO ���������� ���˴�
                     } else if (pc._fishingRod.getItemId() == 41305) { 
                        L1ItemInstance item = pc._fishingRod;
                        if (item != null) {
                           if(Config.INFINIE_FISHING){
                              pc.setFishingTime(System.currentTimeMillis() + 80000);
                              pc.sendPackets(new S_����(80));
                              �������������˴�(pc);
                           }else {
                              if (item.getChargeCount() <= 0) {
                                 L1ItemInstance newfishingRod = null;
                                 pc.getInventory().removeItem(item, 1);
                                 newfishingRod = pc.getInventory().storeItem(41293, 1);
                                 pc._fishingRod = newfishingRod;
                                 endFishing(pc);
                              } else {
                                 item.setChargeCount(item.getChargeCount() - 1);
                                 pc.getInventory().updateItem(item, L1PcInventory.COL_CHARGE_COUNT);
                                 pc.setFishingTime(System.currentTimeMillis() + 80000);
                                 pc.sendPackets(new S_����(80));
                                 �������������˴�(pc);
                              }
                           }
                        }
                        //TODO ������ �ݺ� ���˴�
                     } else if (pc._fishingRod.getItemId() == 41306) {
                        L1ItemInstance item = pc._fishingRod;
                        if (item != null) {
                           if(Config.INFINIE_FISHING){
                              pc.setFishingTime(System.currentTimeMillis() + 80000);
                              pc.sendPackets(new S_����(80));
                              �������ݺ����˴�(pc);
                           }else {
                              if (item.getChargeCount() <= 0) {
                                 L1ItemInstance newfishingRod = null;
                                 pc.getInventory().removeItem(item, 1);
                                 newfishingRod = pc.getInventory().storeItem(41293, 1);
                                 pc._fishingRod = newfishingRod;
                                 endFishing(pc);
                              } else {
                                 item.setChargeCount(item.getChargeCount() - 1);
                                 pc.getInventory().updateItem(item, L1PcInventory.COL_CHARGE_COUNT);
                                 pc.setFishingTime(System.currentTimeMillis() + 80000);
                                 pc.sendPackets(new S_����(80));
                                 �������ݺ����˴�(pc);
                              }
                           }
                        }
                        //TODO Ȳ�Ұ����� ���ô�
                     } else if (pc._fishingRod.getItemId() == 9991) {
                        L1ItemInstance item = pc._fishingRod;
                        if (item != null) {
                           if(Config.INFINIE_FISHING){
                              pc.setFishingTime(System.currentTimeMillis() + 80000);
                              pc.sendPackets(new S_����(80));
                              Ȳ�Ұ��������˴�(pc);
                           }else {
                              if (item.getChargeCount() <= 0) {
                                 L1ItemInstance newfishingRod = null;
                                 pc.getInventory().removeItem(item, 1);
                                 newfishingRod = pc.getInventory().storeItem(9993, 1); // �η������˴�
                                 pc._fishingRod = newfishingRod;
                                 endFishing(pc);
                              } else {
                                 item.setChargeCount(item.getChargeCount() - 1);
                                 pc.getInventory().updateItem(item, L1PcInventory.COL_CHARGE_COUNT);
                                 pc.setFishingTime(System.currentTimeMillis() + 80000);
                                 pc.sendPackets(new S_����(80));
                                 Ȳ�Ұ��������˴�(pc);
                              }
                           }
                        }
                        // TODO ������ ���� ���˴�
                     } else if (pc._fishingRod.getItemId() == 87058) {
                        L1ItemInstance item = pc._fishingRod;
                        if (item != null) {
                           if(Config.INFINIE_FISHING){
                              pc.setFishingTime(System.currentTimeMillis() + 90000);
                              pc.sendPackets(new S_����(90));
                              ������������ = true;
                              Ancientsilver(pc);
                           }else {
                              if (item.getChargeCount() <= 0) {
                                 L1ItemInstance newfishingRod = null;
                                 pc.getInventory().removeItem(item, 1);
                                 newfishingRod = pc.getInventory().storeItem(41293, 1);
                                 pc._fishingRod = newfishingRod;
                                 endFishing(pc);
                              } else {
                                 item.setChargeCount(item.getChargeCount() - 1);
                                 pc.getInventory().updateItem(item, L1PcInventory.COL_CHARGE_COUNT);
                                 pc.setFishingTime(System.currentTimeMillis() + 90000);
                                 pc.sendPackets(new S_����(90));
                                 ������������ = true;
                                 Ancientsilver(pc);
                              }
                           }
                        }
                        // TODO ������ �ݺ� ���˴�
                     } else if (pc._fishingRod.getItemId() == 87059) {
                        L1ItemInstance item = pc._fishingRod;
                        if (item != null) {
                           if(Config.INFINIE_FISHING){
                              pc.setFishingTime(System.currentTimeMillis() + 90000);
                              pc.sendPackets(new S_����(90));
                              ����ݺ����� = true;
                              Ancientgold(pc);
                           }else {
                              if (item.getChargeCount() <= 0) {
                                 L1ItemInstance newfishingRod = null;
                                 pc.getInventory().removeItem(item, 1);
                                 newfishingRod = pc.getInventory().storeItem(41293, 1);
                                 pc._fishingRod = newfishingRod;
                                 endFishing(pc);
                              } else {
                                 item.setChargeCount(item.getChargeCount() - 1);
                                 pc.getInventory().updateItem(item, L1PcInventory.COL_CHARGE_COUNT);
                                 pc.setFishingTime(System.currentTimeMillis() + 90000);
                                 pc.sendPackets(new S_����(90));
                                 ����ݺ����� = true;
                                 Ancientgold(pc);
                              }
                           }
                        }
                        //TODO ������ ���ô�
                     } else if (pc._fishingRod.getItemId() == 600229) {
                        L1ItemInstance item = pc._fishingRod;
                        if (item != null) {
                           /*if(Config.INFINIE_FISHING){
                              pc.setFishingTime(System.currentTimeMillis() + 30000);
                              pc.sendPackets(new S_����(30));
                              ���峬�� = true;
                              �����ǳ��ô�(pc);
                           }else {*/
                              if (item.getChargeCount() <= 0) {
                                 L1ItemInstance newfishingRod = null;
                                 pc.getInventory().removeItem(item, 1);
                                 newfishingRod = pc.getInventory().storeItem(41293, 1);
                                 pc._fishingRod = newfishingRod;
                                 endFishing(pc);
                              } else {
                                 item.setChargeCount(item.getChargeCount() - 1);
                                 pc.getInventory().updateItem(item, L1PcInventory.COL_CHARGE_COUNT);
                                 pc.setFishingTime(System.currentTimeMillis() + 30000);
                                 pc.sendPackets(new S_����(30));
                                 ���峬�� = true;
                                 �����ǳ��ô�(pc);
                              }
                           //}
                        }
                        //TODO ���� ������ ���ô�
                     } else if (pc._fishingRod.getItemId() == 4100293) {
                        L1ItemInstance item = pc._fishingRod;
                        if (item != null) {
                           if(Config.INFINIE_FISHING){
                              pc.setFishingTime(System.currentTimeMillis() + 30000);
                              pc.sendPackets(new S_����(30));
                              ���޼��峬�� = true;
                              ���޼����ǳ��ô�(pc);
                           }else {
                              if (item.getChargeCount() <= 0) {
                                 L1ItemInstance newfishingRod = null;
                                 pc.getInventory().removeItem(item, 1);
                                 newfishingRod = pc.getInventory().storeItem(41293, 1);
                                 pc._fishingRod = newfishingRod;
                                 endFishing(pc);
                              } else {
                                 item.setChargeCount(item.getChargeCount() - 1);
                                 pc.getInventory().updateItem(item, L1PcInventory.COL_CHARGE_COUNT-1);
                                 pc.setFishingTime(System.currentTimeMillis() + 30000);
                                 pc.sendPackets(new S_����(30));
                                 ���޼��峬�� = true;
                                 ���޼����ǳ��ô�(pc);
                              }
                           }
                        }
                        //TODO �Ϲ� ���˴�
                     } else if (pc._fishingRod.getItemId() == 41293) {
                        pc.setFishingTime(System.currentTimeMillis() + 240000);
                        pc.sendPackets(new S_����(240));
                        ��ź�³��˴�(pc);
                     }
                  } else {
                     // �̳��� ��� ���� ó�� ����.
                     endFishing(pc);
                  }
               }
            }
         }
      }
   }

   public void endFishing(L1PcInstance pc) {   
      pc.setFishingTime(0);
      pc.setFishingReady(false);
      pc.setFishing(false);
      pc._fishingRod = null;
      if (���峬��) {
         ���峬�� = false;
         pc.sendPackets(S_InventoryIcon.icoEnd(L1SkillId.Fishing_etc));
      } else if(���޼��峬��) {
         ���޼��峬�� = false;
         pc.sendPackets(S_InventoryIcon.icoEnd(L1SkillId.Fishing_etc));
      } else if(������������) {
         ������������ = false;
         pc.sendPackets(S_InventoryIcon.icoEnd(L1SkillId.Fishing_etc));
      } else if(����ݺ�����) {
         ����ݺ����� = false;
         pc.sendPackets(S_InventoryIcon.icoEnd(L1SkillId.Fishing_etc));
      }
      pc.sendPackets(new S_CharVisualUpdate(pc));
      Broadcaster.broadcastPacket(pc, new S_CharVisualUpdate(pc));
      pc.sendPackets(new S_ServerMessage(1163)); // ���ð� �����߽��ϴ�.
      removeMember(pc);
   }
   
   // TODO ������ ���� ���˴�
   private void Ancientsilver(L1PcInstance pc) {
      int chance = _random.nextInt(10000) + 1;
      if (chance < 6000) { // ���纣���Ƴ�
         successFishing(pc, 41297, "$15565");
      } else if (chance < 8000) { // �����Ƴ�
         successFishing(pc, 41296, "$15564");
      } else if (chance < 8040) { // ���� �����Ƴ�
         successFishing(pc, 49092, "���� �����Ƴ�");
      } else if (chance < 8070) { // �޹������Ƴ�
         successFishing(pc, 41298, "$15566");
      } else if (chance < 8350) { // �����Ƴ�
         successFishing(pc, 41296, "$15564");
      } else if (chance < 8351) { // �޹������Ƴ�
         successFishing(pc, 41298, "$15566");
      } else if (chance < 8352) { // ���� �ݺ� �����Ƴ�
         successFishing(pc, 41300, "$17523");
      } else {
         pc.sendPackets(new S_ServerMessage(1136));
         // ���ÿ� �����߽��ϴ�.
      }
   }

   // TODO ������ �ݺ� ���˴�
   private void Ancientgold(L1PcInstance pc) {
      int chance = _random.nextInt(10000) + 1;
      if (chance < 6000) { // ���纣���Ƴ�
         successFishing(pc, 41297, "$15565");
      } else if (chance < 8000) { // �����Ƴ�
         successFishing(pc, 41296, "$15564");
      } else if (chance < 8020) { // ���� �����Ƴ�
         successFishing(pc, 49092, "���� �����Ƴ�");
      } else if (chance < 8050) { // �޹������Ƴ�
         successFishing(pc, 41298, "$15566");
      } else if (chance < 8350) { // �����Ƴ�
         successFishing(pc, 41296, "$15564");
      } else if (chance < 8351) { // ���� �����Ƴ�
         successFishing(pc, 49092, "���� �����Ƴ�");
      } else if (chance < 8352) { // ���� �ݺ� �����Ƴ�
         successFishing(pc, 41300, "$17523");
      } else {
         pc.sendPackets(new S_ServerMessage(1136));
         // ���ÿ� �����߽��ϴ�.
      }
   }

   private void �����ǳ��ô�(L1PcInstance pc) {
      int chance = _random.nextInt(10000) + 1;
      if (chance < 6000) { // �����Ƴ�
         successFishing(pc, 41296, "$15564");
      } else if (chance < 8000) { // ���� �����Ƴ�
         successFishing(pc, 41297, "$15565");
      } else if (chance < 8020) { // �޹������Ƴ�
         successFishing(pc, 41298, "$15566");
//      } else if (chance < 8130) { // ��ǳ�� ���� ��������
//         successFishing(pc, 300022, "������ ���� ��������");
//      } else if (chance < 8350) { // ǻ�� ������
//         successFishing(pc, 820018, "$20462");
//      } else if (chance < 8360) { // �巡���� ���� ����
//         successFishing(pc, 1000006, "$24608");
//      } else if (chance < 8380) { // ��ǳ�� �ݺ� ��������
//         successFishing(pc, 300021, "������ �ݺ� ��������");
//      } else if (chance < 8380) { // ���� �����Ƴ�
//        successFishing(pc, 41299, "$17521");
//      } else if (chance < 8390) { // �ݺ� �����Ƴ�
//         successFishing(pc, 41300, "$17523");
      }else
         successFishing(pc, 41296, "$15564");
   }
   
   private void ���޼����ǳ��ô�(L1PcInstance pc) {
      int chance = _random.nextInt(10000) + 1;
      if (chance < 6000) { // �����Ƴ�
         successFishing(pc, 41296, "$15564");
      } else if (chance < 8000) { // ���� �����Ƴ�
         successFishing(pc, 41297, "$15565");
      } else if (chance < 8020) { // �޹������Ƴ�
         successFishing(pc, 41298, "$15566");
      } 
//      else if (chance < 8350) { // ��Ŀ �����Ƴ�
//      successFishing(pc, 49093, "$20462");
//      } 
//      else if (chance < 8360) { // ǻ�� ������
//         successFishing(pc, 820018, "$20462");
//      }
//      else if (chance < 8370) { // �޹������Ƴ�
//         successFishing(pc, 41298, "$15566");
//      } 
//      else if (chance < 8380) { // ���� �����Ƴ�
//         successFishing(pc, 41299, "$17521");
//      } 
//      else if (chance < 8390) { // �ݺ� �����Ƴ�
//         successFishing(pc, 41300, "$17523");         
//      }
//      else if (chance < 8390) { // �����ϻ���� ����
//         successFishing(pc, 600230, "$20909");
//      }
      else
         successFishing(pc, 41296, "$15564");
   }

   private void Ȳ�Ұ��������˴�(L1PcInstance pc) {
      int chance = _random.nextInt(10000) + 1;
      if (chance < 6000) { // �����Ƴ�
         successFishing(pc, 41296, "$15564");
      } else if (chance < 8000) { // ���� �����Ƴ�
         successFishing(pc, 41297, "$15565");
      } else if (chance < 8020) { // �޹������Ƴ�
         successFishing(pc, 41298, "$15566");
      } else if (chance < 8130) { // ��ǳ�� ���� ��������
         successFishing(pc, 300022, "��ǳ�� ���� ��������");
      } else if (chance < 8350) { // ǻ�� ������
         successFishing(pc, 820018, "$20462");
      } else if (chance < 8360) { // �巡���� ���� ����
         successFishing(pc, 1000006, "$24608");
      } else if (chance < 8380) { // ��ǳ�� �ݺ� ��������
         successFishing(pc, 300021, "��ǳ�� �ݺ� ��������");
      } else if (chance < 8380) { // ���� �����Ƴ�
         successFishing(pc, 41299, "$17521");
      } else if (chance < 8390) { // �ݺ� �����Ƴ�
         successFishing(pc, 41300, "$17523");
      } else if (chance < 8400) { // �ݺ� �����Ƴ�
         successFishing(pc, 4100206, "���� ��ȯ ����(1,000��)");
      } else if (chance < 8450) { // �ݺ� �����Ƴ�
         successFishing(pc, 4100207, "���� ��ȯ ����(5,000��)");
      } else if (chance < 8451) { // �ݺ� �����Ƴ�
         successFishing(pc, 4100208, "���� ��ȯ ����(10,000��)");
      }else
         successFishing(pc, 41296, "$15564");
   }

   private void ��������ź�³��˴�(L1PcInstance pc) {
      int chance = _random.nextInt(10000) + 1;
      if (chance < 6000) { // �����Ƴ�
         successFishing(pc, 41296, "$15564");
      } else if (chance < 8000) { // ���� �����Ƴ�
         successFishing(pc, 41297, "$15565");
      } else if (chance < 8020) { // �޹������Ƴ�
         successFishing(pc, 41298, "$15566");
      } else if (chance < 8350) { // ������ ���ð���
         successFishing(pc, 41301, "$15815");
      } else if (chance < 8351) { // ���� ���� �����Ƴ�
         successFishing(pc, 41299, "$17521");
      } else if (chance < 8352) { // ���� �ݺ� �����Ƴ�
         successFishing(pc, 41300, "$17523");
      } else {
         pc.sendPackets(new S_ServerMessage(1136));
         // ���ÿ� �����߽��ϴ�.
      }
   }
   private void �������������˴�(L1PcInstance pc) {
      int chance = _random.nextInt(10000) + 1;
      if (chance < 4000) { // �����Ƴ�
         successFishing(pc, 41296, "$15564");
      } else if (chance < 8000) { // ���� �����Ƴ�
         successFishing(pc, 41297, "$15565");
      } else if (chance < 8040) { // �޹������Ƴ�
         successFishing(pc, 41298, "$15566");
      } else if (chance < 8350) { // ������ ���ð���
         successFishing(pc, 41301, "$15815");
      } else if (chance < 8352) { // ���� ���� �����Ƴ�
         successFishing(pc, 41299, "$17521");
      } else if (chance < 8353) { // ū ���� �����Ƴ�
         successFishing(pc, 41303, "$17522");
      } else {
         pc.sendPackets(new S_ServerMessage(1136));
         // ���ÿ� �����߽��ϴ�.
      }
   }
   private void �������ݺ����˴�(L1PcInstance pc) {
      int chance = _random.nextInt(10000) + 1;
      if (chance < 3500) { // ���纣���Ƴ�
         successFishing(pc, 41297, "$15565");
      } else if (chance < 8000) { // �����Ƴ�
         successFishing(pc, 41296, "$15564");
      } else if (chance < 8050) { // �޹������Ƴ�
         successFishing(pc, 41298, "$15566");
      } else if (chance < 8350) { // ������ ���ð���
         successFishing(pc, 41301, "$15815");
      } else if (chance < 8352) { // ���� �ݺ� �����Ƴ�
         successFishing(pc, 41300, "$17523");
      } else if (chance < 8354) { // ū �ݺ� �����Ƴ�
         successFishing(pc, 41304, "$17524"); //
      } else {
         pc.sendPackets(new S_ServerMessage(1136));
         // ���ÿ� �����߽��ϴ�.
      }
   }
   private void ��ź�³��˴�(L1PcInstance pc) {
      int chance = _random.nextInt(10000) + 1; // 100%
      // �����Ƴ�
      if (chance < 4000) {
         successFishing(pc, 41296, "$15564");
         // ���纣���Ƴ�
      } else if (chance < 8000) {
         successFishing(pc, 41297, "$15565");
         // �޹������Ƴ�
      } else if (chance < 8010) {
         successFishing(pc, 41298, "$15566");
         // ������ ���ð���
      } else if (chance < 8350) {
         successFishing(pc, 41301, "$15815");
      } else {
         pc.sendPackets(new S_ServerMessage(1136)); // 16%
         // ���ÿ� �����߽��ϴ�.
      }
      pc.sendPackets(new S_ServerMessage(1147));
   }

   private boolean check_weight(L1PcInstance pc){
      if (pc.getInventory().getSize() > (180 - 16)) {
         pc.sendPackets(new S_ServerMessage(263));
         return false;
      }
      return true;
   }
   
   private void distribute_message(int owner_object_id, String message, int effect_id){
      ServerBasePacket[] pcks = new ServerBasePacket[]{
            new S_PacketBox(S_PacketBox.GREEN_MESSAGE, message),
            new S_SkillSound(owner_object_id, effect_id),
      };
      L1World.getInstance().broadcast_map(5490, pcks);
   }
   
   private void distribute_message(int owner_object_id, int itemid){
      switch(itemid){
      case 41303:
         distribute_message(owner_object_id, "�������� ū ���� �����Ƴ��� ���� �÷Ƚ��ϴ�!", 13641);
         break;
      case 41304:
         distribute_message(owner_object_id, "�������� ū �ݺ� �����Ƴ��� ���� �÷Ƚ��ϴ�!", 13639);
         break;
      case 49094:
         distribute_message(owner_object_id, "�������� ������ ���� �����Ƴ��� ���� �÷Ƚ��ϴ�!", 13641);
         break;
      case 49095:
         distribute_message(owner_object_id, "�������� ������ �ݺ� �����Ƴ��� ���� �÷Ƚ��ϴ�!", 13639);
         break;
      }
   }
   
   private void calculate_exp(L1PcInstance pc, MJEFishingType f_type){
      try {
         int level = Math.max(pc.getLevel(), 2);
         MJFishingExpInfo eInfo = MJFishingExpInfo.find_fishing_exp_info(f_type, level);
         if(eInfo == null)
            return;
         
         int need_exp = ExpTable.getNeedExpNextLevel(52);
         double exp = need_exp * eInfo.get_default_exp();
         int ain = pc.getAccount().getBlessOfAin();
         if(ain >= 10000){
            double ain_effect = 1D;
            if(ain > 20000)
               ain_effect += 0.3D;
            if(pc.hasSkillEffect(L1SkillId.DRAGON_TOPAZ) && ain >= 20000)
               ain_effect += 0.8D;
            exp += (exp * ain_effect);
            pc.getAccount().addBlessOfAin(-(int)SC_REST_EXP_INFO_NOTI.calcDecreaseCharacterEinhasad(pc, exp * eInfo.get_ain_ration()), pc);
         }
         if (pc.PC��_����)
            exp *= 1.1;
         
         exp = exp * eInfo.get_addition_exp();
         exp = Math.max(1, exp);
         
         if (pc.getLevel() >= GameServerSetting.getInstance().get_maxLevel()) {
            int maxexp = ExpTable.getExpByLevel(GameServerSetting.getInstance().get_maxLevel() + 1);
            if (pc.getExp() + exp >= maxexp)
               return;
         }
         
         if ((exp + pc.getExp()) > ExpTable.getExpByLevel((level + 1))) {
            exp = (ExpTable.getExpByLevel((level + 1)) - pc.getExp());
         }
         pc.addExp((int)exp);
         pc.save();
      } catch (Exception e) {
         e.printStackTrace();
      }
   }
   
   private void successFishing(L1PcInstance pc, int itemid, String message) {
      if(!check_weight(pc))
         return;
      
      L1ItemInstance item = pc.getInventory().storeItem(itemid, 1);
      if (item != null) {
         pc.sendPackets(new S_ServerMessage(1185, message)); // ���ÿ� ������%0%o�� �����߽��ϴ�.
         ServerBasePacket pck = new S_SkillSound(pc.getId(), 763);
         pc.sendPackets(pck, false);
         pc.broadcastPacket(pck);
      }
      distribute_message(pc.getId(), itemid);
      if (���峬��) {
         calculate_exp(pc, MJEFishingType.GROWN_UP);
         ���峬�� = false;
      } else if (���޼��峬��) {
         calculate_exp(pc, MJEFishingType.HIGH_GROWN_UP);
         ���޼��峬�� = false;
      } else if (������������) {      
         calculate_exp(pc, MJEFishingType.ACIENT_SILVER);   
         ������������ = false;
      } else if (����ݺ�����) {
         calculate_exp(pc, MJEFishingType.ACIENT_GOLD);
         ����ݺ����� = false;
      }
   }
}