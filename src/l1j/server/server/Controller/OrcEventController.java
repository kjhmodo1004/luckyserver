package l1j.server.server.Controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import l1j.server.Config;
import l1j.server.server.model.L1World;
import l1j.server.server.serverpackets.S_PacketBox;

public class OrcEventController extends Thread
{
  private static OrcEventController _instance;
  private boolean orc_event_open;
  private int orc_location;
  private static long sTime = 0L;

  public long openTime = 0L;

  private static final SimpleDateFormat s = new SimpleDateFormat("HH", Locale.KOREA);

  private static final SimpleDateFormat ss = new SimpleDateFormat("MM-dd HH:mm", Locale.KOREA);

  public boolean isOrc_event_open()
  {
    return this.orc_event_open;
  }

  public void setOrc_event_open(boolean orc_event_open) {
    this.orc_event_open = orc_event_open;
  }

  public int getOrc_location() {
    return this.orc_location;
  }

  public int setOrc_location(int orc_location) {
    return this.orc_location = orc_location;
  }

  public static OrcEventController getInstance()
  {
    if (_instance == null) {
      _instance = new OrcEventController();
      _instance.start();
    }
    return _instance;
  }

  public void run()
  {
    try
    {
      while (true)
      {
        Thread.sleep(1000L);

        String map_name = "";
        if (isOrc_event_open())
        {
          if (getOrc_location() == 1) {
            map_name = "라스타바드 던전";
            Config.orc_event_01_open = true;
          } else if (getOrc_location() == 2) {
            map_name = "잊혀진 섬(던전)";
            Config.orc_event_02_open = true;
          } else if (getOrc_location() == 3) {
            map_name = "지배의 결계 2층";
            Config.orc_event_03_open = true;
          } else if (getOrc_location() == 4) {
            map_name = "지배의 탑 정상";
            Config.orc_event_04_open = true;
          } else if (getOrc_location() == 5) {
            map_name = "기란감옥 1층";
            Config.orc_event_05_open = true;
          } else if (getOrc_location() == 6) {
            map_name = "수련 던전 4층";
            Config.orc_event_06_open = true;
          } else if (getOrc_location() == 7) {
            map_name = "요정숲 던전 3층";
            Config.orc_event_07_open = true;
          } else if (getOrc_location() == 8) {
            map_name = "글루디오 던전 7층";
            Config.orc_event_08_open = true;
          } else if (getOrc_location() == 9) {
            map_name = "개미굴 던전";
            Config.orc_event_09_open = true;
          }

          L1World.getInstance().broadcastPacketToAll(new S_PacketBox(84, "잠시후 [" + map_name + "]에서 30분동안 \\aG붉은 오크 이벤트\\f2가 진행됩니다."));
          L1World.getInstance().broadcastServerMessage("\\f2잠시후 [" + map_name + "]에서 30분동안 \\aG붉은 오크 이벤트\\f2가 진행됩니다.");
          L1World.getInstance().broadcastServerMessage("\\f2잠시후 [" + map_name + "]에서 30분동안 \\aG붉은 오크 이벤트\\f2가 진행됩니다.");
          L1World.getInstance().broadcastServerMessage("\\f2잠시후 [" + map_name + "]에서 30분동안 \\aG붉은 오크 이벤트\\f2가 진행됩니다.");

          Thread.sleep(1800000L);

          L1World.getInstance().broadcastPacketToAll(new S_PacketBox(84, "잠시후 \\aG붉은 오크 이벤트\\f2가 종료됩니다."));
          L1World.getInstance().broadcastServerMessage("\\f2잠시후 \\aG붉은 오크 이벤트\\f2가 종료됩니다.");
          L1World.getInstance().broadcastServerMessage("\\f2잠시후 \\aG붉은 오크 이벤트\\f2가 종료됩니다.");
          L1World.getInstance().broadcastServerMessage("\\f2잠시후 \\aG붉은 오크 이벤트\\f2가 종료됩니다.");

          Thread.sleep(60000L);

          if (getOrc_location() == 1)
            Config.orc_event_01_open = false;
          else if (getOrc_location() == 2)
            Config.orc_event_02_open = false;
          else if (getOrc_location() == 3)
            Config.orc_event_03_open = false;
          else if (getOrc_location() == 4)
            Config.orc_event_04_open = false;
          else if (getOrc_location() == 5)
            Config.orc_event_05_open = false;
          else if (getOrc_location() == 6)
            Config.orc_event_06_open = false;
          else if (getOrc_location() == 7)
            Config.orc_event_07_open = false;
          else if (getOrc_location() == 8)
            Config.orc_event_08_open = false;
          else if (getOrc_location() == 9) {
            Config.orc_event_09_open = false;
          }
          setOrc_event_open(false);

          L1World.getInstance().broadcastPacketToAll(new S_PacketBox(84, "\\aG붉은 오크 이벤트가 종료되었습니다."));
          L1World.getInstance().broadcastServerMessage("\\aG붉은 오크 이벤트가 종료되었습니다.");
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public String OpenTime()
  {
    Calendar c = Calendar.getInstance();
    c.setTimeInMillis(sTime);
    return ss.format(c.getTime());
  }
}