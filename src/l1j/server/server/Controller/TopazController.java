/*
 * 토파즈 컨트롤러 통으로 추가해주세요 by KSOFT
 */


package l1j.server.server.Controller;

import java.util.logging.Level;
import java.util.logging.Logger;
import l1j.server.server.GeneralThreadPool;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_PacketBox;

public class TopazController implements Runnable {
	private static Logger _log = Logger.getLogger(TopazController.class.getName());

	private static TopazController _instance;

	public static TopazController getInstance() {
		if (_instance == null)
			_instance = new TopazController();
		return _instance;
	}

	public TopazController() {
		GeneralThreadPool.getInstance().execute(this);
	}

	@Override
	public void run() {
		while (true) {
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
				_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			}
		}
	}

	// 리스할시 토파즈 상태 불러오기
	public void GetEatTopazForRestart(L1PcInstance pc, int time) {
		GeneralThreadPool.getInstance().execute(new EatTOPAZonRestart(pc, time));
	}

	public class EatTOPAZonRestart implements Runnable {
		L1PcInstance pc;
		int time;

		public EatTOPAZonRestart(L1PcInstance _pc, int _time) {
			pc = _pc;
			time = _time;
		}

		public void run() {
			try {
				Thread.sleep(3 * 1000);
				pc.sendPackets(new S_PacketBox(time, 2, true, true));
				pc.sendPackets(new S_PacketBox(pc, S_PacketBox.EINHASAD));
			} catch (Exception e) {
			}
		}
	}
}
