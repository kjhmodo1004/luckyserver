package l1j.server.server.Controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javolution.util.FastTable;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1PcInstance;

public class �ؼ�Controller extends Thread {

	private static �ؼ�Controller _instance;

	private boolean _�ؼ�Start;

	public boolean get�ؼ�Start() {
		return _�ؼ�Start;
	}

	public void set�ؼ�Start(boolean �ؼ�) {
		_�ؼ�Start = �ؼ�;
		_is��ž�� = _�ؼ�Start;
	}

	private static long sTime = 0;

	public boolean isGmOpen = false;

	public long openTime = 0;

	private static final SimpleDateFormat s = new SimpleDateFormat("HH", Locale.KOREA);

	private static final SimpleDateFormat ss = new SimpleDateFormat("MM-dd HH:mm", Locale.KOREA);

	public static �ؼ�Controller getInstance() {
		if (_instance == null) {
			_instance = new �ؼ�Controller();
			_instance.start();
		}
		return _instance;
	}

	private List<L1PcInstance> list = new FastTable<L1PcInstance>();

	public void add_list(L1PcInstance pc) {
		synchronized (list) {
			if (!list.contains(pc))
				list.add(pc);
		}
	}

	public void removeList(L1PcInstance pc) {
		if (!list.contains(pc))
			return;
		list.remove(pc);
	}

	public void end_pc_map() {
		try {
			Iterator<L1PcInstance> iter = list.iterator();
			L1PcInstance pc = null;

			while (iter.hasNext()) {
				pc = iter.next();
				if (pc == null || pc.isDead()) {
					continue;
				}
				if (pc.getMapId() >= 1708 && pc.getMapId() <= 1712) {// 2�����߰� L1�ڷ���Ʈ
					pc.start_teleport(33970, 33246, 4, 5, 169, true, false);
					iter.remove();
				} else {
					iter.remove();
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		try {
			while (true) {
				Thread.sleep(1000);
				/**
				 * �ؼ��� ���� ���� �ε� PC�� �ش� �ʿ� ���� �Ѵٸ� �ڷ���Ʈ
				 */
				if (list.size() > 0 && !get�ؼ�Start()) {
					end_pc_map();
				}

				/** ���� **/
				if (!isOpen() && !isGmOpen)
					continue;
				if (L1World.getInstance().getAllPlayers().size() <= 0)
					continue;

				isGmOpen = false;

				/** ���� �޼��� **/
				L1World.getInstance().broadcastServerMessage("����� ������ �� ������ 2�ð� ���� ���� �մϴ�.");

				/** �ؼ���Ʈ�ѷ� ���� **/
				set�ؼ�Start(true);

				/** ���� 1�ð� ���� **/
				// Thread.sleep(10000L); // 3800000L �׽�Ʈ��
				Thread.sleep(2 * 60 * 60 * 1000); // 1�ð� �տ� 1�� 1�ð�

				set�ؼ�Start(false);
				Thread.sleep(5000L);

				if (list.size() > 0) {
					end_pc_map();
				}
				L1World.getInstance().broadcastServerMessage("����� ������ �� �̿� �ð��� ���� �˴ϴ�.");
				End();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * ���� �ð��� �����´�
	 *
	 * @return (Strind) ���� �ð�(MM-dd HH:mm)
	 */
	public String OpenTime() {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(sTime);
		return ss.format(c.getTime());
	}

	/**
	 * ���̰� �����ִ��� Ȯ��
	 *
	 *
	 * @return (boolean) �����ִٸ� true �����ִٸ� false
	 */

	public boolean isOpen() {
		Calendar calender = Calendar.getInstance();
		int hour, minute, secon;
		hour = calender.get(Calendar.HOUR_OF_DAY);
		minute = calender.get(Calendar.MINUTE);
		secon = calender.get(Calendar.SECOND);
		// -- ���� 1�� 30��
		// -- ���� 8�� 30��
		if ((hour == 10 && minute == 00 && secon == 00) || (hour == 22 && minute == 00 && secon == 00)) {
			openTime = calender.getTime().getTime();
			return true;
		}
		return false;
	}

	private boolean _is��ž��;

	public void set��ž��(boolean a) {
		_is��ž�� = a;
	}

	public boolean is��ž��() {
		return _is��ž��;
	}

	/** ���� **/
	private void End() {
		// delenpc(1231231);//���Ǿ�����
		set�ؼ�Start(false);
	}
}