package l1j.server.server;

import static l1j.server.server.model.skill.L1SkillId.BONE_BREAK;
import static l1j.server.server.model.skill.L1SkillId.BUYER_COOLTIME;
import static l1j.server.server.model.skill.L1SkillId.CURSE_PARALYZE;
import static l1j.server.server.model.skill.L1SkillId.DESPERADO;
import static l1j.server.server.model.skill.L1SkillId.EARTH_BIND;
import static l1j.server.server.model.skill.L1SkillId.FOG_OF_SLEEPING;
import static l1j.server.server.model.skill.L1SkillId.ICE_LANCE;
import static l1j.server.server.model.skill.L1SkillId.SHOCK_STUN;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import l1j.server.Config;
import l1j.server.GrangKinConfig;
import l1j.server.L1DatabaseFactory;
import l1j.server.GameSystem.AutoSystemController;
import l1j.server.MJBotSystem.Loader.MJBotNameLoader;
import l1j.server.MJCTSystem.Loader.MJCTLoadManager;
import l1j.server.MJKDASystem.Chart.MJKDAChartScheduler;
import l1j.server.MJNetSafeSystem.Distribution.MJClientStatus;
import l1j.server.MJTemplate.Command.MJCommandArgs;
import l1j.server.MJTemplate.MJProto.MainServer_Client.SC_SPECIAL_RESISTANCE_NOTI;
import l1j.server.MJTemplate.MJSqlHelper.Executors.Selector;
import l1j.server.MJTemplate.MJSqlHelper.Executors.Updator;
import l1j.server.MJTemplate.MJSqlHelper.Handler.Handler;
import l1j.server.MJTemplate.MJSqlHelper.Handler.SelectorHandler;
import l1j.server.server.clientpackets.C_CommonClick;
import l1j.server.server.clientpackets.C_NewCharSelect;
import l1j.server.server.datatables.AuctionSystemTable;
import l1j.server.server.datatables.CharacterTable;
import l1j.server.server.datatables.ItemTable;
import l1j.server.server.datatables.LetterTable;
import l1j.server.server.model.L1Clan;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1DollPotentialInstance;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.serverpackets.S_Ability;
import l1j.server.server.serverpackets.S_CharAmount;
import l1j.server.server.serverpackets.S_ChatPacket;
import l1j.server.server.serverpackets.S_Disconnect;
import l1j.server.server.serverpackets.S_LetterList;
import l1j.server.server.serverpackets.S_Message_YN;
import l1j.server.server.serverpackets.S_OwnCharStatus;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.serverpackets.S_ShowAutoInformation;
import l1j.server.server.serverpackets.S_SkillSound;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.serverpackets.S_Unknown2;
import l1j.server.server.serverpackets.S_War;
import l1j.server.server.templates.L1Item;
import l1j.server.server.utils.MJFullStater;
import l1j.server.server.utils.SQLUtil;

public class UserCommands {

	boolean spawnTF = false;

	private static UserCommands _instance;

	private UserCommands() {
	}

	public static UserCommands getInstance() {
		if (_instance == null) {
			_instance = new UserCommands();
		}
		return _instance;
	}

	public void handleCommands(L1PcInstance pc, String cmdLine) {
		if (pc == null) {
			return;
		}
		// System.out.println(cmdLine);
		StringTokenizer token = new StringTokenizer(cmdLine);
		// System.out.println(token.hasMoreTokens());
		String cmd = "";
		if (token.hasMoreTokens())
			cmd = token.nextToken();
		else
			cmd = cmdLine;
		String param = "";
		// System.out.println(cmd);

		while (token.hasMoreTokens()) {
			param = new StringBuilder(param).append(token.nextToken()).append(' ').toString();
		}
		param = param.trim();
		
		try {
			switch (cmd) {
//			case "충전":
//				MJPaymentUserHandler.do_execute(new MJCommandArgs().setOwner(pc).setParam(param));
//				break;
			case "캐릭명변경" : 								 changename(pc, param);   												break;
			case "도움말":									showHelp(pc);																	break;
//			case "결투":									do_lfc(pc, param); 																break;
			case "판매등록":								countR1(pc, param);																break;
			case "구매신청":								countR2(pc, param);																break;
			case "판매취소":								countR3(pc, param);																break;
			case "판매완료":								countR4(pc, param);																break;
			case "구매취소":								countR5(pc, param);																break;
			case "외창": outsideChat(pc, param); break;
			case "캐릭터복구":									복구(pc);																		break;
			case "나이":									age(pc, param);																	break;
			case "혈맹파티":									BloodParty(pc);																	break;
			case "파티멘트":									Ment(pc, param);																break;
			case "무인상점":								privateShop(pc);																break;
			case "라이트":									maphack(pc, param);																break;
//			case "아덴시세":								AdSc(pc);																		break;
			case "혈마크":									clanMark(pc, param);															break;
//			case "타겟팅":									doTarget(new MJCommandArgs().setOwner(pc).setParam(param));						break;
			case "드랍멘트":                               DropMent(pc, param);                                                break;
//			case "원스텟":									fullstat(pc, param);															break;
//			case "차트":									setChart(pc, param);															break;
			case "구슬조회":									MJCTLoadManager.commands(pc, param);											break;
			//case "수배":									hunt(pc, param);																break;
//			case "텔렉풀기":
			case ".":
//			case "텔.":			
				tell(pc);
				break;
//			case "보스알림":								spawnNotifyOnOff(pc, param);													break;
			case "마일리지":
				if (pc != null) {
					String ncoin = NumberFormat.getInstance().format(pc.getAccount().Ncoin_point);
					pc.sendPackets("현재 마일리지:(\\aG" + ncoin + "\\fH)원");
					//\\fH계정(\\aH" + pc.getAccountName() + "\\fH)에 적립된
				}
				break;
//			case "엔코인선물":							giftNCoin(pc, param);																break;
//			case "봉인해제신청":							Sealedoff(pc, param);															break;
//			case "고정":
//			case "고정신청":						
//				phone(pc, param);																
//				break;
			case "암호변경":	
			case "비번변경":
				changepassword(pc, param);
				break;
			case "후원이동":
				GmRoomMove(pc);
				break;
//			case "그랑카인":								testcheck(pc);																	break;
//			case "자동사냥": 								UserCommands.getInstance().autoHunt(pc, param); 								break;
			 case "캐릭터정보":
			        charSearchInfo(pc, param);
			      case "장비검사":
			        charInventoryInfo(pc, param);
			        break;
			        
			default:
				pc.sendPackets("명령어를 잘못 입력했습니다.");
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void GmRoomMove(L1PcInstance pc) {
		try {
				// L1Teleport.teleport(target, 33437, 32812, (short) 4, 5,
				// true);
			if(pc.getMapId() == 4) {
				pc.start_teleport(32895, 32528, 300, pc.getHeading(), 169, true, true);
				pc.sendPackets("후원문의 게시판 확인해주시고, <입금명><금액> 미소피아로 편지 보내주세요.");
			}else {
				pc.sendPackets("안전한 곳에서 다시 시도하여주세요.");
			}			
		} catch (Exception e) {
			pc.sendPackets("오류 발생 메티스로 편지해주세요");
		}
	}
	
	public static void setChart(L1PcInstance pc, String param) {
		boolean isOn;
		if (!MJKDAChartScheduler.isLoaded()) {
			pc.sendPackets(new S_SystemMessage("사용중이지 않습니다."));
			return;
		}

		if (param.equalsIgnoreCase("켬")) {
			isOn = true;
			pc.sendPackets(new S_SystemMessage("킬 랭킹 차트를 보입니다. 잠시 후 화면에 나타납니다."));
			MJKDAChartScheduler.getInstance().onLoginUser(pc);
		} else if (param.equalsIgnoreCase("끔")) {
			isOn = false;
			pc.sendPackets(new S_SystemMessage("킬 랭킹 차트를 숨깁니다. 리스 후 적용됩니다."));
		} else {
			pc.sendPackets(new S_SystemMessage(".차트 [켬/끔] (왼쪽상단 PK차트 리스트)"));
			return;
		}
		if (pc.getKDA() != null)
			pc.getKDA().isChartView = isOn;
	}
	
	private void testcheck(L1PcInstance pc){
		int grangKinOneStep = GrangKinConfig.GRANG_KIN_ANGER_ONE_STEP_LOGIN_TIME;
		int grangKinTwoStep = GrangKinConfig.GRANG_KIN_ANGER_TWO_STEP_LOGIN_TIME;
		int grangKinThreeStep = GrangKinConfig.GRANG_KIN_ANGER_THREE_STEP_LOGIN_TIME;
		int grangKinFourStep = GrangKinConfig.GRANG_KIN_ANGER_FOUR_STEP_LOGIN_TIME;
		int grangKinFiveStep = GrangKinConfig.GRANG_KIN_ANGER_FIVE_STEP_LOGIN_TIME;
		int grangKinSixStep = GrangKinConfig.GRANG_KIN_ANGER_SIX_STEP_LOGIN_TIME;
		
		int real_time = 0;
		int minute = 0;
		int hour = 0;
		if (pc.getAccount().getGrangKinAngerStat() == 1) {
			real_time = grangKinOneStep - pc.getGrangKinAngerSafeTime();
			hour = real_time >= 60 ? real_time / 60 : 0;
			minute = real_time % 60;
		} else if (pc.getAccount().getGrangKinAngerStat() == 2) {
			real_time = grangKinTwoStep - pc.getGrangKinAngerSafeTime();
			hour = real_time >= 60 ? real_time / 60 : 0;
			minute = real_time % 60;
		} else if (pc.getAccount().getGrangKinAngerStat() == 3) {
			real_time = grangKinThreeStep - pc.getGrangKinAngerSafeTime();
			hour = real_time >= 60 ? real_time / 60 : 0;
			minute = real_time % 60;
		} else if (pc.getAccount().getGrangKinAngerStat() == 4) {
			real_time = grangKinFourStep - pc.getGrangKinAngerSafeTime();
			hour = real_time >= 60 ? real_time / 60 : 0;
			minute = real_time % 60;
		} else if (pc.getAccount().getGrangKinAngerStat() == 5) {
			real_time = grangKinFiveStep - pc.getGrangKinAngerSafeTime();
			hour = real_time >= 60 ? real_time / 60 : 0;
			minute = real_time % 60;
		} else if (pc.getAccount().getGrangKinAngerStat() == 6) {
			real_time = grangKinSixStep - pc.getGrangKinAngerSafeTime();
			hour = real_time >= 60 ? real_time / 60 : 0;
			minute = real_time % 60;
		}

		if(real_time != 0){
			pc.sendPackets("그랑카인의 분노 해제시 까지 마을대기 " + hour + "시간 " + minute + "분 남았습니다.");
		} else {
			pc.sendPackets("현재 그랑카인의 분노 상태가 아닙니다.");
		}
	}
	
	private void clanMark(L1PcInstance pc, String param) {
		// TODO 자동 생성된 메소드 스텁
		try {
			long curtime = System.currentTimeMillis() / 1000L;
	          if (pc.getQuizTime() + 30L > curtime)
	          {
	            long time = pc.getQuizTime() + 30L - curtime;
	            pc.sendPackets(new S_ChatPacket(pc, time + " 초 후 사용할 수 있습니다."));
	            return;
	          }
	          if (pc.isDead())
	          {
	            pc.sendPackets(new S_SystemMessage("죽은 상태에선 사용할 수 없습니다."));
	            return;
	          }
	          int i = 1;
	          if (pc.문장주시)
	          {
	            i = 3;
	            pc.문장주시 = false;
	          }
	          else
	          {
	            pc.문장주시 = true;
	            pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "모든 혈맹의 마크를 표시하거나 종료하였습니다."));
	          }
	          for (L1Clan clan : L1World.getInstance().getAllClans()) {
	            if (clan != null) {
	              pc.sendPackets(new S_War(i, pc.getClanname(), clan.getClanName()));
	            }
	          }
	          pc.setQuizTime(curtime);
		} catch (Exception e) {
			pc.sendPackets(".혈마크 [켬 / 끔]");
		}
	}
	
	private void giftNCoin(L1PcInstance pc, String param) {
		try {
			StringTokenizer tok = new StringTokenizer(param);
			String targetName = tok.nextToken();
			int count = Integer.parseInt(tok.nextToken());
			L1PcInstance target = L1World.getInstance().getPlayer(targetName);
			
			if(target == null) {
				pc.sendPackets(targetName + " 님은 현재 접속중이지 않습니다.");
				return;
			}
			if(target.getNetConnection() == null) {
				pc.sendPackets(target + " 님은 올바른 접속상태가 아닙니다.");
				return;
			}
			if(pc == target) {
				pc.sendPackets("엔코인은 본인에게 양도할 수 없습니다.");
				return;
			}
			if(count > pc.getNcoin()) {
				pc.sendPackets("본인의 엔코인이 " + count + "(만)원 보다 적어 불가능합니다.");
				return;
			}			
			pc.addNcoin1(count);
			target.addNcoin(count);
			pc.sendPackets("본인의 엔코인 ["+count+"](만)원을 ["+targetName+"]님께 선물 하였습니다.");
			target.sendPackets("엔코인 ["+count+"](만)원을 ["+targetName+"]님께 선물 받으셨습니다.");
		}catch(Exception e) {
			pc.sendPackets(".엔코인선물 [케릭터명] [갯수]");
		}
	}
	  private void showHelp(L1PcInstance pc) { pc.sendPackets(new S_SystemMessage("\\aW--------------▶ 도 움 말 ◀--------------"));
	    pc.sendPackets(new S_SystemMessage("  \\aA텔렉(..) .암호변경  .라이트[켬/끔] .드랍멘트[켬/끔] "));
	    pc.sendPackets(new S_SystemMessage("  \\aA .혈맹파티  .혈마크[켬/끔] .파티멘트[켬/끔]"));
	    pc.sendPackets(new S_SystemMessage("  \\aA .캐릭명변경    .구슬조회   .외창[켬/끔]"));
	    pc.sendPackets(new S_SystemMessage("  \\aA .판매등록 .판매취소 .판매완료 "));
	    pc.sendPackets(new S_SystemMessage("  \\aA .구매신청 .구매취소 .후원이동"));
	    pc.sendPackets(new S_SystemMessage("  \\aA .캐릭터정보 .장비검사  .마일리지 "));
	    pc.sendPackets(new S_SystemMessage("\\aW------------ 럭 키 서 버 ------------")); 			 
	}
	void AdSc(L1PcInstance pc) {
		pc.sendPackets("----------------------------------------------------");
		pc.sendPackets("거래게시판: .판매등록 .판매완료 .구매신청 확인");
		pc.sendPackets("유저거래시세:("+Config.Ad_Sc+") 억 = 10,000원");
		pc.sendPackets("시세 이하 판매자/구매자 즉시 압류 조취");
		pc.sendPackets("----------------------------------------------------");
	}
	
	private void Ment(L1PcInstance pc, String param) {
		if (param.equalsIgnoreCase("끔")) {
			pc.sendPackets( "멘트: 끔");
			pc.RootMent = false;
		} else if (param.equalsIgnoreCase("켬")) {
			pc.sendPackets( "멘트: 켬");
			pc.RootMent = true;
		} else {
			pc.sendPackets( ".파티멘트 [켬 / 끔] 입력 하시면 됩니다.");
		}
	}

	/*private void Sealedoff(L1PcInstance pc, String param) {
		try {
			StringTokenizer tok = new StringTokenizer(param);
			String param1 = tok.nextToken();
			int off = Integer.parseInt(param1);
			if (off > 10 || off < 0) {
				pc.sendPackets("해제 주문서는 [10]이상 신청불가능합니다.");
				return;
			}
			if (off == 0) {
				pc.setSealScrollCount(0);
				pc.setSealScrollTime(0);
				pc.sendPackets("해제주문서 신청이 초기화되었습니다.");
			} else {
				int sealScrollTime = (int) (System.currentTimeMillis() / 1000) + 1 * 24 * 3600;
				pc.setSealScrollTime(sealScrollTime);
				pc.setSealScrollCount(off);
				pc.sendPackets("해제 주문서 [" + off + "]장이 신청되었습니다.");
				pc.sendPackets("오늘날짜로부터 [1]일 뒤에 자동지급 됩니다.");
				pc.sendPackets("재신청할경우 [1]일 초기화 됩니다.");
			}
			pc.save();
		} catch (Exception e) {
			pc.sendPackets(".봉인해제신청 [신청할 장수]");
		}
	}*/

	public void autoHunt(L1PcInstance pc, String param) {
		try {
			StringTokenizer st = new StringTokenizer(param);
			int type = Integer.parseInt(st.nextToken());
			int mapId = Integer.parseInt(st.nextToken());
			if (!((pc.getX() >= 33424 && pc.getX() <= 33436) && (pc.getY() >= 32807 && pc.getY() <= 32819)
					&& pc.getMapId() == 4)) {
				pc.sendPackets("\\aH기란 중앙탑 근처에서만 사용하실수 있습니다.");
				return;
			}
			if (!pc.getInventory().checkItem(3000209) && (!pc.getInventory().checkItem(3000213) && (!pc.getInventory().checkItem(3000214)))) {
				pc.sendPackets("\\aH자동사냥 사용권이 있어야 사용이 가능합니다.");
				return;
			}
			if (!pc.getInventory().checkItem(40308, 1000000)) {
				pc.sendPackets("\\aH최소 100만 아데나가 있어야 사용이 가능합니다.");
				return;
			}

			if(!AutoSystemController.check_auto_dungeon(pc)){
				pc.sendPackets("\\aH던전 이용시간이 종료 되었습니다(초기화는 부가상인)");
				return;
			}
			
			AutoSystemController auto = AutoSystemController.getInstance();
			if (type == 1) {
				pc.setAutoPotion(40010);
			} else if (type == 2) {
				pc.setAutoPotion(40011);
			} else {
				pc.sendPackets("\\aH자동셋팅은 1 / 2번 중에 하나입니다.");
				return;
			}
			if (mapId == 1) {// 초급
				if (pc.getLevel() < 70 || pc.getLevel() > 75) {
					pc.sendPackets("\\aG레벨 70~75 해당자만 이용이 가능 합니다.");
					return;
				}
				pc.setAutoMapId(612);
			} else if (mapId == 2) {// 중급
				if (pc.getLevel() < 76 || pc.getLevel() > 79) {
					pc.sendPackets("\\aG레벨 76~79 해당자만 이용이 가능 합니다.");
					return;
				}
				pc.setAutoMapId(254);
			} else if (mapId == 3) {// 상급
				if (pc.getLevel() < 80 || pc.getLevel() > 95) {
					pc.sendPackets("\\aG레벨 80~ 해당자만 이용이 가능 합니다.");
					return;
				}
				pc.setAutoMapId(1930);
			} else {
				pc.sendPackets("\\aH단계는 1(하) 2(중) 3(상) 중 입력바랍니다.");
				return;
			}
			pc.setAutoPolyID(0);
			pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\aG10초동안 변신을 해주시고, 원치 안으시면 변신창을 꺼주세요."));
			pc.sendPackets("자동사냥종료는 (자동사냥인증) 아이템을 더블클릭 하시면 됩니다.");
			pc.setAutoSetting(true);
			pc.sendPackets(new S_Ability(2, true));
			pc.sendPackets(new S_Message_YN(180));
			pc.sendPackets(new S_Ability(2, false));
			AutoHunt thread = new AutoHunt(pc, auto);
			thread.begin();
		} catch (Exception e) {
			pc.sendPackets(new S_ShowAutoInformation(pc));
			// pc.sendPackets("\\aD.자동사냥 [타입]
			// [단계(1.초급,2.상급)] 으로 실행해 주십시오.");
			pc.sendPackets( "\\aH.자동사냥 [\\aG타입\\aA] [\\aG단계1~3\\aH] (1:초급 2:중급 3:상급) 실행");
		}
	}

	class AutoHunt extends Thread {
		L1PcInstance _pc;
		AutoSystemController _auto;

		public AutoHunt(L1PcInstance pc, AutoSystemController auto) {
			_pc = pc;
			_auto = auto;
		}

		public void begin() {
			GeneralThreadPool.getInstance().schedule(this, 10000L);
		}

		@Override
		public void run() {
			try {
				_pc.setAutoStatus(_auto.AUTO_STATUS_MOVE_SHOP);
				_pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "귀하의 케릭터가 자동사냥을 시작합니다."));
				_pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\aG종료: \\aA자동사냥인증 -> 더블클릭시 종료됨"));
				_pc.sendPackets(new S_ChatPacket(_pc, "귀하의 케릭터가 자동사냥을 시작합니다.", 1));// 색상이특이하다.퍼런색
				_pc.setAutoSetting(false);
				AutoSystemController.getInstance().addAuto(_pc);
			} catch (Exception e) {
			}
		}
	}
	
	public static void doTarget(MJCommandArgs args) {
		try {
			String cmd = args.nextString();
			if (cmd.equalsIgnoreCase("켬")) {
				args.getOwner().setOnTargetEffect(true);
				args.notify("타겟팅 시스템이 활성화되었습니다.");
			} else if (cmd.equalsIgnoreCase("끔")) {
				args.getOwner().setOnTargetEffect(false);
				args.notify("타겟팅 시스템이 비활성화되었습니다.");
			} else if (cmd.equalsIgnoreCase("상태")) {
				args.notify(String.format("현재 타겟팅 시스템 상태 : %s", args.getOwner().isOnTargetEffect()));
			} else
				throw new Exception();
		} catch (Exception e) {
			args.notify(".타겟팅 [켬|끔|상태]");
		}
	}
	
	private void changename(L1PcInstance pc, String name) {
		try {
			if (pc.getLevel() >= 80) {
				int numOfNameBytes = 0;
				numOfNameBytes = name.getBytes("MS949").length;
				
				if (!pc.getInventory().checkItem(408991, 1)) { // 인벤 아이템 체크
					pc.sendPackets("캐릭명 변경권을 구입 후에 가능합니다.");
					return;
				}
				if (numOfNameBytes == 0) {
					pc.sendPackets(".캐릭명변경 [변경할 이름]");
					return;
				}
				if (pc.getClanid() != 0) {
					pc.sendPackets("혈맹을 잠시 탈퇴한 후 변경할 수 있습니다.");
					return;
				}
				if (pc.isCrown()) {
					pc.sendPackets("군주는 운영자와 상담 후 변경할 수 있습니다.");
					return;
				}
				if (pc.hasSkillEffect(1005) || pc.hasSkillEffect(2005)) {
					pc.sendPackets("채금 상태에는 변경할 수 없습니다.");
					return;
				}
				if (isInvalidName(name)) {
					pc.sendPackets("한글만 가능합니다.");
					return;
				}
				if (numOfNameBytes < 2 || numOfNameBytes > 12) {
					pc.sendPackets("한글 1~6자 사이로 입력 하시길 바랍니다.");
					return;
				}
				// level2
				if (CharacterTable.getInstance().isContainNameList(name) || MJBotNameLoader.isAlreadyName(name)) {
					pc.sendPackets("동일한 케릭명이 존재 합니다.");
					return;
				}
				
				if (BadNamesList.getInstance().isBadName(name)) {
					pc.sendPackets("생성 금지된 케릭명 입니다.");
					return;
				}
				for (int i = 0; i < name.length(); i++) {
					if (
					/*
					 * name.charAt(i) == 'ㄱ' || name.charAt(i) == 'ㄲ' || name.charAt(i) == 'ㄴ' ||
					 * name.charAt(i) == 'ㄷ' || // 한문자(char)단위로 비교. name.charAt(i) == 'ㄸ' ||
					 * name.charAt(i) == 'ㄹ' || name.charAt(i) == 'ㅁ' || name.charAt(i) == 'ㅂ' || //
					 * 한문자(char)단위로 비교 name.charAt(i) == 'ㅃ' || name.charAt(i) == 'ㅅ' ||
					 * name.charAt(i) == 'ㅆ' || name.charAt(i) == 'ㅇ' || // 한문자(char)단위로 비교
					 * name.charAt(i) == 'ㅈ' || name.charAt(i) == 'ㅉ' || name.charAt(i) == 'ㅊ' ||
					 * name.charAt(i) == 'ㅋ' || // 한문자(char)단위로 비교. name.charAt(i) == 'ㅌ' ||
					 * name.charAt(i) == 'ㅍ' || name.charAt(i) == 'ㅎ' || name.charAt(i) == 'ㅛ' || //
					 * 한문자(char)단위로 비교. name.charAt(i) == 'ㅕ' || name.charAt(i) == 'ㅑ' ||
					 * name.charAt(i) == 'ㅐ' || name.charAt(i) == 'ㅔ' || // 한문자(char)단위로 비교.
					 * name.charAt(i) == 'ㅗ' || name.charAt(i) == 'ㅓ' || name.charAt(i) == 'ㅏ' ||
					 * name.charAt(i) == 'ㅣ' || // 한문자(char)단위로 비교. name.charAt(i) == 'ㅠ' ||
					 * name.charAt(i) == 'ㅜ' || name.charAt(i) == 'ㅡ' name.charAt(i) == 'ㅒ' || //
					 * 한문자(char)단위로 비교. name.charAt(i) == 'ㅖ' || name.charAt(i) == 'ㅢ' ||
					 * name.charAt(i) == 'ㅟ' || name.charAt(i) == 'ㅝ' || // 한문자(char)단위로 비교.
					 * name.charAt(i) == 'ㅞ' || name.charAt(i) == 'ㅙ' || name.charAt(i) == 'ㅚ' ||
					 * name.charAt(i) == 'ㅘ' || // 한문자(char)단위로 비교.
					 */							
							name.charAt(i) == 'ㅆ' || name.charAt(i) == 'ㅃ' || name.charAt(i) == 'ㅒ' || name.charAt(i) == 'ㅖ' || // 한문자(char)단위로
							name.charAt(i) == 'ㅞ' || name.charAt(i) == 'ㅙ' || name.charAt(i) == 'ㅚ' || name.charAt(i) == 'ㅘ' || // 한문자(char)단위로
							name.charAt(i) == '씹' || name.charAt(i) == '좃' || name.charAt(i) == '좆' || name.charAt(i) == 'ㅤ' || name.charAt(i) == 'ㄼ' || name.charAt(i) == 'ㅄ' || name.charAt(i) == 'ㄻ'
							|| name.charAt(i) == 'ㅤ') {
						pc.sendPackets("케릭명이 올바르지 않습니다.");
						return;
					}
				}
				for (int i = 0; i < name.length(); i++) {
					if (!Character.isLetterOrDigit(name.charAt(i))) {
						pc.sendPackets("케릭명이 올바르지 않습니다.");
						return;
					}
				}
				
				if (!isAlphaNumeric(name)) {// 특수문자
					pc.sendPackets("특수문자는 금지되어 있습니다.");
					return;
				}
				if (pc.getInventory().checkItem(408991, 1)) { // 인벤 아이템 체크
					Updator.exec("UPDATE characters SET char_name=? WHERE char_name=?", new Handler(){
						@Override
						public void handle(PreparedStatement pstm) throws Exception {
							pstm.setString(1, name);
							pstm.setString(2, pc.getName());
						}
					});
					pc.save(); // 저장

					// /****** LogDB 라는 폴더를 미리 생성 해두세요 *******/
					Calendar rightNow = Calendar.getInstance();
					int year = rightNow.get(Calendar.YEAR);
					int month = rightNow.get(Calendar.MONTH) + 1;
					int date = rightNow.get(Calendar.DATE);
					int hour = rightNow.get(Calendar.HOUR);
					int min = rightNow.get(Calendar.MINUTE);
					String stryyyy = "";
					String strmmmm = "";
					String strDate = "";
					String strhour = "";
					String strmin = "";
					stryyyy = Integer.toString(year);
					strmmmm = Integer.toString(month);
					strDate = Integer.toString(date);
					strhour = Integer.toString(hour);
					strmin = Integer.toString(min);
					String str = "";
					str = new String("[" + stryyyy + "-" + strmmmm + "-" + strDate + " " + strhour + ":" + strmin + "] "
							+ pc.getName() + " ---> " + name);
					StringBuffer FileName = new StringBuffer("LogDB/캐릭명변경.txt");
					PrintWriter out = null;
					
					try {
						out = new PrintWriter(new FileWriter(FileName.toString(), true));
						out.println(str);
						out.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					str = "";// 초기화
					pc.getInventory().consumeItem(408991, 1); // 주문서 삭제
					buddys(pc); // 친구 삭제
					편지삭제(pc); // 편지삭제
					
					GeneralThreadPool.getInstance().schedule(new Runnable(){
						@Override
						public void run(){
							GameClient clnt = pc.getNetConnection();
							C_NewCharSelect.restartProcess(pc);
							Account acc		= clnt.getAccount();
							clnt.sendPacket(new S_CharAmount(acc.countCharacters(), acc.getCharSlot()));
							if(acc.countCharacters() > 0)
								C_CommonClick.sendCharPacks(clnt);							
						}
					}, 500L);
					
					Thread.sleep(500);
					pc.sendPackets(new S_Disconnect());
					pc.logout();
				} else {
					pc.sendPackets("캐릭명 변경권이 없습니다.");
				}
			} else {
				pc.sendPackets("레벨 80 이하는 불가능 합니다.");
			}
		} catch (Exception e) {
			pc.sendPackets(".이름변경 [변경할 이름] 으로 입력하시길 바랍니다.");
		}
	}

	public static boolean isAlphaNumeric(String s) {
		if (s == null) {
			return false;
		}
		boolean flag = true;
		char ac[] = s.toCharArray();
		int i = 0;
		do {
			if (i >= ac.length) {
				break;
			}
			if (!Character.isLetterOrDigit(ac[i])) {
				flag = false;
				break;
			}
			if(getHanguelType(ac[i])){
				flag = false;
				break;
			}
			i++;
		} while (true);
		return flag;
	}

	 public static boolean getHanguelType(char word) {
	    	String str = Character.toString(word);
	        return Pattern.matches("^[a-zA-Z0-9]*$", str);
	 }
	/********* 디비 친구목록에서 변경된 아이디 지우기 ************/
	public static void buddys(L1PcInstance pc) {
		Connection con = null;
		PreparedStatement pstm = null;
		String aaa = pc.getName();
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("DELETE FROM character_buddys WHERE buddy_name=?");

			pstm.setString(1, aaa);
			pstm.execute();
		} catch (SQLException e) {
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public static void 편지삭제(L1PcInstance pc) {
		Connection con = null;
		PreparedStatement pstm = null;

		String aaa = pc.getName();

		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("DELETE FROM letter WHERE receiver=?");
			pstm.setString(1, aaa);
			pstm.execute();
			// System.out.println("....["+ aaa +"].....");
		} catch (SQLException e) {
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	private void hunt(L1PcInstance pc, String cmd){
		int price = 30000000;
		try{
			StringTokenizer tok = new StringTokenizer(cmd);
			String name = tok.nextToken();
			if(name == null || name.equals(""))
				throw new Exception();
			
			L1PcInstance target = L1World.getInstance().getPlayer(name);
			if(target == null){
				pc.sendPackets(String.format("%s 님을 찾을 수 없습니다.", name));
				return;
			}
			if(target.hasSkillEffect(L1SkillId.USER_WANTED)){
				pc.sendPackets(String.format("%s 님은 이미 수배중입니다.", name));
				return;
			}
			if(target.isGm()){
				pc.sendPackets("운영자에게 수배를 걸수없습니다.");
				return;
			}
			if (!(pc.getInventory().checkItem(40308, price))) {
				pc.sendPackets("아데나가 부족합니다");
				return;
			}
			
			String message = String.format("%s 님께서 %s님에게 수배를 걸었습니다.", pc.getName(), target.getName());
			pc.sendPackets(message);
			target.sendPackets(message);
			
			target.setSkillEffect(L1SkillId.USER_WANTED, -1);
			target.doWanted(true , target.getTitle());
			
			pc.getInventory().consumeItem(40308, price);
		}catch(Exception e){
			pc.sendPackets("\\aA.수배 [캐릭터명] [3000만]");
			pc.sendPackets("\\aA 효과 : 추타+3,리덕+3,SP+3,AC-3");
		}
	} 

	private void phone(L1PcInstance pc, String param) {
		try {
			long curtime = System.currentTimeMillis() / 1000;
			if (pc.getQuizTime() + 10 > curtime) {
				long sec = (pc.getQuizTime() + 10) - curtime;
				pc.sendPackets(sec + "초 후에 사용할 수 있습니다.");
				return;
			}
			
			StringTokenizer tok = new StringTokenizer(param);
			String phone = tok.nextToken();
			Account account = Account.load(pc.getAccountName());
			if (param.length() < 10) {
				pc.sendPackets("없는 번호입니다. 다시 입력해주세요.");
				pc.sendPackets("* 경고: 잘못된 정보 입력시 게임 이용에 제재를 받을 수 있습니다.");
				return;
			}
			if (param.length() > 11) {
				pc.sendPackets("잘못된 번호입니다. 다시 입력해주세요.");
				pc.sendPackets("* 경고: 잘못된 정보 입력시 게임 이용에 제재를 받을 수 있습니다.");
				return;
			}
			if (isDisitAlpha(phone) == false) {
				pc.sendPackets("숫자로만 입력하세요.");
				pc.sendPackets("* 경고: 잘못된 정보 입력시 게임 이용에 제재를 받을 수 있습니다.");
				return;
			}
			if (account.getphone() != null) {
				pc.sendPackets("이미 전화번호가 설정되어 있습니다.");
				pc.sendPackets("번호 변경시 메티스에게 편지로 연락처를 보내세요.");
				return;
			}
			
			account.setphone(phone);
			Account.updatePhone(account);
			pc.sendPackets(" " + phone + " 설정 완료. 초기화 시 문자발송됩니다.");
			보안버프(pc);
		} catch (Exception e) {
			pc.sendPackets("고정을 하시게 되시면 재오픈 또는 공지를 사전에 알수있습니다.");
		}
	}
	
	private static void 보안버프(L1PcInstance pc) {
		pc.getAC().addAc(-1);
		pc.sendPackets(new S_PacketBox(pc, S_PacketBox.ICON_SECURITY_SERVICES));
		pc.sendPackets(new S_OwnCharStatus(pc));
	}

	private void spawnNotifyOnOff(L1PcInstance pc, String param){
		try{
			StringTokenizer st = new StringTokenizer(param);
			String on = st.nextToken();
			if (on.equalsIgnoreCase("켬")) {
				if(pc.isBossNotify()){
					pc.sendPackets("보스알림:보스소환 및 알림이 이미 활성화 상태입니다.");
					return;
				}
				pc.setBossNotify(true);
				pc.sendPackets("보스알림:켬 (보스소환 및 알림이 실행 되었습니다)");
			} else if (on.equals("끔")) {
				if(!pc.isBossNotify()){
					pc.sendPackets("보스알림:보스소환 및 알림이 활성화 상태가 아닙니다.");
					return;
				}
				pc.setBossNotify(false);
				pc.sendPackets("보스알림:끔 (보스소환 및 알림이 종료 되었습니다)");
			} else {
				pc.sendPackets("잘못된 요청입니다.");
			}
		}catch(Exception e){
			pc.sendPackets(".보스알림 [켬, 끔]으로 설정하세요. 현재(" + (pc.isBossNotify() == true ? "켜짐" : "꺼짐") + ")입니다.");
		}
	}
	
	private void maphack(L1PcInstance pc, String param) {
		try {
			StringTokenizer st = new StringTokenizer(param);
			String on = st.nextToken();
			if (pc.getMapId() == 132) {
				pc.sendPackets(new S_Ability(3, false));
				pc.sendPackets( "현재 맵에서는 해당명령어를 사용할 수 없습니다.");
				return;
			}
			if (on.equalsIgnoreCase("켬")) {
				pc.sendPackets(new S_Ability(3, true));
				pc.sendPackets("라이트: 켜짐");
			} else if (on.equals("끔")) {
				pc.sendPackets(new S_Ability(3, false));
				pc.sendPackets("라이트: 꺼짐");
			}
		} catch (Exception e) {
			pc.sendPackets(".라이트 켬 또는 끔 으로 설정 해주시길 바랍니다.");
		}
	}
	
	private void DropMent(L1PcInstance pc, String param) {
	      if (param.equalsIgnoreCase("끔")) {
	         pc.sendPackets( "\\f:드랍멘트가 OFF 되었습니다.");
//	         pc.sendPackets( "\\f:기능: 오토루팅템 멘트가 비활성화 되었습니다.");
	         pc.RootMent = false;
	      } else if (param.equalsIgnoreCase("켬")) {
	         pc.sendPackets( "\\f:드랍멘트가 ON 되었습니다.");
//	         pc.sendPackets( "\\f:기능: 오토루팅템 멘트가 활성화 되었습니다.");
	         pc.RootMent = true;
	      } else {
	         pc.sendPackets( ".드랍멘트 [켬/끔] 입력 하시면 됩니다.");
	      }
	   }
	
	private void 복구(L1PcInstance pc) {
		try {
			long curtime = System.currentTimeMillis() / 1000;
			if (pc.getQuizTime2() + 5 > curtime) {
				long time = (pc.getQuizTime2() + 5) - curtime;
				pc.sendPackets( time + "초 후 사용할 수 있습니다.");
				return;
			}
			Updator.exec("UPDATE characters SET LocX=33432,LocY=32807,MapID=4 WHERE account_name=? and MapID not in (38,5001,99,997,5166,39,34,701,2000)", new Handler(){
				@Override
				public void handle(PreparedStatement pstm) throws Exception {
					pstm.setString(1, pc.getAccountName());
				}
			});			
			pc.sendPackets("모든 케릭터의 좌표가 정상적으로 복구 되었습니다.");
			pc.setQuizTime(curtime);
		} catch (Exception e) {
		}
	}

	private void tell(L1PcInstance pc) {

		long curtime = System.currentTimeMillis() / 1000;
		if (pc.getQuizTime2() + 5 > curtime) {
			long time = (pc.getQuizTime2() + 5) - curtime;
			pc.sendPackets( time + "초 후 사용할 수 있습니다.");
			return;
		}
		try {
			if (pc.getMapId() == 781) {
				if (pc.getLocation().getX() <= 32998 && pc.getLocation().getX() >= 32988
						&& pc.getLocation().getY() <= 32758 && pc.getLocation().getY() >= 32736) {
					pc.sendPackets( "사용할 수 없는 장소입니다.");
					return;
				}
			}
			if (pc.hasSkillEffect(SHOCK_STUN) || pc.hasSkillEffect(DESPERADO) || pc.hasSkillEffect(L1SkillId.EMPIRE) || pc.hasSkillEffect(EARTH_BIND)
					|| pc.hasSkillEffect(CURSE_PARALYZE) || pc.hasSkillEffect(ICE_LANCE)
					|| pc.hasSkillEffect(FOG_OF_SLEEPING) || pc.hasSkillEffect(BONE_BREAK)) {
				pc.sendPackets( "현재 사용이 불가능한 상태 입니다.");
				return;
			}
			if (pc.getMapId() == 132) {
				pc.sendPackets(new S_Ability(3, false));
			}
			/*
			 * if (CharPosUtil.getZoneType(pc) == 0 && castle_id != 0) { // 공성장
			 * 주변에서 불가능 pc.sendPackets("사용할 수 없는 장소입니다.");
			 * return; }
			 */
			if (pc.isPinkName() || pc.isDead() || pc.isParalyzed() || pc.isSleeped() || pc.getMapId() == 800 || pc.getMapId() == 12150 || pc.getMapId() == 12154
					|| pc.getMapId() == 5302 || pc.getMapId() == 5153 || pc.getMapId() == 5490) {
				pc.sendPackets( "사용할 수 없는 상태입니다.");
				return;
			}
			pc.sendPackets(new S_PacketBox(S_PacketBox.공격가능거리, pc, pc.getWeapon()), true);
			pc.start_teleport(pc.getX(), pc.getY(), pc.getMapId(), pc.getHeading(), 169, true, false);
			pc.update_lastLocalTellTime();
			pc.setQuizTime2(curtime);
		} catch (Exception exception35) {
		}
	}

	public void BloodParty(L1PcInstance pc) {
		if (pc.isDead()) {
			pc.sendPackets( "죽은 상태에선 사용할 수 없습니다.");
			return;
		}
		int ClanId = pc.getClanid();
		if (ClanId != 0 && pc.getClanRank() == L1Clan.군주 || pc.getClanRank() == L1Clan.수호
				|| pc.getClanRank() == L1Clan.부군주) {
			for (L1PcInstance SearchBlood : L1World.getInstance().getAllPlayers()) {
				if (SearchBlood.getClanid() != ClanId || SearchBlood.isPrivateShop() || SearchBlood.isInParty()) { // 클랜이
					continue; // 포문탈출
				} else if (SearchBlood.getName() != pc.getName()) {
					pc.setPartyType(1); // 파티타입 설정
					SearchBlood.setPartyID(pc.getId()); // 파티아이디 설정
					SearchBlood.sendPackets(new S_Message_YN(954, pc.getName()));
					pc.sendPackets( SearchBlood.getName() + " 님에게 파티를 신청했습니다");
				}
			}
		} else { // 클랜이 없거나 군주 또는 수호기사 [X]
			pc.sendPackets( "혈맹이 있으면서 군주, 부군주, 수호기사라면 사용가능.");
		}
	}

	private void age(L1PcInstance pc, String cmd) {
		try {
			StringTokenizer tok = new StringTokenizer(cmd);
			String AGE = tok.nextToken();
			int AGEint = Integer.parseInt(AGE);
			if (AGEint > 59 || AGEint < 14) {
				pc.sendPackets( "자신의 실제 나이로 설정하세요.");
				return;
			}
			pc.setAge(AGEint);
			pc.save();
			pc.sendPackets( "명령어: 당신의 나이가 [" + AGEint + "] 설정되었습니다.");
		} catch (Exception e) {
			pc.sendPackets( ".나이 숫자 형식으로 입력.(혈맹 채팅 시 표시됨)");
		}
	}
	
	private static boolean isDisitAlpha(String str) {
		boolean check = true;
		for (int i = 0; i < str.length(); i++) {
			if (!Character.isDigit(str.charAt(i)) // 숫자가 아니라면
					&& !Character.isUpperCase(str.charAt(i)) // 대문자가 아니라면
					&& !Character.isLowerCase(str.charAt(i))) { // 소문자가 아니라면
				check = false;
				break;
			}
		}
		return check;
	}

	private void changepassword(L1PcInstance pc, String param) {
		try {
			if (pc.get_lastPasswordChangeTime() + 10 * 60 * 1000 > System.currentTimeMillis()) {
				pc.sendPackets( "암호를 변경하신지 10분이 지나지 않았습니다. 잠시후 다시 변경하세요.");
				return;
			}
			StringTokenizer tok = new StringTokenizer(param);
			String newpasswd = tok.nextToken();
			if (newpasswd.length() < 6) {
				pc.sendPackets( "6자 ~ 16자 사이의 영어나 숫자로 입력하세요.");
				return;
			}
			if (newpasswd.length() > 16) {
				pc.sendPackets( "6자 ~ 16자 사이의 영어나 숫자로 입력하세요.");
				return;
			}
			if (isDisitAlpha(newpasswd) == false) {
				pc.sendPackets( "영어와 숫자로만 입력하세요.");
				return;
			}
			to_Change_Passwd(pc, newpasswd);

		} catch (Exception e) {
			pc.sendPackets( ".암호변경 [변경할암호]로 입력 하세요.");
		}
	}

	private void to_Change_Passwd(L1PcInstance pc, String passwd) {
		Selector.exec("select account_name from characters where char_name=?", new SelectorHandler(){
			@Override
			public void handle(PreparedStatement pstm) throws Exception {
				pstm.setString(1, pc.getName());
			}
			@Override
			public void result(ResultSet rs) throws Exception {
				while(rs.next()){
					final String login = rs.getString("account_name");
					Updator.exec("update accounts set password=? where login=?", new Handler(){
						@Override
						public void handle(PreparedStatement pstm) throws Exception {
							pstm.setString(1, passwd);
							pstm.setString(2,  login);
						}
					});
				}
			}
		});
		pc.sendPackets(String.format("당신의 계정 암호가 (%s)로 변경되었습니다.", passwd));
	}

	// 패스워드 맞는지 여부 리턴
	public static boolean isPasswordTrue(String Password, String oldPassword) {
		String _rtnPwd = null;
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		boolean result = false;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT password(?) as pwd");

			pstm.setString(1, oldPassword);
			rs = pstm.executeQuery();
			if (rs.next()) {
				_rtnPwd = rs.getString("pwd");
			}
			if (_rtnPwd.equals(Password)) { // 동일하다면
				result = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
		return result;
	}

	public static void privateShop(L1PcInstance pc) {
		try {
			if (!pc.isPrivateShop()) {
				pc.sendPackets( "개인상점 상태에서 사용이 가능합니다.");
				return;
			}
			for (L1PcInstance target : L1World.getInstance().getAllPlayers3()) {
				if (target.getId() != pc.getId() && target.getAccountName().toLowerCase().equals(pc.getAccountName().toLowerCase()) && target.isPrivateShop()) {
					pc.sendPackets("\\f3이미 당신의 보조 캐릭터가 무인상점 상태입니다.");
					pc.sendPackets("\\f3상점을 종료하시길 바랍니다. /상점");
					return;
				}
			}
			GameClient client = pc.getNetConnection();
			pc.setNetConnection(null);
			pc.stopHpMpRegeneration();
			pc.set무인상점(true);
			try {
				pc.save();
				pc.saveInventory();
			} catch (Exception e) {
			}
			client.setActiveChar(null);
			client.setStatus2(MJClientStatus.CLNT_STS_AUTHLOGIN);
			client.sendPacket(new S_Unknown2(1)); // 리스버튼을 위한 구조변경 // Episode U

		} catch (Exception e) {
		}
	}
	
	private void outsideChat(L1PcInstance pc, String param){
		try{
			if(param.equalsIgnoreCase("켬")){
				pc.setOutSideChat(true);
			}else if(param.equalsIgnoreCase("끔")){
				pc.setOutSideChat(false);				
			}else if(param.equalsIgnoreCase("상태")){
				
			}else
				throw new Exception();
			
			pc.sendPackets(String.format("외창 : %s", pc.isOutsideChat() ? "켜짐" : "꺼짐"));
		}catch(Exception e){
			pc.sendPackets(".외창 [켬/끔/상태]");
		}
	}
	
	private String parseStat(String s) throws Exception{
		if(s.equalsIgnoreCase("힘"))
			return "str";
		else if(s.equalsIgnoreCase("덱스"))
			return "dex";
		else if(s.equalsIgnoreCase("콘"))
			return "con";
		else if(s.equalsIgnoreCase("인트"))
			return "int";
		else if(s.equalsIgnoreCase("위즈"))
			return "wis";
		else if(s.equalsIgnoreCase("카리"))
			return "cha";
		throw new Exception(s);
	}
	
	public void fullstat(L1PcInstance pc, String param){
		try{
			String[] arr = param.split(" ");
			if(arr == null || arr.length < 2)
				throw new Exception();
			
			String s = parseStat(arr[0]);
			MJFullStater.running(pc, s, Integer.parseInt(arr[1]));
		}catch(Exception e){
			pc.sendPackets(String.format(".원스텟 [힘/덱스/콘/인트/위즈/카리] [올릴수] 남은스텟 %d", (pc.getLevel() - 50 - pc.getBonusStats())));
		}
	}
	
	// TODO 중개 거래 게시판
			private static void LetterList(L1PcInstance pc, int type, int count) {
				pc.sendPackets(new S_LetterList(pc, type, count));
			}
			
			public static void WriteCancle(String buyer, String seller) {
				int nu1 = 949;
				SimpleDateFormat formatter = new SimpleDateFormat("yy/MM/dd", Locale.KOREA);
				Date currentTime = new Date();
				String dTime = formatter.format(currentTime);
				String subject = "구매취소내역";
				String content = "구매자 :" + buyer + "" + "\n\n상대방이 구매를 취소하엿습니다." + "\n입금 대기상태로 바뀝니다.";
				String name = "거래관리자";
				L1PcInstance target = L1World.getInstance().getPlayer(seller);
				LetterTable.getInstance().writeLetter(nu1, dTime, name, seller, 0, subject, content);
				if (target != null && target.getOnlineStatus() != 0) {
					target.sendPackets(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "구매취소: " + buyer + "님이 구매를 취소 하였습니다."));
					LetterList(target, 0, 20);
					target.sendPackets(new S_SkillSound(target.getId(), 1091));
					target.sendPackets(new S_ServerMessage(428));
				}
			}

			public static void WriteComplete(String buyer, int count, String seller) {
				int nu1 = 949;
				SimpleDateFormat formatter = new SimpleDateFormat("yy/MM/dd", Locale.KOREA);
				Date currentTime = new Date();
				String dTime = formatter.format(currentTime);
				String subject = "판매완료내역";
				String content = "아데나 :" + count + "개" + "\n판매자 :" + seller + "" + "\n물품 구매가 완료되었습니다. " + "\n\n반드시 리스타트 후에 "
						+ "\n부가 아이템 창고에서" + "\n아데나를 수령하십시요.";
				String name = "거래관리자";
				L1PcInstance target = L1World.getInstance().getPlayer(buyer);
				LetterTable.getInstance().writeLetter(nu1, dTime, name, buyer, 0, subject, content);
				if (target != null && target.getOnlineStatus() != 0) {
					//target.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "판매완료: 아데나가 '부가아이템창고'로 지급 되었습니다(리스타트후 수령가능)"));
					target.killSkillEffectTimer(BUYER_COOLTIME);
					LetterList(target, 0, 20);
					target.sendPackets(new S_SkillSound(target.getId(), 1091));
					target.sendPackets(new S_ServerMessage(428));
				}
			}

			public static void WriteBuyLetter(String player, int count, int sellcount, String bankname, String name,
					String number, String sellername, String buyername) {
				int nu1 = 949;
				SimpleDateFormat formatter = new SimpleDateFormat("yy/MM/dd", Locale.KOREA);
				Date currentTime = new Date();
				String dTime = formatter.format(currentTime);
				String subject = "구매 신청 내역";
				String content = "구매 수표 : " + count + "개." + "\n구매 금액 : " + sellcount + "원. " + "\n은행 : " + bankname + ""
						+ "\n예금주 : " + name + "" + "\n계좌번호 : " + number + "" + "\n판매자 캐릭터 : " + sellername + "" + "\n구매자 캐릭터 : "
						+ buyername + "" + "\n\n판매자는 입금완료후 반드시" + "\n.판매완료 (게시번호) 를 하셔야" + "\n구매자에게 아데나가 지급됩니다."
						+ "\n\n판매자가 악의적으로 판매완료를" + "\n거부할 경우 메티스로 문의." + "\n입금전 반드시 상대방이" + "\n접속중인지 확인후 거래를"
						+ "\n진행해 주시기 바랍니다.";
				String project = "거래관리자";
				L1PcInstance target = L1World.getInstance().getPlayer(player);
				LetterTable.getInstance().writeLetter(nu1, dTime, project, player, 0, subject, content);
				if (target != null && target.getOnlineStatus() != 0) {
					LetterList(target, 0, 20);
					target.sendPackets(new S_SkillSound(target.getId(), 1091));
					target.sendPackets(new S_ServerMessage(428));
				}
			}

			public static void WriteSellLetter(String player, int count, int sellcount, String bankname, String name,
					String number, String charname) {
				int nu1 = 949;
				SimpleDateFormat formatter = new SimpleDateFormat("yy/MM/dd", Locale.KOREA);
				Date currentTime = new Date();
				String dTime = formatter.format(currentTime);
				String subject = "판매 등록 내역";
				String content = "판매 수표 : " + count + "개." + "\n판매 금액 : " + sellcount + "원. " + "\n은행 : " + bankname + ""
						+ "\n예금주 : " + name + "" + "\n계좌번호 : " + number + "" + "\n판매자 캐릭터 : " + charname + ""
						+ "\n\n위 내용과 상이할 경우 " + "\n.판매취소 (게시글번호)" + "\n하신후 재등록 하시기 바랍니다." + "\n\n잘못된 정보 기재로 인한 피해사항은"
						+ "\n어떤한 경우에도 책임지지 않습니다.";
				String project = "거래관리자";
				L1PcInstance target = L1World.getInstance().getPlayer(player);
				LetterTable.getInstance().writeLetter(nu1, dTime, project, player, 0, subject, content);
				if (target != null && target.getOnlineStatus() != 0) {
					LetterList(target, 0, 20);
					target.sendPackets(new S_SkillSound(target.getId(), 1091));
					target.sendPackets(new S_ServerMessage(428));
				}
			}

			private static void countR1(L1PcInstance pc, String count) {
	            StringTokenizer st = new StringTokenizer(count);
	            try {
	               int i = 0;
	               int sellcount = 0;
	               String bankname = null;
	               String name = null;
	               String numeber = null;
	               try {
	                  i = Integer.parseInt(st.nextToken());
	                  sellcount = Integer.parseInt(st.nextToken());
	                  bankname = st.nextToken();
	                  name = st.nextToken();
	                  numeber = st.nextToken();
	               } catch (NumberFormatException e) {
	               }

	               if (i <= 0) {
	                  pc.sendPackets(".판매등록  (판매금액)을 적어주세요.");
	                  return;
	               }
	               if (pc.getLevel() < Config.ADENASHOP_LEVEL) {
	                  pc.sendPackets("판매등록 최소 (" + Config.ADENASHOP_LEVEL + ")레벨 이상만 등록 가능합니다.");
	                  return;
	               }
	               if (i > Config.MAX_SELL_ADENA) {
	                  pc.sendPackets("최대 수표판매 개수는 " + Config.MAX_SELL_ADENA + " 이상 판매하실 수 없습니다.");
	                  return;
	               }
	               if (i >= 1 && i < Config.MIN_SELL_ADENA) {
	                  pc.sendPackets("최소 수표판매 개수는 " + Config.MIN_SELL_ADENA + " 이상 입니다.");
	                  return;
	               }
	               if (sellcount < Config.MIN_SELL_CASH) {
	                  pc.sendPackets("최소판매 "+Config.MIN_SELL_ADENA+"억당 " + Config.MIN_SELL_CASH + "원 이상 입니다.");
	                  return;
	               }
	               if (sellcount > Config.MAX_SELL_CASH) {
	                  pc.sendPackets("최대판매는 " + Config.MAX_SELL_CASH + "원 이하 입니다.");
	                  return;
	               }

	               String selltype = "판매중";
	               
	               int Adena_supo = 400254;
	               L1Item temp = ItemTable.getInstance().getTemplate(Adena_supo);
	               L1ItemInstance adena = new L1ItemInstance(temp, i);

	               if (pc.getInventory().checkItem(Adena_supo, i)) {
	                  pc.getInventory().consumeItem(Adena_supo, i);
	                  AuctionSystemTable.getInstance().writeTopic(pc, selltype, adena, i, sellcount, bankname, numeber, name);
	                  WriteSellLetter(pc.getName(), i, sellcount, bankname, name, numeber, pc.getName());
	                  WriteSellLetter("메티스", i, sellcount, bankname, name, numeber, pc.getName());
	               } else {
	                  pc.sendPackets("입력하신 숫자가 소지하신 수표개수보다 작습니다.");
	               }
	            } catch (Exception exception) {
	               pc.sendPackets(".판매등록 [수표수량] [판매금액] [은행명] [예금주] [계좌]");
	               pc.sendPackets(".판매등록  "+Config.MIN_SELL_ADENA+" 10000 농협 홍길동 123123(-제외)");
	            }
	         }

	         private static void countR2(L1PcInstance pc, String count) {
	            if (pc.hasSkillEffect(BUYER_COOLTIME)) {
	               int n = pc.getSkillEffectTimeSec(BUYER_COOLTIME);
	               pc.sendPackets("이미 다른 물품을 구매중이거나 쿨타임이 적용중입니다.");
	               pc.sendPackets(new S_SystemMessage(String.format("%d초 후에 구매신청을 하실 수 있습니다.", n)));
	               return;
	            }
	            int i = 0;
	            Connection con = null;
	            PreparedStatement pstm = null;
	            ResultSet rs = null;

	            try {
	               i = Integer.parseInt(count);
	            } catch (NumberFormatException e) {
	            }
	            String charname = null;
	            int adenacount = 0;
	            int sellcount = 0;
	            int status = 0;
	            String bank = null;
	            String banknumeber = null;
	            String bankname = null;
	            try {
	               con = L1DatabaseFactory.getInstance().getConnection();
	               pstm = con.prepareStatement(
	                     "SELECT  id, name, count, sellcount, status, bank, banknumber, bankname from Auction where id Like '"
	                           + i + "'");
	               rs = pstm.executeQuery();
	               while (rs.next()) {
	                  charname = rs.getString("name");
	                  adenacount = rs.getInt("count");
	                  sellcount = rs.getInt("sellcount");
	                  status = rs.getInt("status");
	                  bank = rs.getString("bank");
	                  banknumeber = rs.getString("banknumber");
	                  bankname = rs.getString("bankname");
	               }

	               if (pc.getLevel() < Config.ADENASHOP_BUY_LEVEL) {
		                  pc.sendPackets("구매신청 최소 (" + Config.ADENASHOP_BUY_LEVEL + ")레벨 이상만 신청 가능합니다.");
		                  return;
		               }
	               
	               if (charname == null) {
	                  pc.sendPackets("잘못된 게시번호 입니다.");
	                  return;
	               }
	               if (status == 1) {
	                  pc.sendPackets("이미 다른사람이 구매신청을 한 상태입니다.");
	                  return;
	               }
	               if (status == 2) {
	                  pc.sendPackets("이미 판매 완료된 물품입니다.");
	                  return;
	               }

	               try {
	                  int time = 600;
	                  WriteBuyLetter(pc.getName(), adenacount, sellcount, bank, bankname, banknumeber, charname,
	                        pc.getName());
	                  WriteBuyLetter(charname, adenacount, sellcount, bank, bankname, banknumeber, charname, pc.getName());
	                  WriteBuyLetter("메티스", adenacount, sellcount, bank, bankname, banknumeber, charname, pc.getName());
	                  AuctionSystemTable.getInstance().AuctionUpdate(i, pc.getName(), pc.getAccountName(), "입금대기", 1);
	                  pc.setSkillEffect(BUYER_COOLTIME, time * 1000);
	               } catch (Exception e) {
	                  e.printStackTrace();
	               }

	            } catch (SQLException e) {
	               pc.sendPackets(".구매신청 (게시글번호)");
	               pc.sendPackets(".구매신청 0017");
	            } finally {
	               SQLUtil.close(rs, pstm, con);
	            }
	         }

	         private static void countR3(L1PcInstance pc, String count) {
	            int i = 0;
	            Connection con = null;
	            PreparedStatement pstm = null;
	            ResultSet rs = null;
	            try {
	               i = Integer.parseInt(count);
	            } catch (NumberFormatException e) {
	            }
	            int itemid = 0;
	            int itemcount = 0;
	            String account = null;
	            int status = 0;
	            try {
	               con = L1DatabaseFactory.getInstance().getConnection();
	               pstm = con.prepareStatement(
	                     "SELECT  item_id ,count ,AccountName ,status from Auction where id Like '" + i + "'");
	               rs = pstm.executeQuery();
	               while (rs.next()) {
	                  itemid = rs.getInt("item_id");
	                  itemcount = rs.getInt("count");
	                  account = rs.getString("AccountName");
	                  status = rs.getInt("status");
	               }
	               if (status == 1) {
	                  pc.sendPackets("입금 대기상태 에서는 판매취소가 불가능 합니다.");
	                  return;
	               }
	               if (status == 2) {
	                  pc.sendPackets("판매가 완료된 상태입니다.");
	                  return;
	               }
	               if (pc.getAccountName().equalsIgnoreCase(account) || pc.isGm()) {
	            	   
	            	   //itemcount =  (itemcount - (int)(itemcount*0.20));
	            	   
	                  pc.getInventory().storeItem(itemid, itemcount);
	                  AuctionSystemTable.getInstance().deleteTopic(i);
	                  pc.sendPackets("정상취소 되었습니다");
	               } else {
	                  pc.sendPackets("등록하신 물품이 아닙니다");
	                  pc.sendPackets(".판매취소 (게시글번호) 라고 적어주십시요.");
	               }
	            } catch (SQLException e) {
	               pc.sendPackets(".판매취소 (게시글번호) 라고 적어주십시요.");
	               pc.sendPackets("예)  .판매취소 0035");
	            } finally {
	               SQLUtil.close(rs, pstm, con);
	            }
	         }

	         private static void countR4(L1PcInstance pc, String count) {
	            int i = 0;
	            Connection con = null;
	            PreparedStatement pstm = null;
	            ResultSet rs = null;

	            try {
	               i = Integer.parseInt(count);
	            } catch (NumberFormatException e) {
	            }
	            String charname = null;
	            int sellcount = 0;
	            int status = 0;
	            String buyerName = null;
	            String buyerAccountName = null;
	            try {
	               con = L1DatabaseFactory.getInstance().getConnection();
	               pstm = con.prepareStatement(
	                     "SELECT  name, count, status, buyername, buyerAccountName from Auction where id Like '" + i + "'");
	               rs = pstm.executeQuery();
	               while (rs.next()) {
	                  charname = rs.getString("name");
	                  sellcount = rs.getInt("count");
	                  status = rs.getInt("status");
	                  buyerName = rs.getString("buyername");
	                  buyerAccountName = rs.getString("buyerAccountName");
	               }

	               if (status == 1) {
	            	   //구매자가 접속안할때 체크 추가
	            	  L1PcInstance tg = L1World.getInstance().getPlayer(buyerName);
//	   				  if (tg != null) {
//	   					  pc.sendPackets("구매자가 접속중이지 않습니다.");
//	   					  return;
//	   				  }
//	   				  
	                  if (pc.getName().equalsIgnoreCase(charname) || pc.isGm()) {
	                     AuctionSystemTable.getInstance().AuctionComplete(i);
	                     try {
	                    	 
	                    	 //sellcount =  (sellcount - (int)(sellcount*0.20));
	                    	 
	                        WriteComplete(buyerName, sellcount, charname);
	                       // WriteComplete("메티스", sellcount, charname);
	                        
	                        int itemid = 400254;
	                        
	                        L1PcInstance target = L1World.getInstance().getPlayer(buyerName);                        
	                        target.getInventory().storeItem(itemid , sellcount);
	                        
	                     } catch (Exception e) {
	                        e.printStackTrace();
	                     }
	                     pc.sendPackets("정상 판매 완료 되었습니다.");
	                  } else {
	                     pc.sendPackets("당신은 해당글의 판매자가 아닙니다.");
	                  }
	               } else {
	                  pc.sendPackets("아직 구매 신청자가 없습니다.");
	                  pc.sendPackets(".판매완료(게시글번호)");
	               }
	            } catch (SQLException e) {
	               pc.sendPackets(".판매완료(게시글번호)");
	               pc.sendPackets(".판매완료 0017");
	            } finally {
	               SQLUtil.close(rs, pstm, con);
	            }
	         }
	         
	         private static void countR5(L1PcInstance pc, String count) {
	            int i = 0;
	            Connection con = null;
	            PreparedStatement pstm = null;
	            ResultSet rs = null;
	            try {
	               i = Integer.parseInt(count);
	            } catch (NumberFormatException e) {
	            }
	            int status = 0;
	            String charname = null;
	            String buyername = null;
	            String buyeraccount = null;
	            try {
	               con = L1DatabaseFactory.getInstance().getConnection();
	               pstm = con.prepareStatement(
	                     "SELECT  name, status, buyername, buyerAccountName from Auction where id Like '" + i + "'");
	               rs = pstm.executeQuery();
	               while (rs.next()) {
	                  charname = rs.getString("name");
	                  status = rs.getInt("status");
	                  buyeraccount = rs.getString("buyerAccountName");
	                  buyername = rs.getString("buyername");
	               }
	               if (status == 0) {
	                  pc.sendPackets("구매 취소가 불가능한 상태입니다.");
	                  return;
	               }
	               if (status == 2) {
	                  pc.sendPackets("판매가 완료된 상태입니다.");
	                  return;
	               }
	               if (pc.getAccountName().equalsIgnoreCase(buyeraccount) || pc.isGm()) {
	                  AuctionSystemTable.getInstance().AuctionUpdate(i, "", "", "판매중", 0);
	                  WriteCancle(buyername, charname);
	                  WriteCancle("메티스", charname);
	                  pc.sendPackets("정상취소 되었습니다");
	               } else {
	                  pc.sendPackets("등록하신 물품이 아닙니다");
	               }
	            } catch (SQLException e) {
	               pc.sendPackets(".구매취소 (게시글번호) 라고 적어주십시요.");
	               pc.sendPackets("예)  .구매취소 0035");
	            } finally {
	               SQLUtil.close(rs, pstm, con);
	            }
	         }
			// TODO 중개 거래 게시판
			
			/*private static void do_lfc(L1PcInstance pc, String param){
				StringTokenizer token = new StringTokenizer(param);
				String name = token.nextToken();
				L1PcInstance target = L1World.getInstance().findpc(name);
				if(target == null){
					pc.sendPackets(String.format("%s님을 찾을 수 없습니다.", name));
				}
				L1BoardPost bp = L1BoardPost.createLfc(name, "-", String.format("3 %s", pc.getName()));
				MJLFCCreator.registLfc(pc, 3);
				pc.sendPackets(String.format("%s님에게 결투를 신청했습니다.", name));
				pc.sendPackets(MJSurveySystemLoader.getInstance().registerSurvey(String.format("%s님이 결투를 신청했습니다.", pc.getName()), bp.getId() + 1000, MJSurveyFactory.createLFCSurvey(), 30 * 1000));
			}*/
	         
	         private static boolean isInvalidName(String name) {
	     		int numOfNameBytes = 0;
	     		try {
	     			numOfNameBytes = name.getBytes("EUC-KR").length;
	     		} catch (UnsupportedEncodingException e) {
	     			e.printStackTrace();
	     			return false;
	     		}

	     		if (isAlphaNumeric(name)) {
	     			return false;
	     		}
	     		
	     		

	     		// XXX - 본청의 사양과 동등한가 미확인
	     		// 전각 문자가 5 문자를 넘는지, 전체로 12바이트를 넘으면(자) 무효인 이름으로 한다
	     		if (5 < (numOfNameBytes - name.length()) || 12 < numOfNameBytes) {
	     			return false;
	     		}

	     		if (BadNamesList.getInstance().isBadName(name)) {
	     			return false;
	     		}
	     		return true;
	     	}
	         
	         

	         public static void charSearchInfo(L1PcInstance pc, String param) {
	           try {
	             StringTokenizer st = new StringTokenizer(param);
	             String name = st.nextToken();
	             L1PcInstance target = L1World.getInstance().getPlayer(name);
	             if (target == null) {
	               pc.sendPackets(new S_ServerMessage(73, name));
	               return;
	             }

	             if (!pc.getInventory().checkItem(400254, 3)) {
	               pc.sendPackets("\\aG캐릭터 정보를 조회를 하기 위해서는 1억 수표(3)개가 필요합니다.");
	               return;
	             }

	             String typeName = null;
	             switch (target.getType()) {
	             case 0:
	               typeName = "군주";
	               break;
	             case 1:
	               typeName = "기사";
	               break;
	             case 2:
	               typeName = "요정";
	               break;
	             case 3:
	               typeName = "마법사";
	               break;
	             case 4:
	               typeName = "다크엘프";
	               break;
	             case 5:
	               typeName = "용기사";
	               break;
	             case 6:
	               typeName = "환술사";
	               break;
	             case 7:
	               typeName = "전사";
	               break;
	             default:
	               typeName = "????";
	             }
	             pc.sendPackets(new S_SystemMessage("\\aD--------------------------------------------------"));
	             pc.sendPackets(new S_SystemMessage(new StringBuilder().append("\\aD[ ").append(target.getName()).append(" ] 직업:").append(typeName).append(", 혈맹:").append(target.getClanname()).toString()));
	             if (!target.noPlayerCK);
	             pc.sendPackets(new S_SystemMessage("\\aD--------------------------------------------------"));

	             int hpr = target.getHpr() + target.getInventory().hpRegenPerTick();
	             int mpr = target.getMpr() + target.getInventory().mpRegenPerTick();
	             pc.sendPackets(new S_ChatPacket(pc, new StringBuilder().append("* 피: ").append(target.getCurrentHp()).append('/').append(target.getMaxHp()).append(" (틱: ").append(hpr).append(')').append("   엠: ").append(target.getCurrentMp()).append('/').append(target.getMaxMp()).append(" (틱: ").append(mpr).append(')').toString()));
	             pc.sendPackets(new S_ChatPacket(pc, new StringBuilder().append("* 힘: ").append(target.getAbility().getTotalStr()).append("  덱: ").append(target.getAbility().getTotalDex()).append("   콘: ")
	               .append(target
	               .getAbility().getTotalCon()).append("   인: ").append(target.getAbility().getTotalInt()).append("   위: ")
	               .append(target
	               .getAbility().getTotalWis()).append("   카: ").append(target.getAbility().getTotalCha()).toString()));
	             pc.sendPackets(new S_ChatPacket(pc, new StringBuilder().append("* 불: ").append(target.getResistance().getFire()).append("  물: ").append(target.getResistance().getWater()).append("  바람: ").append(target.getResistance().getWind()).append("  땅: ").append(target.getResistance().getEarth()).toString()));

	             pc.sendPackets(new S_ChatPacket(pc, String.format("* 기술내성:%d 정령내성:%d 용언내성:%d 공포내성:%d 전체내성:%d", new Object[] { 
	               Integer.valueOf(target
	               .getSpecialResistance(SC_SPECIAL_RESISTANCE_NOTI.eKind.ABILITY)), 
	               Integer.valueOf(target
	               .getSpecialResistance(SC_SPECIAL_RESISTANCE_NOTI.eKind.SPIRIT)), 
	               Integer.valueOf(target
	               .getSpecialResistance(SC_SPECIAL_RESISTANCE_NOTI.eKind.DRAGON_SPELL)), 
	               Integer.valueOf(target
	               .getSpecialResistance(SC_SPECIAL_RESISTANCE_NOTI.eKind.FEAR)), 
	               Integer.valueOf(target
	               .getSpecialResistance(SC_SPECIAL_RESISTANCE_NOTI.eKind.ALL)) })));

	             pc.sendPackets(new S_ChatPacket(pc, String.format("* 기술적중:%d 정령적중:%d 용언적중:%d 공포적중:%d 전체적중:%d", new Object[] { 
	               Integer.valueOf(target
	               .getSpecialPierce(SC_SPECIAL_RESISTANCE_NOTI.eKind.ABILITY)), 
	               Integer.valueOf(target
	               .getSpecialPierce(SC_SPECIAL_RESISTANCE_NOTI.eKind.SPIRIT)), 
	               Integer.valueOf(target
	               .getSpecialPierce(SC_SPECIAL_RESISTANCE_NOTI.eKind.DRAGON_SPELL)), 
	               Integer.valueOf(target
	               .getSpecialPierce(SC_SPECIAL_RESISTANCE_NOTI.eKind.FEAR)), 
	               Integer.valueOf(target
	               .getSpecialPierce(SC_SPECIAL_RESISTANCE_NOTI.eKind.ALL)) })));

	             pc.sendPackets(new S_SystemMessage(new StringBuilder().append("\\aD초기스텟 :(S: ").append(target.getAbility().getBaseStr()).append(" / D: ").append(target.getAbility().getBaseDex()).append(" / C: ")
	               .append(target
	               .getAbility().getBaseCon()).append(" / I: ").append(target.getAbility().getBaseInt()).append(" / W: ")
	               .append(target
	               .getAbility().getBaseWis()).append(" / C: ").append(target.getAbility().getBaseCha()).append(')').toString()));
	             pc.sendPackets(new S_SystemMessage(new StringBuilder().append("\\aH현재스텟:(S: ").append(target.getAbility().getTotalStr()).append(" / D: ").append(target.getAbility().getTotalDex()).append(" / C: ")
	               .append(target
	               .getAbility().getTotalCon()).append(" / I: ").append(target.getAbility().getTotalInt()).append(" / W: ")
	               .append(target
	               .getAbility().getTotalWis()).append(" / C: ").append(target.getAbility().getTotalCha()).append(')').toString()));
	             pc.sendPackets(new S_SystemMessage("\\aD--------------------------------------------------"));
	             pc.getInventory().consumeItem(400254, 3);
	           }
	           catch (Exception e) {
	             pc.sendPackets(new S_ChatPacket(pc, ".캐릭터정보 [캐릭명] 으로 입력."));
	           }
	         }

	         public static void charInventoryInfo(L1PcInstance pc, String param) {
	           Connection c = null;
	           PreparedStatement p = null;
	           PreparedStatement p1 = null;
	           ResultSet r = null;
	           ResultSet r1 = null;
	           try
	           {
	             if (!pc.getInventory().checkItem(400254, 5)) {
	               pc.sendPackets("\\aG상대방 캐릭터 장비를 조회를 하기 위해서는 1억 수표(5)개가 필요합니다.");
	               return;
	             }

	             StringTokenizer st = new StringTokenizer(param);
	             String charname = st.nextToken();

	             L1PcInstance target_ = L1World.getInstance().getPlayer(charname);
	             if (target_ == null) {
	               pc.sendPackets("\\aH해당 사용자가 접속하지 않았습니다.");
	               return;
	             }

	             c = L1DatabaseFactory.getInstance().getConnection();

	             int searchCount = 0;

	             p = c.prepareStatement(new StringBuilder().append("SELECT objid, char_name FROM characters WHERE char_name = '").append(charname).append("'").toString());
	             r = p.executeQuery();
	             while (r.next()) {
	               pc.sendPackets(new S_SystemMessage(new StringBuilder().append("\\aG=============== 캐릭터명: ").append(charname).append(" ===============").toString()));
	               L1PcInstance target = L1World.getInstance().getPlayer(charname);
	               if (target != null) target.saveInventory();

	               p1 = c.prepareStatement(new StringBuilder().append("SELECT item_id,enchantlvl,is_equipped,count,item_name,bless,attr_enchantlvl,bless_level,item_level FROM character_items WHERE char_id = '")
	                 .append(r
	                 .getInt(1))
	                 .append("' ORDER BY 3 DESC,2 DESC, 1 ASC").toString());
	               r1 = p1.executeQuery();
	               while (r1.next()) {
	                 L1ItemInstance item = ItemTable.getInstance().createItem(r1.getInt(1));
	                 if ((item.getItem().getType2() == 1) || (item.getItem().getType2() == 2) || ((item.getItem().getType2() == 0) && (r1.getInt(9) > 0))) {
	                   String itemname = getInvenItem(r1.getInt(1), r1.getInt(2), r1.getInt(3), r1.getInt(4), r1.getString(5), r1.getInt(6), r1.getInt(7), r1.getInt(8), r1.getInt(9));
	                   pc.sendPackets(new S_SystemMessage(new StringBuilder().append("\\fU").append(++searchCount).append(". ").append(itemname).toString()));
	                   itemname = "";
	                 }
	               }
	             }

	             pc.getInventory().consumeItem(400254, 5);
	           }
	           catch (Exception e) {
	             pc.sendPackets(new S_ChatPacket(pc, ".장비검사 [캐릭명] 으로 입력."));
	           } finally {
	             SQLUtil.close(r1); SQLUtil.close(p1);
	             SQLUtil.close(r); SQLUtil.close(p);
	             SQLUtil.close(c);
	           }
	         }
	         

	         private static String getInvenItem(int itemid, int enchant, int equip, int count, String itemname, int bless, int attr, int bless_level, int item_level) {
	           StringBuilder name = new StringBuilder();

	           switch (bless_level) { case 1:
	             name.append("\\aD[축1단]\\fU"); break;
	           case 2:
	             name.append("\\aD[축2단]\\fU"); break;
	           case 3:
	             name.append("\\aD[축3단]\\fU"); break;
	           case 4:
	             name.append("\\aD[축4단]\\fU"); break;
	           case 5:
	             name.append("\\aD[축5단]\\fU"); break;
	           }

	           L1DollPotentialInstance doll = new L1DollPotentialInstance();
	           if (item_level > 0) {
	             name.append(new StringBuilder().append("\\aG[").append(doll.find_potential(item_level)).append("]\\fU").toString());
	           }

	           if (enchant > 0)
	             name.append(new StringBuilder().append("+").append(enchant).append(" ").toString());
	           else if (enchant == 0)
	             name.append("");
	           else if (enchant < 0) {
	             name.append(new StringBuilder().append(String.valueOf(enchant)).append(" ").toString());
	           }

	           switch (attr) {
	           case 1:
	             name.append("화령:1단 ");
	             break;
	           case 2:
	             name.append("화령:2단 ");
	             break;
	           case 3:
	             name.append("화령:3단 ");
	             break;
	           case 4:
	             name.append("화령:4단 ");
	             break;
	           case 5:
	             name.append("화령:5단 ");
	             break;
	           case 6:
	             name.append("수령:1단 ");
	             break;
	           case 7:
	             name.append("수령:2단 ");
	             break;
	           case 8:
	             name.append("수령:3단 ");
	             break;
	           case 9:
	             name.append("수령:4단 ");
	             break;
	           case 10:
	             name.append("수령:5단 ");
	             break;
	           case 11:
	             name.append("풍령:1단 ");
	             break;
	           case 12:
	             name.append("풍령:2단 ");
	             break;
	           case 13:
	             name.append("풍령:3단 ");
	             break;
	           case 14:
	             name.append("풍령:4단 ");
	             break;
	           case 15:
	             name.append("풍령:5단 ");
	             break;
	           case 16:
	             name.append("지령:1단 ");
	             break;
	           case 17:
	             name.append("지령:2단 ");
	             break;
	           case 18:
	             name.append("지령:3단 ");
	             break;
	           case 19:
	             name.append("지령:4단 ");
	             break;
	           case 20:
	             name.append("지령:5단 ");
	             break;
	           }

	           name.append(new StringBuilder().append(itemname).append(" ").toString());

	           if (equip == 1) {
	             name.append("(착용)");
	           }
	           switch (bless) { case 129:
	             name.append("[봉인]"); break;
	           }

	           if (count > 1) {
	             name.append(new StringBuilder().append("(").append(count).append(")").toString());
	           }
	           return name.toString();
	         }
}

